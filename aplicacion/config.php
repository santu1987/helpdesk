<?php

	class Config{

		public static function bd(){
			$config = array(
				"bd2"=>array(
					"proveedor"		=>"mysql",
					"host"			=>"",
					"puerto"		=>"",
					"usuario"		=>"",
					"password"		=>"",
					"nombre_base"	=>""
				),
				"bd"=>array(
					"proveedor"		=>"postgres",
					"host"			=>"127.0.0.1",
					"puerto"		=>"5432",
					"usuario"		=>"postgres",
					"password"		=>"machurucuto666",
					"nombre_base"	=>"db_helpdesk"
				),
				/*"bd_sigesp"=>array(
					"proveedor"		=>"postgres",
					"host"			=>"10.0.8.17",
					"puerto"		=>"5432",
					"usuario"		=>"westerlund",
					"password"		=>"G4b0m4c0nd0%*",
					"nombre_base"	=>"db_avilatv_2015"
				)*/
				/*"bd_sigesp"=>array(
					"proveedor"		=>"postgres",
					"host"			=>"10.0.2.28",
					"puerto"		=>"5432",
					"usuario"		=>"postgres",
					"password"		=>"123456",
					"nombre_base"	=>"db_sms2"
				)*/
			);

			return $config;
		}

		public static function urls(){
			$urls = array(
				"url_server_name"	=>RUTA_SERVER,
				"url_server_js"		=>RUTA_SERVER."media/javascript/",
				"url_server_css"	=>RUTA_SERVER."media/stylesheet/"
			);
			return $urls;
		}

	}

?>
