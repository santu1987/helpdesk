<?php

	abstract class proveedor{

		protected $conex;
		public abstract function connect($host,$puerto,$usuario,$password,$base);
		public abstract function cerrar_conexion();
		public abstract function query($sql);
		public abstract function fetchArray($registros);
		public abstract function registrosDevueltos($registros);
		public abstract function registrosAfectados($registros);
		public abstract function escape($string);
		public abstract function getError();

	}

	class mysql extends proveedor{

		public function connect($host,$puerto,$usuario,$password,$base){
			$this->conex = mysql_connect($host,$usuario,$password);
			mysql_select_db($base,$this->conex);
			return $this->conex;
		}

		public function getError(){
			return mysql_error($this->conex);
		}

		public function query($sql){
			return mysql_query($sql);
		}

		public function fetchArray($registros){
			return mysql_fetch_array($registros);
		}

		public function registrosDevueltos($registros){
			return mysql_num_rows($registros);
		}

		public function registrosAfectados($registros){
			return mysql_affected_rows($this->conex);
		}

		public function escape($string){
			$string = stripslashes($string);
			return mysql_real_escape_string($string);
		}

		public function cerrar_conexion(){
			return mysql_close($this->conex);
		}

	}

	class postgres extends proveedor{

		public function connect($host,$puerto,$usuario,$password,$base){
			$this->conex = pg_connect("host='$host' port='$puerto' dbname='$base' user='$usuario' password='$password'");
			return $this->conex;
		}

		public function getError(){
			return pg_last_error($this->conex);
		}

		public function query($sql){
			return pg_query($sql);
		}

		public function fetchArray($registros){
			return pg_fetch_array($registros);
		}

		public function registrosDevueltos($registros){
			return pg_num_rows($registros);
		}

		public function registrosAfectados($registros){
			return pg_affected_rows($registros);
		}

		public function escape($string){
			if(function_exists("pg_escape_literal")){
				return pg_escape_literal($string);
			}else{
				$string = stripslashes($string);
				return pg_escape_string($string);
			}
		}

		public function cerrar_conexion(){
			return pg_close($this->conex);
		}
	}


	class CapaBaseDatos{
		private $conn;
		private $proveedor;
		private static $_con;
		private $parametros;
		private static $no_data = "NO_DATA";
		private static $host;
		private static $puerto;
		private static $usuario;
		private static $base;
		private static $password;

		private function __construct($proveedor){
			$this->proveedor = new $proveedor;
			$this->conn = $this->proveedor->connect(self::$host,self::$puerto,self::$usuario,self::$password,self::$base);
		}

		public static function abrir_conexion($proveedor,$host,$puerto,$usuario,$password,$base){
			if(!class_exists($proveedor)){
				print "EL PROVEEDOR ESPECIFICADO NO SE HA IMPLEMENTADO";
				exit;
			}else{
				$class 			= __CLASS__;
				self::$host		= $host;
				self::$puerto	= $puerto;
				self::$usuario	= $usuario;
				self::$base		= $base;
				self::$password	= $password;
				self::$_con = new $class($proveedor);
				if(!self::$_con->conn){
					print "OCURRIO UN ERROR EN EL ALGORITMO DE CONEXION";
					exit;
				}else{
					return self::$_con;
				}
			}
		}

		public function cerrar_conexion(){
			$this->proveedor->cerrar_conexion($this->conn);
		}

		private function replaceParams($coincidencias){
			$b=current($this->parametros);
			next($this->parametros); 
			return $b;
		}

		private function prepare($sql, $parametros){
			for($i=0;$i<sizeof($parametros); $i++){
				if(is_bool($parametros[$i])){
					$parametros[$i] = $parametros[$i]? 1:0;
				}
				elseif(is_double($parametros[$i]))
					$parametros[$i] = str_replace(',', '.', $parametros[$i]);
				elseif(is_numeric($parametros[$i]))
					$parametros[$i] = $this->proveedor->escape($parametros[$i]);
				elseif(is_null($parametros[$i]))
					$parametros[$i] = "NULL";
				else
					//$parametros[$i] = "'".$this->proveedor->escape($parametros[$i])."'"; para el servidor
					$parametros[$i] = $this->proveedor->escape($parametros[$i]);
			}
		
			$this->parametros = $parametros;
			$q = preg_replace_callback("/(\?)/i", array($this,"replaceParams"), $sql);
		
			return $q;
		}

		private function enviarQuery($q, $parametros){
			$query = $this->prepare($q, $parametros);
			$result = $this->proveedor->query($query);
			if($this->proveedor->getError()){
				//Controlar errores
				return 0;
			}else{
				return $result;
			}
		}

		public function execute($q, $parametros=null){
			$result = $this->enviarQuery($q, $parametros);
			if($result !== 0){ //LA SENTENCIA NO DEVOLVIO ERROR
				//REGEX PARA SABER QUE TIPO DE DDL SE EJECUTO
				$regex = "/(SELECT|INSERT|UPDATE|DELETE)/i";
				preg_match($regex,$q,$matches);
				$match = $matches[0];
				switch(strtoupper($match)){
					case 'SELECT':
						if($this->proveedor->registrosDevueltos($result) > 0){
							$arr = array();
							while($row = $this->proveedor->fetchArray($result)){
								$arr[] = $row;
							}
							return $arr;
						}else{
							return self::$no_data;
						}
					break;
					case 'DELETE':
						if($this->proveedor->registrosAfectados($result) > 0){
							return true;
						}else{
							return false;
						}
					break;
					case 'UPDATE':
						if($this->proveedor->registrosAfectados($result) > 0){
							return true;
						}else{
							return false;
						}
					break;
					case 'INSERT':
						if($this->proveedor->registrosAfectados($result) > 0){
							return true;
						}else{
							return false;
						}
					break;
				}
			}else if($result === 0){
				return "POSIBLE ERROR DE SINTAXIS SQL: ".$this->proveedor->getError();
			}
		}
	}

?>