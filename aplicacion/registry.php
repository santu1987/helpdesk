<?php

	class Registry{

		private static $_instancia;

		private $_storage;

		private function __contruct(){}
		

		public static function getInstance(){

			if(!self::$_instancia instanceof self){
				self::$_instancia = new Registry;
			}
			return self::$_instancia;

		}

		public function __set($key,$value){
			$this->_storage[$key] = $value;
		}

		public function __get($key){
			if(isset($this->_storage[$key])){
				return $this->_storage[$key];
			}
			return false;
		}


	}

?>