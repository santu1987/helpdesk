<?php

	class Load{

		public function vistaLogica($nombre){
			$vista = $nombre."View";
			$rutaVista = RUTA_SITIO."vistasLogicas/".$vista.".php";
			if(is_readable($rutaVista)){
				require_once($rutaVista);
				if(class_exists($vista)){
					$registry = Registry::getInstance();
					$registry->$nombre = new $vista;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}

		public function vistaGrafica($nombre){
			$rutaHtml = RUTA_SITIO."media/vistasGraficas/".$nombre.".html";
			if(is_readable($rutaHtml)){
				$registry = Registry::getInstance();
				$registry->$nombre = file_get_contents($rutaHtml);
			}else{
				return false;
			}
		}

		public function plantilla($nombre){
			$rutaPlantilla = RUTA_SITIO."media/plantillas/".$nombre.".html";
			if(is_readable($rutaPlantilla)){
				$registry = Registry::getInstance();
				$registry->$nombre = file_get_contents($rutaPlantilla);
			}else{
				return false;
			}
		}

		public function modelo($nombre){
			$modelo = $nombre."Model";
			$rutaModelo = RUTA_SITIO."modelos/".$modelo.".php";

			if(is_readable($rutaModelo)){
				require_once($rutaModelo);

				if(class_exists($modelo)){
					$registry =  Registry::getInstance();
					$registry->$nombre = new $modelo;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}

		public function controlador($nombre){
			$controlador = $nombre."Controller";
			$rutaControlador = RUTA_SITIO."controladores/".$controlador.".php";

			if(is_readable($rutaControlador)){
				require_once($rutaControlador);

				if(class_exists($controlador)){
					return new $controlador;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}

	}

?>