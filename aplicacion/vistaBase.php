<?php

	abstract class vistaBase{

		protected $_registry;

		protected $load;

		abstract public function render_dinamico($html,$data_dinamica);
		abstract public function render_vista($nombre_html,$html,$data_estatica,$data_dinamica);

		public function __construct(){
			$this->_registry = Registry::getInstance();
			$this->load = new Load();
		}

		final public function __get($key){
			if($return = $this->_registry->$key){
				return $return;
			}
			return false;
		}

		private function identificadores_estaticos($nombre_html){

			$identificadores = array();
			switch ($nombre_html){
				case 'pagina1':
					$identificadores = array("titulo","descripcion","creadopor","nombres");
				break;
				case 'cargaTicket':
					 $identificadores = array("btn_crearticket","mensaje_permisos","opciones_sol");
				break;	
				case 'cargasugerencias':
					 $identificadores = array("btn_guardar_sugerencia","mensaje_permisos");
				break;	  		
				case 'listamistickets':
					$identificadores = array("paginador_siguiente","paginador_anterior","clase_paginador_siguiente","clase_paginador_anterior","offset_tabla","cuantos_tabla","inicio_tabla","fin_tabla","clase_tabla","clase_tickets");	
				break;
				case 'listadmintickets':
					$identificadores = array("paginador_siguiente","paginador_anterior","clase_paginador_siguiente","clase_paginador_anterior","offset_tabla","cuantos_tabla","inicio_tabla","fin_tabla","clase_tabla","clase_tickets");	
				break;
				case 'banner':
					$identificadores = array("logo_institucion","datos_institucion","datos_usr","img_ppal_usr");
				break;
				case 'cargasolticket':
					$identificadores = array("paginador_siguiente","paginador_anterior","clase_paginador_siguiente","clase_paginador_anterior","offset_tabla","cuantos_tabla","inicio_tabla","fin_tabla","clase_tabla","clase_tickets");
				break;	
				case 'listapelacionestec':
					$identificadores = array("id_tec","id_ticket","nombre_tecnico","correo_tecnico");
				break;
				case 'cargapelticket':
					$identificadores = array("paginador_siguiente","paginador_anterior","clase_paginador_siguiente","clase_paginador_anterior","offset_tabla","cuantos_tabla","inicio_tabla","fin_tabla","clase_tabla","clase_tickets");
				break;
				case 'listauditoria':
					$identificadores = array("paginador_siguiente","paginador_anterior","clase_paginador_siguiente","clase_paginador_anterior","offset_tabla","cuantos_tabla","inicio_tabla","fin_tabla","clase_tabla","clase_tickets","clase_audi");
				break;
				case 'admin_tickets_cabecera':
					$identificadores = array("cerrar_ticket");
				break;
				case 'tablaAdminSistema_tecnico':
					$identificadores = array("paginador_siguiente","paginador_anterior","clase_paginador_siguiente","clase_paginador_anterior","offset_tabla","cuantos_tabla","inicio_tabla","fin_tabla");
				break;			
				case 'tablaAdminSistema_area_solucion':
					$identificadores = array("paginador_siguiente","paginador_anterior","clase_paginador_siguiente","clase_paginador_anterior","offset_tabla","cuantos_tabla","inicio_tabla","fin_tabla");
				break;			
				case 'tablaAdminSistema_departamento':
					$identificadores = array("paginador_siguiente","paginador_anterior","clase_paginador_siguiente","clase_paginador_anterior","offset_tabla","cuantos_tabla","inicio_tabla","fin_tabla");
				break;			
				case 'tablaAdminSistema_perfil':
					$identificadores = array("paginador_siguiente","paginador_anterior","clase_paginador_siguiente","clase_paginador_anterior","offset_tabla","cuantos_tabla","inicio_tabla","fin_tabla");
				break;
				case 'tablaAdminSistema_tiposol':
					$identificadores = $identificadores = array("paginador_siguiente","paginador_anterior","clase_paginador_siguiente","clase_paginador_anterior","offset_tabla","cuantos_tabla","inicio_tabla","fin_tabla");
				break;
				case 'listadoreportesTickets':
					$identificadores = array("tecnicos_tecnologia");
				break;	
			}
			return $identificadores;
		}

		protected function render_estatico($nombre_html,$html,$data_estatica){
			$html = $this->render_url_estaticas($html);
			if(($html != "")&&(count($data_estatica) > 0)){
				$diccionario = $this->armar_diccionario($data_estatica,$this->identificadores_estaticos($nombre_html));
				if($diccionario){
					foreach ($diccionario as $clave => $valor) {
						$html = str_replace("{".$clave."}",$valor,$html);
					}
				}
			}
			return $html;

		}


		private function armar_diccionario($data_estatica,$arr_identificadores){

			$diccionario = array();
			$identificadores = $arr_identificadores;
			if($identificadores){
				foreach($identificadores as $identificador){
					if(array_key_exists($identificador, $data_estatica)){
						$diccionario[$identificador] = $data_estatica[$identificador];
					}
				}
			}

			return $diccionario;
			
		}

		protected function set_match_identificador_dinamico($html,$identificador){

			$ini = strpos($html,$identificador);
            $fin = strpos($html,$identificador,$ini+1);
            $len = strlen($identificador);
            $cal = substr($html,($ini+$len),($fin-$len-$ini));

            return $cal;

		}

		private function render_url_estaticas($html){
			$urls = Config::urls();
			$dic = array(
				"{url_server_js}"	=>$urls["url_server_js"],
				"{url_server_css}"	=>$urls["url_server_css"],
				"{url_server_name}"	=>$urls["url_server_name"]
			);

			$html = str_replace(array_keys($dic), array_values($dic), $html);
			return $html;
		}

	}

?>