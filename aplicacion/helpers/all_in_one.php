<?php
require_once(RUTA_SITIO."aplicacion/helpers/PHPMailermaster/class.phpmailer.php");
require_once(RUTA_SITIO."aplicacion/helpers/PHPMailermaster/class.smtp.php");
	class allinoneHelper{

		public function __construnct(){}

        /*
        #### TOKENS
        */

		public static function generar_token(){
			$token = "=".sha1(uniqid(rand(),true))."=";
        	$_SESSION["token"] = $token;
            return $token;
		}

		public static function tokenEsValido($token){
        	if(trim($token) === trim($_SESSION["token"])){
            	return true;
            }else{
            	return false;
            }
        }

        /*
        #### SANITIZAR STRING DE ENTRADAS
        */

        public static function sanitizar_str($string){
            //$string = htmlentities($string);
            return $string;
        }

        /*
        ### $_POST $_GET $_REQUEST
        */

        public static function get_vars($arr=array()){
            $var = array();
            $str_var = array_keys($arr);
            $val_var = array_values($arr);
            for($i=0;$i<count($str_var);$i++){
                $var[$str_var[$i]] = self::sanitizar_str($val_var[$i]);
            }
            return $var;
        }

        /*
        ### REDIRECT Location
        */

        public static function redirect($lugar){
            header("Location: ".$lugar);
        }

        /*
        ### $_COOKIE
        */

        public static function generar_cookies($arr=array()){
            $str_var = array_keys($arr);
            $val_var = array_values($arr);
            for($i=0;$i<count($str_var);$i++){
                setcookie($str_var[$i],$val_var[$i],time()+60*60*24*2,"/","",0,1);
            }
        }
        /*
        ### Para transformar un array plpgsql en un array php
        */
        public function array_plpgsql_to_php($recordset){
            $recordset = substr($recordset, 1,strlen($recordset)-2);
            $recor_vector = array();
            $record_vector = explode(",",$recordset);
            return $record_vector;
        }
        /*
        ### Metodo que devuelve la ip real de un equipo
        */
        public function get_ip(){
                        if($_SERVER){
                            if($_SERVER["HTTP_X_FORWARDED_FOR"]){
                            $realip = $_SERVER["HTTP_X_FORWARDED_FOR"];
                            }elseif($_SERVER["HTTP_CLIENT_IP"]){
                            $realip = $_SERVER["HTTP_CLIENT_IP"];
                            }else{
                            $realip = $_SERVER["REMOTE_ADDR"];
                            }
                        }else{
                            if (getenv('HTTP_X_FORWARDED_FOR')){
                                    $realip = getenv('HTTP_X_FORWARDED_FOR');
                            }elseif(getenv('HTTP_CLIENT_IP')){
                                    $realip = getenv('HTTP_CLIENT_IP');
                            }else{
                                    $realip = getenv('REMOTE_ADDR');
                            }
                        }
                        return $realip;
        }
        /*
        ### Metodo para calcular diferencias entre dos fechas y horas
        */
        public function calcula_tiempo($start_time, $end_time) { 
            $total_seconds = strtotime($end_time) - strtotime($start_time); 
            $tiempo = $this->obtener_tiempo($total_seconds);
            return $tiempo;
        }
        /*
        ### Metodo para calcular sumatoria de fechas
        */
        public function calcular_suma($start_time,$end_time){
            //Crepo vecto tiempo_final
            $tiempo_final = array("horas"=>"","minutos"=>"","segundos"=>"");
            //--Realizo la transformación de tiempo a lista
            $tiempo_ini = $this->transformar_tiempo($start_time);
            $tiempo_fin = $this->transformar_tiempo($end_time);
            // return $tiempo_ini['horas'].":".$tiempo_ini['minutos'].":".$tiempo_ini['segundos']."---".$tiempo_fin['horas'].":".$tiempo_fin['minutos'].":".$tiempo_fin['segundos'];
            //---Calculo de tiempos:
            //--Segundos
            $tiempo_final['segundos'] = floatval($tiempo_ini['segundos'])+floatval($tiempo_fin['segundos']);
            $minutos = intval(($tiempo_final['segundos']/60)%60);//excedente de minutos en segundos
            $tiempo_final['segundos'] = $tiempo_final['segundos']-($minutos*60);
            //--Minutos
            $tiempo_final['minutos'] = $minutos+intval($tiempo_ini['minutos']+intval($tiempo_fin['minutos']));
            $horas = intval(($tiempo_final['minutos']/60)%60);
            $tiempo_final['minutos'] = $tiempo_final['minutos']-($horas*60);
            //--Horas
            $tiempo_final['horas'] = $horas+intval($tiempo_ini['horas'])+intval($tiempo_fin['horas']);
            //
            return sprintf(
                            '%02s:%02s:%02s',
                            $tiempo_final['horas'],
                            $tiempo_final['minutos'],
                            $tiempo_final['segundos'] 
                          );
        }
        /*
        ### Metodo para pasar de segundos a formato h:m:s
        */
        public function obtener_tiempo($total_seconds){
            $horas              = floor ( $total_seconds / 3600 );
            $minutes            = ( ( $total_seconds / 60 ) % 60 );
            $seconds            = ( $total_seconds % 60 );
            $time['horas']      = str_pad( $horas, 2, "0", STR_PAD_LEFT );
            $time['minutes']    = str_pad( $minutes, 2, "0", STR_PAD_LEFT );
            $time['seconds']    = str_pad( $seconds, 2, "0", STR_PAD_LEFT );
            $time               = implode( ':', $time );
            return $time;
        }
        /*
        ### Metodo que transforma h:m:s en segundos
        */
        public function equivalente_segundos($horas){
            //Transformar a formato tiempo
            $tiempo = $this->transformar_tiempo($horas);
            $total_segundos = ($tiempo["horas"]*3600)+($tiempo["minutos"]*60)+$tiempo["segundos"];
            return $total_segundos;
            //
        }
        /*
        ### Metodo que transforma segundos a h:m:s para el promedio
        */
        public function equivalente_hms($tiempo_en_segundos){
            $time["horas"] = floor($tiempo_en_segundos / 3600);
            $time["minutos"] = floor(($tiempo_en_segundos - ($time["horas"] * 3600)) / 60);
            $time["segundos"] = $tiempo_en_segundos - ($time["horas"] * 3600) - ($time["minutos"] * 60);
            //Le coloco dos numeros por segmento
            // 
            $time["horas"] = str_pad($time["horas"], 2, "0", STR_PAD_LEFT );
            $time["minutos"] = str_pad($time["minutos"], 2, "0", STR_PAD_LEFT );
            $time["segundos"] = str_pad($time["segundos"], 2, "0", STR_PAD_LEFT );
            $time["segundos"] = substr($time["segundos"],0,5);
            //$time["segundos"] = number_format($time["segundos"],2,'.','');
            $time = implode( ':', $time );
            return $time;
        }
        /*
        ### Metodo que recibiendo un tiempo :H:m:s retorna una lista
        */
        public function transformar_tiempo($tiempo){
            $tiempo_r = array("horas"=>"","minutos"=>"","segundos"=>"");
            list($tiempo_r['horas'],$tiempo_r['minutos'],$tiempo_r['segundos']) = explode(":",$tiempo);
            return $tiempo_r;
        }
        /*
        ### Metodo que calcula el promedio de horas
        */    
        public function calcular_promedio_horas($suma_horas,$cuantos_tickets){
            //--Transformo horas en segundos:
            $total_segundos = $this->equivalente_segundos($suma_horas);//-Cargo el metodo con los segundos equivalentes a las horas totales...
            $promedio_segundos = $total_segundos/$cuantos_tickets;
            $tiempo_total = $this->equivalente_hms($promedio_segundos);
            return $tiempo_total;
            //--
        } 
        /*
        ### Metodo que transforma los &acute; en acentos
        */
        public  function remplazar_acentos_acute($cadena1){
            $cadena = "";
            $cadena = str_replace("&AACUTE;","Á",$cadena1); 
            $cadena = str_replace("&EACUTE;","É",$cadena); 
            $cadena = str_replace("&IACUTE;","Í",$cadena); 
            $cadena = str_replace("&OACUTE;","Ó",$cadena); 
            $cadena = str_replace("&UACUTE;","Ú",$cadena); 
            $cadena = str_replace("&aacute;","á",$cadena); 
            $cadena = str_replace("&eacute;","é",$cadena); 
            $cadena = str_replace("&iacute;","í",$cadena); 
            $cadena = str_replace("&oacute;","ó",$cadena); 
            $cadena = str_replace("&uacute;","ú",$cadena); 
            $cadena = str_replace("ñ","Ñ",$cadena); 
            return $cadena;
        }
        /*
        ###
        /*
        ### Metodo que transformo las ó en Ó
        */
        public function remplazar_acentos_min($cadena1){
            $cadena = "";
            $cadena = str_replace("&AACUTE;","Á",$cadena1); 
            $cadena = str_replace("&EACUTE;","É",$cadena); 
            $cadena = str_replace("&IACUTE;","Í",$cadena); 
            $cadena = str_replace("&OACUTE;","Ó",$cadena); 
            $cadena = str_replace("&UACUTE;","Ú",$cadena); 
            $cadena = str_replace("&aacute;","á",$cadena); 
            $cadena = str_replace("&eacute;","é",$cadena); 
            $cadena = str_replace("&iacute;","í",$cadena); 
            $cadena = str_replace("&oacute;","ó",$cadena); 
            $cadena = str_replace("&uacute;","ú",$cadena); 
            $cadena = str_replace("á","Á",$cadena1);
            $cadena = str_replace("é","É",$cadena);
            $cadena = str_replace("í","Í",$cadena);
            $cadena = str_replace("ó","Ó",$cadena);
            $cadena = str_replace("ú","Ú",$cadena);
            $cadena = str_replace("ñ","Ñ",$cadena); 
            return $cadena;     
        }
        /*
        ###
        */
        /*
        ### Metodo que transforma el fechas de formato dd-mm-YYYY en YYYY-mm-dd
        */
        public function formato_fecha_pgsql($fecha){
            $fecha_result = date("Y-m-d", strtotime($fecha) );
            return $fecha_result;
        }
        /*
        ##
        */
    }    
?>