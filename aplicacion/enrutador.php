<?php

	class Enrutador{

		private $load;
		private $_uri = array();
		private $_accion = array();

		public function __construct(){
			$this->load = new Load();
			$this->setear_uris("/","index");
			$this->setear_uris("/entorno/login","acceso");
			$this->setear_uris("/entorno/panel","panel");
			$this->setear_uris("/entorno/crearticket","crearticket");
			$this->setear_uris("/entorno/cerrar_session","cerrar");
			$this->setear_uris("/entorno/verificar_sesion","verificarsesion");
			$this->setear_uris("/entorno/reportes","cargareportes");
			$this->setear_uris("/entorno/reportes_dpto","cargareportes");
		}
		
		private function setear_uris($uri,$accion = null){
			$this->_uri[] = ($uri == "/") ? "/" : trim($uri,"/");
			if($accion != null){
				$this->_accion[] = $accion;
			}
		}

		public function run(){
			if(array_key_exists("uri", $_GET)){ //$_GET["uri"] viene del .htaccess
				//ENTRA POR PETICION COMUN (www.ejemplo.com/peticion/)
				$uri = (isset($_GET["uri"])) ? $_GET["uri"] : "/";
				$match = false;

				if(isset($_POST["letrametodo"])){
					$metodo = $_POST["letrametodo"];
				}else if(isset($_GET["letrametodo"])){
					$metodo = $_GET["letrametodo"];
				}else{
					$metodo = "index";
				}

				foreach($this->_uri as $clave => $valor){
					if(preg_match("#^$valor#", $uri)){
						$match = true;
						$archivoControlador = $this->_accion[$clave];
						$controlador = $this->load->controlador($archivoControlador);
						if($controlador !== false){
							call_user_func(array($controlador,$metodo));
						}
						break;
					}
				}
				if(!$match){
					$controlador = $this->load->controlador("notfound");
					if($controlador !== false){
						call_user_func(array($controlador,'index'));
					}
				}
			}else if(array_key_exists("letrajax",$_GET) || array_key_exists("letrajax",$_POST)){
				//ENTRA POR AJAX..
				//LA VARIABLE letrajax ES UNA VARIABLE ADREDE QUE PUEDE TENER CUALQUIER VALOR Y QUE SE UTILIZA PARA
				//REDIRECCIONAR LAS PETICIONES AJAX A ESTA SECCION DEL BLOQUE "ELSE IF" Y PODER CAPTURARLAS
				if(isset($_POST["letracontrol"])){
					$controlador = $_POST["letracontrol"];
				}else if(isset($_GET["letracontrol"])){
					$controlador = $_GET["letracontrol"];
				}else{
					$controlador = "index";
				}

				if(isset($_POST["letrametodo"])){
					$metodo = $_POST["letrametodo"];
				}else if(isset($_GET["letrametodo"])){
					$metodo = $_GET["letrametodo"];
				}else{
					$metodo = "index";
				}

				$controlador = $this->load->controlador($controlador);
				if($controlador !== false){
					if(isset($_POST["valores"])){
						$controlador->$metodo($_POST["valores"]);
					}else if(isset($_GET["valores"])){
						$controlador->$metodo($_GET["valores"]);
					}else{
						call_user_func(array($controlador,$metodo));
					}
				}

			}else{
				//NO ES UNA PETICION COMUN (www.ejemplo.com/peticion/)
				//NO ES UNA PETICION AJAX.
				//ENTONCES HA DE ESTAR EN LA PAGINA PRINCIPAL (www.ejemplo.com)
				$controlador = $this->load->controlador("index");
				if($controlador !== false){
					call_user_func(array($controlador,'index'));
				}
			}
		}

	}

?>
