<?php
	class adminticketsController extends controladorBase{
		public function __construct(){
			parent::__construct();
		}
		public function index(){
		}
		//
		public function calculo_limites($offset,$cuantos_son,$tipo){
			$this->inicio_tabla = $offset+1;
			//Valido que si $offset=0 se inicie la tabla en valor 0
			if($cuantos_son==0){
				$this->inicio_tabla = 0;
			}
			$valor = $offset+50;
			if($valor >= $cuantos_son)
				$this->fin_tabla = $cuantos_son;
			else
				$this->fin_tabla = $valor;		
		}
		//Metodo para cargar la consulta en tabla con su respectiva paginacion
		public function cargar_pag_adm($vector){
			$this->load->vistaLogica('admintickets');//cargo la vista lógica admintickets
			$this->load->vistaGrafica('listadmintickets');//cargo la vista grafica
			$obj = $this->load->modelo('caradmintickets');//cargo el modelo caradmintickets
			//Validación de cuantos_son
			if(($vector[1]==0)||($vector[3]==3))
			{
				$cuantos_son = $this->caradmintickets->cuantos_tickets_adm();//Determino cuantos ticket_son
				$cuantos_son = $cuantos_son[0][0];
			}				
			else
				$cuantos_son = $vector[1];//Determino cuantos ticket_son
			//--Calculo de a que página debe ir
			switch ($vector[3]){
				case 0:
						$offset=0;
						///Calculo limites:
						$this->calculo_limites($offset,$cuantos_son,1);
				break;		
				case 1:
						$offset=$vector[0]+$vector[2];
						///Calculo limites:
						$this->calculo_limites($offset,$cuantos_son,1);
				break;
				case 2:
						$offset=$vector[0]-$vector[2];
						///Calculo limites:
						$this->calculo_limites($offset,$cuantos_son,2);
				break;
				case 3:
						$offset=$vector[0];
						//--
						if($vector[0]==$cuantos_son)
							$offset=0;
						//--
						$this->calculo_limites($offset,$cuantos_son,1);
				break;		
			}
			$clase_sig='';
			$clase_ant='';
			//Para ocultar siguiente 
			$offset_sig = $offset+$vector[2];
			if(($offset_sig == $cuantos_son)||($offset_sig > $cuantos_son ))
				$clase_sig = "disabled";
			//Parea ocultar anterior
			if($offset == 0)
				$clase_ant = "disabled";

			$admintickets = $this->caradmintickets->consultar_admin_tickets($offset, $vector[2]);//obtener los tickets segun parametros de paginacion
			//
			//Validar si tiene o no tickets
			if($cuantos_son>0){
					$clase_tabla = 'show';
					$clase_tickets = 'hide';
			}else{
					$clase_tabla = 'hide';
					$clase_tickets = 'show';
			}
			//--Bloque de permisologías a acciones
			$this->load->modelo('permisologias');//Cargo el modelo
			$obj2 = new allinoneHelper();//Cargo helper
			$admintickets_permisos = $this->permisologias->permiso_pantalla(7);//Metodo que consulta si el usuario tiene permisos en pantalla Admin-tickets
			$permisos_astec = $obj2->array_plpgsql_to_php($admintickets_permisos[0][0]);///Metodo que transforma arreglo plpgsql en array php
			//--	
			$this->admintickets->render_vista(
				"listadmintickets",
				$this->listadmintickets,
				array(
						"paginador_siguiente"=>"ir_tabla_adm_btn(".$offset.",".$cuantos_son .",50,1);",
						"paginador_anterior"=>"ir_tabla_adm_btn(".$offset.",".$cuantos_son .",50,2);",
						"clase_paginador_siguiente"=>$clase_sig,
						"clase_paginador_anterior"=>$clase_ant,
						"offset_tabla"=>$offset,
						"cuantos_tabla"=>$cuantos_son,
						"inicio_tabla"=>$this->inicio_tabla,
						"fin_tabla"=>$this->fin_tabla,
						"clase_tabla"=>$clase_tabla,
						"clase_tickets"=>$clase_tickets 
					),
				array(
						"admintickets"=>$admintickets,
						"permisos_astec"=>$permisos_astec
					)
				);
		}
		//Metodo para cargar tecnicos en modal de administracion de tickets
		public function cargatecnicos_ticket($vector){
			$this->load->vistaLogica('admintecnicos');//Cargando vista logica de sección técnicos asignados de modal de administración de tickets
			$this->load->vistaGrafica('listadmintecnicos');//Cargando la vista grafica de la seccion ""
			$this->load->modelo('cargatecnicos');//Cargando el modelo 
			$seleccion_tc = $this->cargatecnicos->consultar_tecnicos_todos($vector[0]);//Proceso consulta para seleccionar tecnico
			//---Bloque de permisologías
			$this->load->modelo('permisologias');//cargo el modelo
			$obj2 = new allinoneHelper();//Cargo helper
			$astec_permisos = $this->permisologias->permiso_pantalla(7);//Metodo que consulta si el usuario tiene permisos en pantalla crear ticket
			$vector_permisos = $obj2->array_plpgsql_to_php($astec_permisos[0][0]);///Metodo que transforma arreglo plpgsql en array php
			//---
			$this->admintecnicos->render_vista(
				"listadmintecnicos",
				$this->listadmintecnicos,
				array(""),
				array(
						"seleccion_tc"=>$seleccion_tc,
						"ticket"	  =>$vector[0],
						"apelaciones"	  =>$vector[1],
						"vector_permisos" =>$vector_permisos
					 )
			);
		}
		//Metodo que asigna o des-asigna tecnicos a un ticket
		public function asignar_tecnicos($matriz){
			$obj = $this->load->modelo('cargatecnicos');//Cargando el modelo
			$matriz[2]=1;
			$resp = $this->cargatecnicos->registrar_tecnicos($matriz);//Cargo el metodo para realizar el registro
			echo $resp;
		}
		//Metodo que asigna o des-asigna tecnicos a un ticket
		public function revocar_tecnicos($matriz){
			$obj = $this->load->modelo('cargatecnicos');//Cargando el modelo
			$matriz[2]=2;
			$resp = $this->cargatecnicos->registrar_tecnicos($matriz);//Cargo el metodo para realizar el registro
			echo $resp;
		}
		//Metodo que renderiza las apelaciones del tecnico en el modal
		public function cargar_apelaciones_tecnicos($vector)
		{
			$this->load->vistaLogica("apelacionestecnicos");//cargo la vista logica
			$this->load->vistaGrafica("listapelacionestec");//cargo la vista grafica
			$this->load->modelo("carapelaciones");
			$apelaciones = $this->carapelaciones->consultar_apl_tecnico($vector[0],$vector[1]);
			$this->apelacionestecnicos->render_vista(
				"listapelacionestec",
				$this->listapelacionestec,
				array(
						"id_tec"    =>$vector[1],
						"id_ticket" =>$vector[0],
						"nombre_tecnico" =>$vector[2],
						"correo_tecnico" =>$vector[3]
					 ),
				array(
						"apelaciones" =>$apelaciones
					)
			);
		}
		//Metodo que registra la asignaciones a las apelaciones
		public function reg_as_apel($vector)
		{
			$arreglo_check = $vector[0];
			$arreglo_marcado = $vector[1];
			$this->load->modelo("carapelaciones");
			$resp = $this->carapelaciones->registrar_asignacion_apel($arreglo_check,$arreglo_marcado,$vector[2],$vector[3]);
			print $resp;
		}
		//Metodo para revocar tickets a un tecnico
		public function revocar_ticket_tec($vector){
			$this->load->modelo("cargatecnicos");//cargando modelo
			$resp = $this->cargatecnicos->eliminar_tecnico($vector[0],$vector[1]);//cargo el metodo y le paso el id_ticket y el id_tecnico...
			print $resp;
		}
		//Metodo que consulta si un tecnico ya fue revocado de un ticket
		public function consultar_estatus_tecnico($vector)
		{
			$this->load->modelo("cargatecnicos");//cargo el modelo
			$resp = $this->cargatecnicos->consultar_estatus_tecnico($vector[0],$vector[1]);//cargo el metodo y le paso id_ticket y el id_tecnico...
			print $resp;
		}
		//Metodo que se encarga de recorrer tickets_x_tecnico para verificar su estatus y dar cierre
		public function cerrar_tickets($vector)
		{
			$this->load->modelo("caradmintickets");//cargo el modelo
			$resp = $this->caradmintickets->cerrar_tickets($vector);//ejecuto el metodo
			print $resp;
		}
		//Metodo para realizar el envio de correo a la asignacion de un ticket a un técnico
		public function envio_correo_asignacion($vector){
			$this->load->modelo('correo');
			//$correo_sistema = "gsantucci@avilatv.gob.ve";//aquí va el correo del sistema
			$correo_sistema = "soporte@avilatv.gob.ve";//aquí va el correo del sistema
			$titulo = "Nueva asignación de ticket para técnico ".$vector[0];
			$titulo = utf8_decode($titulo);
			$encabezado = "El técnico ".$vector[0]." ha sido asignado para resolver el siguiente ticket: <br>";
			$encabezado = utf8_decode($encabezado);	
			$descripcion_ticket = utf8_decode($vector[3]);
			$mensaje = $this->correo->crear_cuadro($titulo,$encabezado,$descripcion_ticket,$vector[2]);//Metodo que crea el cuadro del correo enviado
			$this->correo->enviar_correo($correo_sistema,$vector[1],$mensaje,$titulo);//Metodo que realiza el envío del correo		
		}
		//Metodo para realizar el envío de correo a la asignación de una apelación a un técnico
		public function envio_correo_asignacion_apl($vector){
				$this->load->modelo('correo');
				//$correo_sistema = "gsantucci@avilatv.gob.ve";//aquí va el correo del sistema
				$correo_sistema = "soporte@avilatv.gob.ve";//aquí va el correo del sistema
				$titulo = "Nueva asignación de apelación para técnico ".$vector[2];
				$titulo = utf8_decode($titulo);
				$encabezado ="El técnico ".$vector[2]." ha sido asignado para resolver las siguientes apelaciones: <br>";
				$encabezado = utf8_decode($encabezado);
				$descripcion_ticket = $this->correo->cargar_apelaciones_listado($vector[0]);//Metodo que ordena el listado de las apelaciones asignadas
				$mensaje = $this->correo->crear_cuadro($titulo,$encabezado,$descripcion_ticket,$vector[1]);
				$this->correo->enviar_correo($correo_sistema,$vector[3],$mensaje,$titulo);//Metodo que realiza el envío del correo	
		}
		//Metodo que renderiza la cabecera de título de admintickets
		public function cargarcabecera()
		{
			$this->load->vistaLogica("cabeceradmin");
			$this->load->plantilla("admin_tickets_cabecera");
			//---Bloque de permisologías
			$this->load->modelo('permisologias');//cargo el modelo
			$obj2 = new allinoneHelper();//Cargo helper
			$astec_permisos = $this->permisologias->permiso_pantalla(4);//Metodo que consulta si el usuario tiene permisos en pantalla crear ticket
			$vector_permisos = $obj2->array_plpgsql_to_php($astec_permisos[0][0]);///Metodo que transforma arreglo plpgsql en array php
				//--
				if($vector_permisos[3]=='t')//Valido solo si es técnico con permiso de Modificar 
				{
					$cerrar_ticket = '<div id="contenedor_select" name="contenedor_select"></div>
											<div class="btn_historial" id="historial_mes" name="historial_mes" title="Pulse aqu&iacute; para actualizar los estatus de los ticket por mes">
												<i class="fa fa-history"></i>
											</div>';
				}else
				{
					$cerrar_ticket="";
				}
				//--
			//---
			$this->cabeceradmin->render_vista(
				"admin_tickets_cabecera",
				$this->admin_tickets_cabecera,
				array("cerrar_ticket"=>$cerrar_ticket),
				array("")
			);
		}
		//
	}
?>