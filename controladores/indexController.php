<?php

	class indexController extends controladorBase{

		public function __construct(){
			parent::__construct();
		}

		public function index(){
					
			$this->load->vistaLogica('index');

			$this->load->vistaGrafica('pantalla_is');

			//$this->load->modelo("posts");

			//$get = $this->posts->get_tickets();

			$this->index->render_vista(
				"pantalla_is",
				$this->pantalla_is,
				array(""),
				array("")
			);

		}
	}	
?>
