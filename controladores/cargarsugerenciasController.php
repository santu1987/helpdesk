<?php

	class cargarsugerenciasController extends controladorBase{

		public function __construct(){
			parent::__construct();
		}
		public function index(){
			
			$this->load->vistaGrafica('cargasugerencias');//Cargo vista gráfica 
			$this->load->vistaLogica('listsugerencias');//Cargo la vista lógica
			$this->load->modelo('permisologias');
			$obj2 = new allinoneHelper();//Cargo helper
			$sugerencias_permisos = $this->permisologias->permiso_pantalla(3);//Metodo que consulta si el usuario tiene permisos en pantalla sugerencias
			$vector_permisos = $obj2->array_plpgsql_to_php($sugerencias_permisos[0][0]);///Metodo que transforma arreglo plpgsql en array php
			if(($vector_permisos[0] == '0')||($vector_permisos[2]=='t'))//Valido solo si es usuario(no técnico) o técnico con permiso de incluir 
			{
				$btn_guardar_sugerencia = "<div type='button' id='btn_guardar_sugerencia' name='btn_guardar_sugerencia' class='btn btn-aceptar'>Enviar Sugerencia</div>";
				$mensaje_permisos = ""; 
			}else
			{
				$btn_guardar_sugerencia="";
				$mensaje_permisos = "<div class='alert alert-info mensaje_no_permiso'><i class='fa fa-exclamation-circle'></i> No tiene permiso para registrar Sugerencias</div>";
			}
			//
			$this->listsugerencias->render_vista(
				'cargasugerencias',
				$this->cargasugerencias,
				array(
						"btn_guardar_sugerencia" => $btn_guardar_sugerencia,
					    "mensaje_permisos" =>$mensaje_permisos
					  ),
				array("")
			);
			//print $this->cargarsugerencias;
		}
		public function registrarsugerencia($vector){
			$obj = $this->load->modelo('cargarsugerencias');
			$resp = $this->cargarsugerencias->registrar_sugerencia($vector[0]);
			echo $resp[0][0];
		}	
	}	
?>