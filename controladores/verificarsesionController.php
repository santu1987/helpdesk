<?php
class verificarsesionController extends controladorBase{
	public function __construct(){
			parent::__construct();
	}
	//Metodo que valida si la sesion esta activa
	public function index(){
		$obj = new allinoneHelper();//cargo el archivo allinone
		if(isset($_SESSION["helpdesk"]))
		{
		  if ($_SESSION["helpdesk"] == "ACTIVO")
		  {
		    $obj->redirect("/helpdesk/entorno/panel");
		  }else{
		  	$obj->redirect("http://helpdesk.avilatv.gob.ve/");
		  }
		}else{
			$obj->redirect("http://helpdesk.avilatv.gob.ve/");
		}
		//if (isset($_GET["cerrar"])){ $cerrar = base64_decode($_GET["cerrar"]);} else { $cerrar = "";}
	}
	//Metodo que valida llamada a través de una petición ajax si  la sesión se encuentra activa...
	public function encuentra_sesion_activa(){
		if(isset($_SESSION["helpdesk"])){
			if($_SESSION["helpdesk"] == "ACTIVO")
			{
				echo 0; //sesion activa
			}else
			{
				echo 1; //session inactiva				
			}	
		}else
		{
			echo 1;//sesion inactiva
		}
	}
}
?>