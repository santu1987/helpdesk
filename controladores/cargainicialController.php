<?php
class cargainicialController extends controladorBase{
	public function __construct(){
		parent::__construct();
	}
	public function index(){
	}
	//--Metodo que carga el banner en la pantalla principal
	public function cargabanner(){
		$this->load->vistaLogica('estructurabanner');//cargar vista logica
		$this->load->plantilla('banner');//cargar vista grafica
		$this->estructurabanner->render_vista(
			"banner",
			$this->banner,
			array(
					"logo_institucion"  => "<div id='logo_sistem' class='options-logo'><i class='fa fa-desktop'></i></div>",
					"datos_institucion" => "Sistema HelpDesk de la Fundación Ávila Tvé",
					"datos_usr" 		=> $_SESSION["nombre"],
					"img_ppal_usr" 		=> "http://appsigesp.avilatv.gob.ve/sno/fotospersonal/".$_SESSION["cedula"]
				),
			array("")
		);//
	}
	//--Metodo que carga el menú según la permisología de acceso del usuario
	public function cargamenu(){
		$this->load->vistaLogica("estructuramenu");//carga la vista lógica de la estructura del menú
		$this->load->plantilla("menu");//carga la vista grafica del menú
		$this->load->modelo("permisologias");//cargo el modelo que retorna las permisologías del usuario
		$menu_us = $this->permisologias->accesomenu();//cargo el metodo que devuelve la estructura de las permisologías según usuario
		$this->estructuramenu->render_vista(
			"menu",
			$this->menu,
			array(""),
			array("menu"=>$menu_us)
		);
	}
}
?>