<?php
	class cargarapelacionController extends controladorBase{
		
		public function __construct(){
			parent::__construct();
		}
		public function index(){
			
		}
		public function registrarapelacion($vector){
			$obj = $this->load->modelo('apelaciones');//cargo el modelo
			$helper = new allinoneHelper();
			$resp = $this->apelaciones->registrar_apelacion($vector);//le paso un vector al método al metodo...
			//--Transformación de arreglo postgres a arreglo en php
			$record_vector = $helper->array_plpgsql_to_php($resp[0][0]);//Metodo que transforma arreglo plpgsql en array php
			//--
			if($record_vector[0] == 1)//Si regisró satisfactoriamente
			{
				$result = $this->enviar_correo_apelacion($vector[1],$vector[0],$record_vector[1]);//Se realiza envío del correo
				if($result==-1){
					echo $result;
				}
			}
			echo $resp;
		}
		public function consultarmensaje($vector){
			$this->load->vistaLogica('apelacioneslistados');//cargo vista logica apelacioneslistados 
			$this->load->vistaGrafica('listapelaciones');//cargo la vista grafica listapelaciones
			$obj = $this->load->modelo('apelaciones');//cargo el modelo
			$mensajes = $this->apelaciones->consultar_mensajes_apelacion($vector);//ejecuto el metodo que consulta los mensajes por cada apelacion
			$mensajes_tecnicos = $this->apelaciones->consultar_mensaje_tecnico($vector);//ejecuto el metodo de consulta de los mensajes del tecnico SOLUCIÓN INICIAL
			$this->apelacioneslistados->render_vista(
				"listapelaciones",
				$this->listapelaciones,
				array(''),
				array(
						$this->listapelaciones,
						"mensajes"=>$mensajes,
						"mensajes_tecnicos"=>$mensajes_tecnicos,
					)
			);
			//falta crear la vista lógica y grafica.....
		}
		//Metodo para realizar envio de email
		public function enviar_correo_apelacion($n_ticket,$observacion,$n_apel){
			$this->load->modelo('correo');//Cargo el modelo correo
			$titulo = "Nueva apelación a Ticket #".$n_ticket." ".$_SESSION["nombre"];//Título del email
			$titulo = utf8_decode($titulo);
			$encabezado = "El usuario ".$_SESSION["nombre"]." realiz&oacute; la siguiente apelaci&oacute;n :";
			//$correo_sistema = "gsantucci@avilatv.gob.ve";// aqui va el correo del sistema
			$correo_sistema = "soporte@avilatv.gob.ve";//aquí va el correo del sistema
			$mensaje = $this->correo->crear_cuadro_apl($titulo,$encabezado,utf8_decode($observacion),$n_apel,$n_ticket);//armo la estructura del cuadro del mensaje
			$resp2 = $this->correo->enviar_correo($correo_sistema,$correo_sistema,$mensaje,$titulo);//envío el correo
			echo $resp;
		}
	}
?>