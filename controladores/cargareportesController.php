<?php
ob_end_clean();//De esta manera limpia el buffer de salida
require_once(RUTA_SITIO."aplicacion/helpers/tcpdf/tcpdf.php");//requiero la clase tcpdf
class cargareportesController extends controladorBase{
	//--Metodo constructor
	public function __construct(){
		parent::__construct();
	}
	//--Metodo index
	public function index(){
		$this->load->vistaLogica('reportessistema');//Cargo la vista logica
		$this->load->vistaGrafica('listadoreportes');//Cargo la vista Grafica
		$this->reportessistema->render_vista(
			"listadoreportes",
			$this->listadoreportes,
			array(""),
			array("")
		);
	}
	//--Metodo que permite la carga de los filtros de reporte tickets_x_tecnico
	public function reporte_filtro_ticket_tecnico(){
		$this->load->vistaLogica('reportessistema');//Cargo la vista lógica
		$this->load->vistaGrafica('listadoreportesTickets');//Cargo la vista Gráfica
		$this->reportessistema->render_vista(
			"listadoreportesTickets",
			$this->listadoreportesTickets,
			array(""),
			array("")
		);
	}
	//--Metodo que permite la carga de lois filtros de reporte de tickets_x_dpto
	public function reporte_filtro_ticket_dpto(){
		$this->load->vistaLogica('reportessistemadpto');//Cargo la vista lógica
		$this->load->vistaGrafica('listadoreportesTicketsDpto');//Cargo la vista gráfica
		$this->reportessistemadpto->render_vista(
			"listadoreportesTicketsDpto",
			$this->listadoreportesTicketsDpto,
			array(""),
			array("")
		);
	}
	//--Metodo que realiza la consulta de los tecnicos
	public function cargar_tecnicos_select(){
		$this->load->modelo('cargatecnicos');//Cargo el modelo que contiene la consulta de los tecnicos
		$tecnicos_tecnologia = $this->cargatecnicos->consultar_tecnicos_select();//Ejecuto el metodo
		$opcion = "<option id='tecnico_rep0' name='tecnico_rep0' value='0'>--Seleccione un Técnico--</option>";
		if($tecnicos_tecnologia[0][0]!="NO_DATA"){
			for($i=0;$i<count($tecnicos_tecnologia);$i++){
				$opcion.="<option id='tecnico_rep".$i."' value='".$tecnicos_tecnologia[$i]["id_tecnico"]."'>".$tecnicos_tecnologia[$i]["nombres"]." ".$tecnicos_tecnologia[$i]["apellidos"]."</option>";
			}
		}
		die($opcion);			
	}
	//--Metodo que realiza la consulta de los dptos
	public function cargar_dpto_select(){
		$this->load->modelo("cargadptos");//Cargo el modelo que contiene la consulta de los tecnicos...
		$dptos = $this->cargadptos->consultar_dptos_select();//Ejecuto el modulo
		$opcion = "<option id='dpto_rep0' name='dpto_rep0' value='0'>--Seleccione un Departamento--</option>";
		if($dptos[0][0]!="NO_DATA"){
			for($i=0;$i<count($dptos);$i++){
				$opcion.="<option id='dpto_rep".$i."' value='".$dptos[$i]["codigo_dpto"]."'>".$dptos[$i]["departamento"]."</option>";
			}
		}
		die($opcion);
	}
	//--Método que se encarga de generar consulta para tickets tecnicos
	public function reporte_ticket_tecnico($vector){
		$this->load->modelo('reporteticket');
		$resp = $this->reporteticket->consulta_ticket($vector);
		echo $resp;
	}
	//--Método que se encarga de consultar a los técnicos que no cuentan con tickets resueltos
	public function consulta_leyenda($vector){
		$obj = new allinoneHelper();
		$vector[0] = $obj->formato_fecha_pgsql($vector[0]);
		$vector[1] = $obj->formato_fecha_pgsql($vector[1]);
		$this->load->modelo('reporteticket');
		switch ($vector[2]) {
			case '1':
				$arr_leyenda = $this->reporteticket->consulta_notickets($vector);
				break;
			case '2':
				$arr_leyenda = $this->reporteticket->consulta_cuantos_tecnicos($vector);
				break;
		}
		//-
		if($arr_leyenda[0][0]!="NO_DATA"){
			for($i=0;$i<count($arr_leyenda);$i++){
				if( $i == 0) 
					$leyenda = $arr_leyenda[$i][0];
				else
					$leyenda.=", ".$arr_leyenda[$i][0];
			}
		}
		echo $leyenda;
	}
	//--
	//--Método que se encarga de consultar a los departamentos que no cuentan con tickets resueltos
	public function consulta_leyenda_dpto($vector){
		$obj = new allinoneHelper();
		$vector[0] = $obj->formato_fecha_pgsql($vector[0]);
		$vector[1] = $obj->formato_fecha_pgsql($vector[1]);
		$this->load->modelo('reporteticket');
		switch ($vector[2]) {
			case '1':
				$arr_leyenda = $this->reporteticket->consulta_notickets_dptos($vector);
				break;
			case '2':
				$arr_leyenda = $this->reporteticket->consulta_cuantos_dptos($vector);
				break;
		}
		//-
		if($arr_leyenda[0][0]!="NO_DATA"){
			for($i=0;$i<count($arr_leyenda);$i++){
				if( $i == 0) 
					$leyenda = $arr_leyenda[$i][0];
				else
					$leyenda.=", ".$arr_leyenda[$i][0];
			}
		}
		echo $leyenda;
	}
	//--Metodo que arma la tabla de consulta para gráfico #3
	public function cargar_tabla_leyenda_g3($vector){
		$this->load->vistaLogica('tablaleyenda');//cargo vista lógica tablaleyenda
		$this->load->plantilla('tabla_leyenda_pl');//cargo la plantilla tabla de la leyenda
		$this->load->modelo('reporteticket');//cargo el modelo
		//--
		$arreglo_data = array(
								"fecha_desde" => $vector[0],
								"fecha_hasta" => $vector[1],
								"select_tecnico" => $vector[2] 
						);
		//--
		//--Transformo el formato de arreglo data para la consulta
		$obj = new allinoneHelper();
		$arreglo_data["fecha_desde"] = $obj->formato_fecha_pgsql($arreglo_data["fecha_desde"]);
		$arreglo_data["fecha_hasta"] = $obj->formato_fecha_pgsql($arreglo_data["fecha_hasta"]);
		//--
		$tec_promedio = $this->reporteticket->consulta_ticket_promedio($arreglo_data);
		$this->tablaleyenda->render_vista(
			"tabla_leyenda_pl",
			$this->tabla_leyenda_pl,
			array(""),
			array(
					"tec_promedio" => $tec_promedio
				)
		);
	}
	//--Método que obtiene los datos del usuario que genera un reporte desde jsPDF
	public function encabezado_rp_js(){
		$datos_us[0] = $_SESSION["usuarios"];
		$datos_us[1] = date("d-m-Y H:i:s"); 
		die(json_encode($datos_us));
	}
	//----------------------------------------------------------------------------------------------------------------------------------
	//--Metodo para generar el reporte ticket_x_tecnico
	public function reporte_ticket_x_tecnico(){
		//--Recibo por post el valor de los campos
		$vars = allinoneHelper::get_vars($_POST);
			$arreglo_data = array(
				"fecha_desde" => $vars["fecha_desde"],
				"fecha_hasta" => $vars["fecha_hasta"],
				"select_tecnico" => $vars["select_tecnico"] 
			);
		$fechas_reporte =$arreglo_data["fecha_desde"]." / ".$arreglo_data["fecha_hasta"];	
		//--Armo encabezado:	
		$encabezado = $this->armar_encabezado($fechas_reporte, "REPORTE DE TICKETS ASIGNADOS");//Armo el encabezado
		//--Armo la leyenda
		$leyenda = $this->armar_leyenda();//Armo la leyenda
		//--Configuro el pdf
		$pdf = $this->configurar_pdf('L','250','cintillo_index.jpg');//Genero el reporte en pdf*/
		//--Transformo el formato de arreglo data para la consulta
		$obj = new allinoneHelper();
		$arreglo_data["fecha_desde"] = $obj->formato_fecha_pgsql($arreglo_data["fecha_desde"]);
		$arreglo_data["fecha_hasta"] = $obj->formato_fecha_pgsql($arreglo_data["fecha_hasta"]);
		//--Genero el reporte
		$html = $this->generar_reporte_html($arreglo_data,$encabezado,$leyenda,$pdf,$fechas_reporte);//Armo el html
		//--
	}
	//-------------------------------------------------------------------------------------------------------------------------------
	//--Metodo para generar reporte de ticket_x_dpto
	public function reporte_ticket_x_dpto(){
		//--Recibo por post el valor de los campos
		$vars = allinoneHelper::get_vars($_POST);
			$arreglo_data = array(
				"fecha_desde" => $vars["fecha_desde_dpto"],
				"fecha_hasta" => $vars["fecha_hasta_dpto"],
				"select_dpto" => $vars["select_dpto"]					
		);
		$fechas_reporte = $arreglo_data["fecha_desde"]."/".$arreglo_data["fecha_hasta"];
		//--Armo el encabezado:
		$encabezado = $this->armar_encabezado($fechas_reporte, "REPORTE DE TICKETS SOLUCIONADOS POR DEPARTAMENTO");//Armo el encabezado
		//--Armo la leyenda
		$leyenda = $this->armar_leyenda();//Armo la leyenda
		//--Configuro el pdf
		$pdf = $this->configurar_pdf('L','250','cintillo_index.jpg');//Genero el reporte en pdf...
		//-- Transformo el formato del arreglo data para la consulta
		$obj = new allinoneHelper();
		$arreglo_data["fecha_desde"] = $obj->formato_fecha_pgsql($arreglo_data["fecha_desde"]);
		$arreglo_data["fecha_hasta"]= $obj->formato_fecha_pgsql($arreglo_data["fecha_hasta"]);
		//--Genero el reporte segun departamentos:
		$html = $this->generar_reporte_html_dpto($arreglo_data,$encabezado,$leyenda,$pdf,$fechas_reporte);//Armo el html
		//---	
	}
	//--------------------------------------------------------------------------------------------------------------------------------
	//--Metodo que arma el encabezado
	public function armar_encabezado($fechas_reporte,$mensaje){
		$estructura_consulta='<div style="width:30%;height:10%;text-align:right;font-size:12px;font-weight:bold;">'.date("d-m-Y H:i:s").'<br>'.'IMPRESO POR: '.strtoupper($_SESSION["usuarios"]).'</div>
							   <div style="width:100%;text-align:center;font-size:16px;font-weight:bold;">'.$mensaje.'('.$fechas_reporte.')</div><br>';
		return $estructura_consulta;					   
	}
	//--Metodo que arma la leyenda
	public function armar_leyenda(){
		$estructura_consulta='<div style="">
								<div><label style="font-weight:bold;font-size:16px;">Leyenda:</label></div>
								<div><label style="font-weight:bold">TA:</label> Tiempo transcurrido desde la hora de creación del ticket hasta la hora de asignación.</div>
								<div><label style="font-weight:bold">TPRC: </label>Tiempo transcurrido desde la hora de creación del ticket hasta la hora de primera respuesta del técnico.</div>
								<div><label style="font-weight:bold">TPRA: </label>Tiempo transcurrido desde la hora de asignación del ticket hasta la hora de primera respuesta del técnico.</div>
							 </div>';
		return $estructura_consulta;
	}
	//--Metodo que genera resumen de tenicos x cantidad de tickets
	public function armar_resumen_cuantos($cuantos_tickets_x_tecnicos,$fechas_reporte){
		//--Mensaje antes de tabla
		$estructura_resumen = '<div style="font-size:16px;font-weight:bold">Resumen:</div>
								<div>En el rango de fechas seleccionado ( '.$fechas_reporte.' ) se presenta el siguiente resumen acerca de la cantidad de tickets atendidos por técnicos:</div>
		';
		//--Armo la estructura de la tabla
		$estructura_resumen.= '<br><table cellpadding="3px" border="1">
									<thead>
										<tr style="background-color:#066;color:#fff;">
											<th style="text-align:center;">Técnicos</th>
											<th style="width:30%;text-align:center">Cantidad de Tickets Atendidos</th>
										</tr>
									</thead>';

		for($i=0;$i<count($cuantos_tickets_x_tecnicos);$i++){
			$fg=$i+1;
			$estructura_resumen.='<tr>
										<td>'.$fg.'.-'.$cuantos_tickets_x_tecnicos[$i][1].'</td>
										<td style="width:30%;text-align:center;">'.$cuantos_tickets_x_tecnicos[$i][0].'</td>
								  </tr>';
		}
		$estructura_resumen.= '</table>';
		return $estructura_resumen;	
	}
	//--Metodo que genera reseumen de tecnicos con promedio de tpra
	public function armar_resumen_tpra($vector_promedio,$fechas_reporte){
		//--Mensaje antes de tabla
		$estructura_resumen_tpra = '<div style="font-size:16px;font-weight:bold">Resumen:</div>
								<div>En el rango de fechas seleccionado ( '.$fechas_reporte.' ) se presenta el siguiente resumen acerca del promedio de tiempo con respecto a la primera respuesta del técnico:</div>';
		//--Armo la estructura de la tabla...
		$estructura_resumen_tpra.= '<br><table cellpadding="3px" border="1">
									<thead>
										<tr style="background-color:#066;color:#fff;">
											<th style="width:30%;text-align:center;">Técnicos</th>
											<th style="width:20%;text-align:center">Tickets</th>
											<th style="width:20%;text-align:center">Total Horas</th>
											<th style="width:20%;text-align:center">(<span style=" text-decoration: overline">  X  </span>)  Promedio</th>
										</tr>
									</thead>';

		for($i=0;$i<count($vector_promedio);$i++){
			$fg=$i+1;
			$estructura_resumen_tpra.='<tr>
										<td style="width:30%;">'.$fg.'.-'.$vector_promedio[$i][1].'</td>
										<td style="width:20%;text-align:center;">'.$vector_promedio[$i][0].'</td>
										<td style="width:20%;text-align:center;font-size:14;font-weight:bold">'.$vector_promedio[$i][2].'</td>
										<td style="width:20%;text-align:center;font-size:15;font-weight:bold">'.$vector_promedio[$i][3].'</td>
									</tr>';
		}
		$estructura_resumen_tpra.= '</table>';
		return $estructura_resumen_tpra;
	}
	//--Metodo que arma la estructura del pdf de ticket solucionados por departamento
	public function generar_reporte_html_dpto($arreglo_data,$encabezado,$leyenda,$pdf,$fechas_reporte){
		//--Bloque para configurar el reporte
		//--Establecer la fuente
		$pdf->SetFont('times','',12);
		//-- Añadir una página
		$pdf->AddPage();
		//--Muestro el encabezado
		$pdf->writeHTML($encabezado, true, 0, true, 0);
		//--Bloque para armar HTML
		//--Consulta de tickets
		$this->load->modelo('reporteticket');//
		$vector = $this->reporteticket->consultar_tickets_departamentos($arreglo_data);
		//--
		$x=0;
		$i=0;
		$j=0;
		$cont2 = 0;
		$fecha_suma = "";
		$estructura_consulta = "";
		$cuantos_tickets_x_dpto = array();
		$obj = new allinoneHelper();//Cargo el helper
		if($vector!="NO_DATA"){
		//
			$solicitud[0] = $vector[0]["descripcion_solicitud"];
			$cantidad[0] = 1;
			while($x<count($vector)){
				if($x>0){
				//--Añadir una pagina
					$pdf->AddPage();	
				}
				//-------------------------------------------------------------------------------
				$estructura_consulta.='<table cellpadding="3px">
										<thead>
											<tr><td style="font-weight:bold">DEPARTAMENTO: '.strtoupper($obj->remplazar_acentos_min($vector[$i]["nombre_departamento"])).'</td></tr>
											<tr style="background-color:#066;color:#fff;" >
												<th  border="1"  style="width:40px;text-align:center;">#</th>
												<th  border="1" style="width:270px;text-align:center">Descripción</th>
												<th  border="1" style="width:230px;text-align:center;">Usuario Solicitante</th>
												<th border="1" style="width:230px;text-align:center;">Técnico Responsable</th>
												<th  border="1" style="width:100px;text-align:center;">Fecha de Creación</th>
												<th border="1" style="width:100px;text-align:center;">Fecha de Solución</th>
											</tr>
										</thead>';
				//-------------------------------------------------------------------------------
				$arreglo_data["codigo_departamento"] = $vector[$i]["codigo_departamento"];//--Para devolver el departamento anterior
				//--Cargo método que desarrolla tabla resumén...
				$resumen_ticket_dpto = $this->armar_resumen_tickets_dpto($arreglo_data,$fechas_reporte);
				//-------------------------------------------------------------------------------
				if($x==18)
				{
					$solicitud[$j] = "Here";
				}
				//--
				while($i<count($vector)){
				//-------------------------------------------------------------------------------
					$id_ant = $vector[$i]["codigo_departamento"];
					$estructura_consulta.='<tr>
												<td  border="1"  style="width:40px;text-align:center;">'.$vector[$i]["id_ticket"].'</td>
												<td  border="1"  style="width:270px;"><b>'.$vector[$i]["descripcion_solicitud"].':</b> '.ucfirst($vector[$i]["descripcion_ticket"]).'</td>
												<td  border="1"  style="width:230px;">'.$vector[$i]["datos_usuarios"].'</td>
												<td  border="1"  style="width:230px;">'.$vector[$i]["nombre_tecnico"].'</td>
												<td  border="1"  style="width:100px;text-align:center">'.$vector[$i]["tiempo_creacion"].'</td>
												<td border="1" style="width:100px;text-align:center">'.$vector[$i]["tiempo_solucion"].'</td>
										  </tr>';//<div>'.$fecha_suma.'</div><br>';
				//-------------------------------------------------------------------------------	
					$x++;
					$i++;
					//-- Si el código del departamento es diferente al departamento anterior se rompe el ciclo
					if(isset($vector[$i]["codigo_departamento"])){
					//--	
						if($id_ant != $vector[$i]["codigo_departamento"])
						{
							break;
						}
					//--	
					}
				//-------------------------------------------------------------------------------
				}
					$estructura_consulta.="</table>";
					//--
					//--Guardo el nombre del departamento
					//--Para el resumen de tickets x departamento
					$cuantos_tickets_x_dpto[$j][1] = $vector[$i-1]["codigo_departamento"];
					//--Muestro la estructura del HMLT generado
					$pdf->writeHTML($estructura_consulta, true, false, false,false,'');//Para escribir el código HTML
					$estructura_consulta = "";
					//--
					$pdf->writeHTML($resumen_ticket_dpto, true,false, false,false,'');
					$cont2++;
					//--
			}
			//$pdf->writeHTML($leyenda, true, 0, true, 0);
			//--Cargo metodo para armar resumen 1: Tecnicos por cantidad de tickets....
			//$resumen_ticket_x_tecnico = $this->armar_resumen_cuantos($cuantos_tickets_x_tecnicos,$fechas_reporte);
			//$pdf->writeHTML($resumen_ticket_x_tecnico, true,false, false,false,'');
			//--Cargo método para armar resumen 2: Técnicos con promedio de horas segun tpra
			//$resumen_tpra = $this->armar_resumen_tpra($vector_promedio,$fechas_reporte);
			//$pdf->writeHTML($resumen_tpra, true,false, false,false,'');
					//--Resumen de departamento
			if(($arreglo_data["select_dpto"]=="0")&&($cont2>1)){
				$resumen_general_dpto = $this->consultar_general_tickets($arreglo_data,$fechas_reporte);
				$pdf->writeHTML($resumen_general_dpto,true,false,false,false,'');		
			}			
		}else{
			$estructura_consulta = '<div style="width:400px;"><span style="font-size:20px;text-align:center;"> No se encontraron datos asociados a la consulta realizada</span></div>';
			$pdf->writeHTML($estructura_consulta, true, false, false,false,'');//Para escribir el código HTML
			$estructura_consulta = "";
		}
		//----------------------------------------------------------------------------------
		//--output the HTML content
		//Cerramos y damos salida al fichero PDF
		$pdf->Output('tickets_por_departamentos.pdf', 'I');
		//-----------------------------------------------------------------------------------
	}
	//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
	public function consultar_general_tickets($arreglo_data,$fechas_reporte){
		//Resumen de tabla
		$estructura_resumen = '<div style="font-size:16px;font-weight:bold">Resumen:</div>
								<div>En el rango de fechas seleccionado ('.$fechas_reporte.') se presenta el siguiente resumen acerca de la cantidad de tickets atendidos por departamento y tipo de solicitud:</div><br>';
		//--
		$tabla_resumen_dpto ='<table cellpadding="3">
									<thead>
										<tr style="background-color:#066;color:#fff;">
											<th border="1"  style="width:300px;text-align:center;">Departamento</th>
											<th border="1"  style="width:300px;text-align:center;">Tipo Solicitud</th>
											<th border="1"  style="width:300px;text-align:center;">Cantidad Tickets Atendidos</th>
										</tr>
									</thead>
									<tbody>';
		//--Incluyo el modelo
		$this->load->modelo('reporteticket');
		$arreglo_gen = $this->reporteticket->consultar_general_tickets($arreglo_data);
		for($i=0;$i<count($arreglo_gen);$i++){
			$tabla_resumen_dpto.='<tr>
										<td border="1" style="width:300px;text-align:center">'.$arreglo_gen[$i]["departamento"].'</td>
										<td border="1" style="width:300px;text-align:center">'.$arreglo_gen[$i]["tipo_solicitud"].'</td>
										<td border="1" style="width:300px;text-align:center">'.$arreglo_gen[$i]["cantidad_atendidos"].'</td>
								  </tr>';
			$total = $total+$arreglo_gen[$i]["cantidad_atendidos"];
		}
		$tabla_resumen_dpto.='<tr>
								<td border="1" rowspan="4" style="width:900px;text-align:center;font-weight:bold">Total Tickets Atendidos: '.$total.'</td>
							  </tr></tbody></table>';
		$estructura_resumen.=$tabla_resumen_dpto;
		return $estructura_resumen;
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------
	public function armar_resumen_tickets_dpto($arreglo_data,$fechas_reporte){
		//Resumen de tabla
		$estructura_resumen = '<div style="font-size:16px;font-weight:bold">Resumen:</div>
								<div>En el rango de fechas seleccionado ('.$fechas_reporte.') se presenta el siguiente resumen acerca de la cantidad de tickets atendidos por tipo de solicitud:</div><br>';
		//--
		$tabla_resumen_dpto ='<table cellpadding="3">
									<thead>
										<tr style="background-color:#066;color:#fff;">
											<th border="1"  style="width:400px;text-align:center;">Tipo Solicitud</th>
											<th border="1"  style="width:400px;text-align:center;">Cantidad Tickets Atendidos</th>
										</tr>
									</thead>
									<tbody>';						
		//Incluyo el modelo
		$this->load->modelo('reporteticket');//
		$arreglo_cnt = $this->reporteticket->consultar_cantidad_tickets_dpto($arreglo_data);
		for($i=0;$i<count($arreglo_cnt);$i++){
			$tabla_resumen_dpto.='<tr>
										<td border="1" style="width:400px;text-align:center">'.$arreglo_cnt[$i]["tipo_solicitud"].'</td>
										<td border="1" style="width:400px;text-align:center">'.$arreglo_cnt[$i]["cantidad_atendidos"].'</td>
								  </tr>';
			$total = $total+$arreglo_cnt[$i]["cantidad_atendidos"];					  
		}
			$tabla_resumen_dpto.='<tr>
									<td border="1" rowspan="2" style="width:800px;text-align:center;font-weight:bold">Total Tickets Atendidos en este departamento: '.$total.'</td>
								 </tr></tbody></table>';
		$estructura_resumen.=$tabla_resumen_dpto;
		//--
		return $estructura_resumen;
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------	
	//--Metodo que arma la estructura del pdf
	public function generar_reporte_html($arreglo_data,$encabezado,$leyenda,$pdf,$fechas_reporte){
		//--Bloque para configurar el reporte
		//--Establecer la fuente
		$pdf->SetFont('times', '', 12);
		//--Añadir una página
		$pdf->AddPage();
		//--Muestro el encabezado
		$pdf->writeHTML($encabezado, true, 0, true, 0);//Para escribir la Leyenda
		//--Bloque para armar HTML
		//--Consulta de tickets
		$this->load->modelo('reporteticket');//Cargo el modelo que realiza la consulta
		$vector = $this->reporteticket->consulta_ticket($arreglo_data);//Cargo el metodo que se encarga de consultar los tickets
		//--
		$x=0;
		$i=0;
		$j=0;
		$fecha_suma="";
		$estructura_consulta = "";
		$cuantos_tickets_x_tecnicos =  array();
		$vector_promedio = array();
		$obj = new allinoneHelper();//Cargo helper
		if($vector!="NO_DATA")
		{
			while($x<count($vector))
			{
				if($x>0){
				//--Añadir una página
					$pdf->AddPage();
				}	
				//--
				$estructura_consulta.='<table cellpadding="3px">
										<thead>
										<tr><td style="font-weight:bold">ASIGNADOS A '.strtoupper($obj->remplazar_acentos_acute($vector[$i]["tecnico_asignado"])).'</td></tr>
										<tr style="background-color:#066;color:#fff;" >
											<th  border="1"  style="width:40px;text-align:center;">#</th>
											<th  border="1" style="width:270px;text-align:center">Descripción</th>
											<th  border="1" style="width:250px;text-align:center;">Usuario Solicitante</th>
											<th  border="1"  style="width:120px;text-align:center;">Tiempo de Creación</th>
											<th  border="1" style="width:120px;text-align:center;">Tiempo de Asignación</th>
											<th  border="1"  style="width:120px;text-align:center;">Tiempo de Primera Respuesta</th>
										</tr>
										</thead>';
				//--
				while($i<count($vector)){
				//---------------------------------	
					//--Calculo de tiempos...
					//--calculando ta:
					$fecha_inicio_ta = $vector[$i]["tiempo_creacion2"];
					$fecha_fin_ta = $vector[$i]["tiempo_asignacion2"];
					$ta = $obj->calcula_tiempo($fecha_inicio_ta,$fecha_fin_ta);//tiempo transcurrido desde la creación del ticket hasta ser asignado...
					//--calculando tprc:
					$fecha_inicio_tprc = $vector[$i]["tiempo_creacion2"];
					$fecha_fin_tprc = $vector[$i]["tiempo_primera_respuesta2"];
					$tprc = $obj->calcula_tiempo($fecha_inicio_tprc,$fecha_fin_tprc);//tiempo transcurrido desde la creación hasta la primera respuesta...
					//--calculando tpra:
					$fecha_inicio_tpra = $vector[$i]["tiempo_asignacion2"];
					$fecha_fin_tpra = $vector[$i]["tiempo_primera_respuesta2"];
					$tpra = $obj->calcula_tiempo($fecha_inicio_tpra,$fecha_fin_tpra);//tiempo transcurrido desde la asignación hasta la primera respuesta...
					//--Debe obtenerse la suma
					if($fecha_suma==''){
						$fecha_suma = $tpra;
					}else
					{
						$fecha_suma = $obj->calcular_suma($fecha_suma,$tpra);
					}  
					//--	
				//--------------------------------
					$id_ant = $vector[$i]["id_tecnico"];	
					$estructura_consulta.='<tr>
												<td  border="1"  style="width:40px;text-align:center;">'.$vector[$i]["id_ticket"].'</td>
												<td  border="1"  style="width:270px;"><b>'.ucfirst($vector[$i]["descripcion_solicitud"]).':</b> '.ucfirst($vector[$i]["desc_ticket100"]).'</td>
												<td  border="1"  style="width:250px;">'.$vector[$i]["datos_usuarios"].'</td>
												<td  border="1"  style="width:120px;text-align:center">'.$vector[$i]["tiempo_creacion"].'</td>
												<td  border="1" style="width:120px;text-align:center">'.$vector[$i]["tiempo_asignacion"].' <b>TA=</b>'.$ta.'</td>
												<td  border="1" style="width:120px;text-align:center">'.$vector[$i]["tiempo_primera_respuesta"].' <b>TPRC</b>='.$tprc.' <b>TPRA=</b>'.$tpra.'</td>
										  </tr>';//<div>'.$fecha_suma.'</div><br>';
				//--------------------------------	
					$x++;
					$i++;
					//--Para calcular cantidades de tickets x tecnico
					//--Incremento por cada ticket... 
					if(isset($cuantos_tickets_x_tecnicos[$j][0]))
					{
						$cuantos_tickets_x_tecnicos[$j][0] = $cuantos_tickets_x_tecnicos[$j][0]+1;	
					}else
					{
						$cuantos_tickets_x_tecnicos[$j][0] = 1;
					}
					//-- Si el ID del tecnico es diferente al tecnico anterior se 
					if(isset($vector[$i]["id_tecnico"]))
					{
						if($id_ant != $vector[$i]["id_tecnico"])
						{
							break;
						}
					}
					//--
				}
				//---------------------------------
					$estructura_consulta.="</table><";
					//--Guardo el nombre del técnico
					//--Para el resumen de tickets x tecnico
					$cuantos_tickets_x_tecnicos[$j][1] = $vector[$i-1]["tecnico_asignado"];
					//--Para el vector resumen de tecnicos y su promedio de primera respuesta
					$vector_promedio[$j][1] = $vector[$i-1]["tecnico_asignado"]; 
					//$vector_promedio[$j][0] = "Total Horas:".$fecha_suma."-> Promedio:".$obj->calcular_promedio_horas($fecha_suma,$cuantos_tickets_x_tecnicos[$j][0]);
					$vector_promedio[$j][2] = $fecha_suma;
					$vector_promedio[$j][0] = $cuantos_tickets_x_tecnicos[$j][0];
					$vector_promedio[$j][3] = $obj->calcular_promedio_horas($fecha_suma,$cuantos_tickets_x_tecnicos[$j][0]);
					$fecha_suma = "";
					$j++;	
					//--Muestro la estructura del HTML generado
					$pdf->writeHTML($estructura_consulta, true, false, false,false,'');//Para escribir el código HTML
					$estructura_consulta = "";
				//--
			}
			//$pdf->writeHTML($tickets_resueltos, true, false, false,false,'');//Para escribir el código HTML
			//--Para la leyenda
			$pdf->writeHTML($leyenda, true, 0, true, 0);
			//--Cargo metodo para armar resumen 1: Tecnicos por cantidad de tickets....
			$resumen_ticket_x_tecnico = $this->armar_resumen_cuantos($cuantos_tickets_x_tecnicos,$fechas_reporte);
			$pdf->writeHTML($resumen_ticket_x_tecnico, true,false, false,false,'');
			//--Cargo método para armar resumen 2: Técnicos con promedio de horas segun tpra
			$resumen_tpra = $this->armar_resumen_tpra($vector_promedio,$fechas_reporte);
			$pdf->writeHTML($resumen_tpra, true,false, false,false,'');
		}
		else
		{
			$estructura_consulta = '<div style="width:400px;"><span style="font-size:20px;text-align:center;"> No se encontraron datos asociados a la consulta realizada</span></div>';
			$pdf->writeHTML($estructura_consulta, true, false, false,false,'');//Para escribir el código HTML
			$estructura_consulta = "";
		}	
		//----------------------------------------------------------------------------------
		//--output the HTML content
		//Cerramos y damos salida al fichero PDF
		$pdf->Output('tickets_x_tecnicos.pdf', 'I');
		//-----------------------------------------------------------------------------------
	}
	//--Metodo que genera el pdf a través tcpdf
	public function configurar_pdf($orientacion,$width_header,$ruta_header){
		$pdf = new TCPDF($orientacion, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		// Informacion propia del pdf
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Gianni Santucci');
		$pdf->SetTitle('');
		$pdf->SetSubject('Helpdesk');
		$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		
		// Contenido del Header
		$pdf->SetHeaderData($ruta_header, $width_header,'','',array(0,0,0), array(0,96,99));

		//Contenido del Footer
		$pdf->SetFooterData(array(0,0,0),array(0,96,99));

		// Fuente cabecera y pie de página
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		
		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		// saltos de página automáticos
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// Se establece ratio de imagenes a utilizar
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		//--
		return $pdf;
	}
	//--Metodo que se encarga de consultar datos de tickets resueltos por técnicos, para desarrollo de gráfica...
	public function consultar_tickets_tecnicos_g1($vector){
		$this->load->modelo('reporteticket');//Cargo el modelo que contiene la consulta de los tecnicos
		//--
		$obj = new allinoneHelper();
		$vector[0] = $obj->formato_fecha_pgsql($vector[0]);
		$vector[1] = $obj->formato_fecha_pgsql($vector[1]);
		//--
		$tickets_tecnicos = $this->reporteticket->consultar_tickets_tecnicos($vector);//Ejecuto el metodo	
		$rows = array();
		for($i=0;$i<count($tickets_tecnicos);$i++){
			$row[0] = $tickets_tecnicos[$i][0];  
			$row[1] = $tickets_tecnicos[$i][1];
			$row[2] = $tickets_tecnicos[$i][2];
			array_push($rows,$row);
		}
		print(json_encode($rows,JSON_NUMERIC_CHECK));
	}
	//--Metodo que se encarga de consultar los tickets resueltos por departamento, para desarrollo de gráfica...
	public function consultar_tickets_deptos_g1($vector){
		$this->load->modelo('reporteticket');//Cargo el modelo que contiene la consulta de los tecnicos
		//--
		$obj = new allinoneHelper();
		$arreglo_data["fecha_desde"] = $obj->formato_fecha_pgsql($vector[0]);
		$arreglo_data["fecha_hasta"] = $obj->formato_fecha_pgsql($vector[1]);
		//--
		$tickets_dptos = $this->reporteticket->consultar_cantidad_tigr($arreglo_data);//Ejecuto el metodo	
		$rows = array();
		for($i=0;$i<count($tickets_dptos);$i++){
			$row[0] = $tickets_dptos[$i][0];  
			$row[1] = $tickets_dptos[$i][1];
			$row[2] = $tickets_dptos[$i][2];
			array_push($rows,$row);
		}
		print(json_encode($rows,JSON_NUMERIC_CHECK));
	}
	//--Metodo que se encarga de cponsultar datos de tickets por técnicos para realizar el cálculo de promedios de tiempos
	public function consultar_tickets_tecnicos_g3($vector){
	//-- Armo arreglo de los valores recibidos	
		$arreglo_data = array(
			"fecha_desde" => $vector[0],
			"fecha_hasta" => $vector[1],
			"select_tecnico" => $vector[2] 
		);
	//-- Transformo las fechas en formato YYYY-mm-dd
	$obj = new allinoneHelper();
	//--
		$arreglo_data["fecha_desde"] = $obj->formato_fecha_pgsql($arreglo_data["fecha_desde"]);
		$arreglo_data["fecha_hasta"] = $obj->formato_fecha_pgsql($arreglo_data["fecha_hasta"]);
	//--		
		$this->load->modelo('reporteticket');//Cargo el modleo que contiene la consulta de los tecnicos
		$tickets_tecnicos = $this->reporteticket->consulta_ticket_promedio($arreglo_data);//Cargo el metodo que se encarga de consultar los tickets
		for($i=0;$i<count($tickets_tecnicos);$i++){
			//$arreglo[] = array("nombre"=>$tickets_tecnicos[$i][1],"total_horas"=>$tickets_tecnicos[$i][2],"total_promedio_horas"=>$tickets_tecnicos[$i][3],"total_segundos"=>number_format($tickets_tecnicos[$i][4],2,",","."),"total_promedio_segundos"=>number_format($tickets_tecnicos[$i][5],2,",","."));
			$arreglo[] = array("nombre"=>$tickets_tecnicos[$i][1],"total_horas"=>$tickets_tecnicos[$i][2],"total_promedio_horas"=>$tickets_tecnicos[$i][3],"total_segundos"=>number_format($tickets_tecnicos[$i][4],2,",",""),"total_promedio_segundos"=>number_format($tickets_tecnicos[$i][5],2,",",""));
		}
		die(json_encode($arreglo));
	//--	
	}
}
?>