<?php
class procesoauditoriaController extends controladorBase{
	//Metodo index
	public function index(){
	}
	//Metodo para cargar la consulta en tabla con su respectiva paginacion
	public function cargar_auditoria($vector){
		$this->load->vistaLogica('auditoriasistema');//cargo la vista lógica admintickets
//		$this->load->vistaGrafica('listauditoria');//cargo la vista grafica
		$this->load->plantilla("listauditoria");
		$obj = $this->load->modelo('carauditoria');//cargo el modelo caradmintickets
		//Validación de cuantos_son
		if(($vector[1]==0)||($vector[3]==3))
		{
			$cuantos_son = $this->carauditoria->cuantos_auditoria();//Determino cuantos ticket_son
			$cuantos_son = $cuantos_son[0][0];
		}				
		else
			$cuantos_son = $vector[1];//Determino cuantos ticket_son
		//--Calculo de a que página debe ir
		switch ($vector[3]){
			case 0:
					$offset=0;
					///Calculo limites:
					$this->calculo_limites($offset,$cuantos_son,1,$vector[2]);
			break;		
			case 1:
					$offset=$vector[0]+$vector[2];
					///Calculo limites:
					$this->calculo_limites($offset,$cuantos_son,1,$vector[2]);
			break;
			case 2:
					$offset=$vector[0]-$vector[2];
					///Calculo limites:
					$this->calculo_limites($offset,$cuantos_son,2,$vector[2]);
			break;
			case 3:
					$offset=$vector[0];
					//--
					if($vector[0]==$cuantos_son)
						$offset=0;
					//--
					$this->calculo_limites($offset,$cuantos_son,1,$vector[2]);
			break;		
		}
		$clase_sig='';
		$clase_ant='';
		//Para ocultar siguiente 
		$offset_sig = $offset+$vector[2];
		if(($offset_sig == $cuantos_son)||($offset_sig > $cuantos_son ))
			$clase_sig = "disabled";
		//Parea ocultar anterior
		if($offset == 0)
			$clase_ant = "disabled";

		$auditoria = $this->carauditoria->consultar_auditoria($offset, $vector[2]);//obtener los tickets segun parametros de paginacion
		//
		//Validar si tiene o no tickets
		if($cuantos_son>0){
				$clase_tabla = 'show';
				$clase_audi = 'hide';
		}else{
				$clase_tabla = 'hide';
				$clase_audi = 'show';
		}
		$this->auditoriasistema->render_vista(
			"listauditoria",
			$this->listauditoria,
			array(
					"paginador_siguiente"=>"ir_tabla_audit(".$offset.",".$cuantos_son .",".$vector[2].",1);",
					"paginador_anterior"=>"ir_tabla_audit(".$offset.",".$cuantos_son .",".$vector[2].",2);",
					"clase_paginador_siguiente"=>$clase_sig,
					"clase_paginador_anterior"=>$clase_ant,
					"offset_tabla"=>$offset,
					"cuantos_tabla"=>$cuantos_son,
					"inicio_tabla"=>$this->inicio_tabla,
					"fin_tabla"=>$this->fin_tabla,
					"clase_tabla"=>$clase_tabla,
					"clase_audi"=>$clase_audi 
				),
			array(
					"auditoria"=>$auditoria
				)
			);
	}
	//Metodo calculo limites
	public function calculo_limites($offset,$cuantos_son,$tipo,$cuantos_x_pag){
			$this->inicio_tabla = $offset+1;
			//Valido que si $offset=0 se inicie la tabla en valor 0
			if($cuantos_son==0){
				$this->inicio_tabla = 0;
			}
			$valor = $offset+$cuantos_x_pag;
			if($valor >= $cuantos_son)
				$this->fin_tabla = $cuantos_son;
			else
				$this->fin_tabla = $valor;		
	}
}
?>