<?php
class tablasolController extends controladorBase{
	public function __construct(){
		parent::__construct();
	}
	public function index(){
	}
	//Metodo calculo limites
	public function calculo_limites($offset,$cuantos_son,$tipo){
			$this->inicio_tabla = $offset+1;
			//Valido que si $offset=0 se inicie la tabla en valor 0
			if($cuantos_son==0){
				$this->inicio_tabla = 0;
			}
			$valor = $offset+50;
			if($valor >= $cuantos_son)
				$this->fin_tabla = $cuantos_son;
			else
				$this->fin_tabla = $valor;		
		}
	//Metodo cargar_pag_sol	
	public function cargar_pag_sol($vector){
		$this->load->vistaLogica("listsolticket");//cargo la viosta logica
		$this->load->vistaGrafica("cargasolticket");//cargo la vista grafica
		$this->load->modelo("solticket");//cargo el modelo
		//Validación de cuantos_son
		if(($vector[1]==0)||($vector[3]==3))
		{
			$cuantos_son = $this->solticket->cuantas_solicitudes();//Determino cuantos ticket_son
			$cuantos_son = $cuantos_son[0][0];
		}				
		else
			$cuantos_son = $vector[1];//Determino cuantos ticket_son
		//--Calculo de a que página debe ir
		switch ($vector[3]){
			case 0:
					$offset=0;
					///Calculo limites:
					$this->calculo_limites($offset,$cuantos_son,1);
			break;		
			case 1:
					$offset=$vector[0]+$vector[2];
					///Calculo limites:
					$this->calculo_limites($offset,$cuantos_son,1);
			break;
			case 2:
					$offset=$vector[0]-$vector[2];
					///Calculo limites:
					$this->calculo_limites($offset,$cuantos_son,2);
			break;
			case 3:
					$offset=$vector[0];
					//--
					if($vector[0]==$cuantos_son)
						$offset=0;
					//--
					$this->calculo_limites($offset,$cuantos_son,1);
			break;
		}	
		//---
		$offset_sig = $offset+$vector[2];
			$clase_sig = "";
			if(($offset_sig == $cuantos_son)||($offset_sig > $cuantos_son))
				$clase_sig = "disabled";
			//Parea ocultar anterior
			if($offset == 0)
				$clase_ant = "disabled";
		//---
		//Validar si tiene o no tickets
		if($cuantos_son>0){
				$clase_tabla = 'show';
				$clase_tickets = 'hide';
		}else{
				$clase_tabla = 'hide';
				$clase_tickets = 'show';
		}
		//---Bloque de permisologías
			$this->load->modelo('permisologias');//cargo el modelo
			$obj2 = new allinoneHelper();//Cargo helper
			$sol_permisos = $this->permisologias->permiso_pantalla(5);//Metodo que consulta si el usuario tiene permisos en pantalla solución tickets
			$vector_permisos = $obj2->array_plpgsql_to_php($sol_permisos[0][0]);///Metodo que transforma arreglo plpgsql en array php
		//--	
		$solticket = $this->solticket->cargar_lista_soltickets($offset,$vector[2]);//ejecuto el metodo de la lista de soluciones de los tickets
		$this->listsolticket->render_vista(
			"cargasolticket",
			$this->cargasolticket,
			array(
					"paginador_siguiente"=>"ir_tabla_sol(".$offset.",".$cuantos_son.",50,1);",
					"paginador_anterior"=>"ir_tabla_sol(".$offset.",".$cuantos_son.",50,1)",
					"clase_paginador_siguiente"=>$clase_sig,
					"clase_paginador_anterior"=>$clase_ant,
					"offset_tabla"=>$offset,
					"cuantos_tabla"=>$cuantos_son,
					"inicio_tabla"=>$this->inicio_tabla,
					"fin_tabla"=>$this->fin_tabla,
					"clase_tabla"=>$clase_tabla,
					"clase_tickets"=>$clase_tickets
				),
			array(
					"solticket" 	  => $solticket,
					"vector_permisos" => $vector_permisos
				 )
		);
		//---
	}
	//Metodo para cargar apelaciones soluciones en la pantalla
	public function cargar_pag_apl_sol($vector){
		$this->load->vistaLogica("listapelsolticket");//cargo la vista logica
		$this->load->vistaGrafica("cargapelticket");//cargo la vista grafica
		$this->load->modelo("solticket");//cargo el modelo
		//Validación de cuantos_son
		if(($vector[1]==0)||($vector[3]==3))
		{
			$cuantos_son = $this->solticket->cuantas_solicitudes_apel();//Determino cuantos ticket_son
			$cuantos_son = $cuantos_son[0][0];
		}				
		else
			$cuantos_son = $vector[1];//Determino cuantos ticket_son
		//--Calculo de a que página debe ir
		switch ($vector[3]){
			case 0:
					$offset=0;
					///Calculo limites:
					$this->calculo_limites($offset,$cuantos_son,1);
			break;		
			case 1:
					$offset=$vector[0]+$vector[2];
					///Calculo limites:
					$this->calculo_limites($offset,$cuantos_son,1);
			break;
			case 2:
					$offset=$vector[0]-$vector[2];
					///Calculo limites:
					$this->calculo_limites($offset,$cuantos_son,2);
			break;
			case 3:
					$offset=$vector[0];
					//--
					if($vector[0]==$cuantos_son)
						$offset=0;
					//--
					$this->calculo_limites($offset,$cuantos_son,1);
			break;
		}	
		//---
		$offset_sig = $offset+$vector[2];
			$clase_sig = "";
			if(($offset_sig == $cuantos_son)||($offset_sig > $cuantos_son))
				$clase_sig = "disabled";
			//Parea ocultar anterior
			if($offset == 0)
				$clase_ant = "disabled";
		//---
		//Validar si tiene o no tickets
		if($cuantos_son>0){
				$clase_tabla = 'show';
				$clase_tickets = 'hide';
		}else{
				$clase_tabla = 'hide';
				$clase_tickets = 'show';
		}
		//---Bloque de permisologías
			$this->load->modelo('permisologias');//cargo el modelo
			$obj2 = new allinoneHelper();//Cargo helper
			$sol_permisos = $this->permisologias->permiso_pantalla(5);//Metodo que consulta si el usuario tiene permisos en pantalla solución tickets
			$vector_permisos = $obj2->array_plpgsql_to_php($sol_permisos[0][0]);///Metodo que transforma arreglo plpgsql en array php
		//--	
		$solapel = $this->solticket->cargapelticket($offset,$vector[2]);//ejecuto el metodo de la lista de soluciones de los tickets
		$this->listapelsolticket->render_vista(
			"cargapelticket",
			$this->cargapelticket,
			array(
					"paginador_siguiente"=>"ir_tabla_apl_sol(".$offset.",".$cuantos_son.",50,1);",
					"paginador_anterior"=>"ir_tabla_apl_sol(".$offset.",".$cuantos_son.",50,1)",
					"clase_paginador_siguiente"=>$clase_sig,
					"clase_paginador_anterior"=>$clase_ant,
					"offset_tabla"=>$offset,
					"cuantos_tabla"=>$cuantos_son,
					"inicio_tabla"=>$this->inicio_tabla,
					"fin_tabla"=>$this->fin_tabla,
					"clase_tabla"=>$clase_tabla,
					"clase_tickets"=>$clase_tickets
				),
			array(
					"solapel" => $solapel,
					"vector_permisos" => $vector_permisos
				 )
		);
		//---
	}
	//Metodo registrar sol
	public function registrar_solucion($vector){
		$this->load->modelo("solticket");//Cargo el modelo
		$objeto = $this->solticket->reg_solucion($vector);//Cargo el metodo
		//--Si registra efectivamente la solucion, enviar el correo
		if($objeto[0][0]==1){
			$result = $this->enviar_correo_solucion($vector[0]);
			if($result==-1){
				echo $result;
			}
		}
		//--
		print $objeto[0][0];
	}
	//Metodo registrar apelación
	public function registrar_solucion_apel($vector){
		$this->load->modelo("solticket");//Cargo el modelo
		$objeto = $this->solticket->reg_solapel_ticket($vector);//Cargo el metodo
		//--Si registra efectivamente la solucion, enviar el correo
		if($objeto[0][0]==1){
			$result = $this->enviar_correo_solucion_apelacion($vector[0],$vector[1],$vector[2]);
			if($result==-1){
				echo $result;
			}
		}
		//--
		print $objeto[0][0];
	}
	//Metodo para realizar envio de email
	public function enviar_correo_solucion($n_ticket){
		$this->load->modelo('correo');//Cargo el modelo correo
		$titulo = "Solución al requerimiento solicitado, Técnico ".utf8_decode($_SESSION["nombre"]);//Título del email
		$titulo = utf8_decode($titulo);
		$encabezado = "Se di&oacute; solucion a su requerimiento, t&eacute;cnico ".utf8_decode($_SESSION["nombre"])." : ";
		//$correo_sistema = "gsantucci@avilatv.gob.ve";// aqui va el correo del sistema
		$correo_sistema = "soporte@avilatv.gob.ve";// aqui va el correo del sistema
		//--Cargo metodo para consultar: destinatario, descripcion del ticket
		$vector_datos = $this->correo->correo_datos_correo($n_ticket,$_SESSION["cedula"]);//
		//--
		$mensaje = $this->correo->crear_cuadro($titulo,$encabezado,utf8_decode($vector_datos[0][1]),$n_ticket);//armo la estructura del cuadro del mensaje
		$resp2 = $this->correo->enviar_correo($correo_sistema,$vector_datos[0][0],$mensaje,$titulo);//envío el correo
		echo $resp;
	}
	//Metodo para realizar envio de email para apelaciones
	public function enviar_correo_solucion_apelacion($n_apel,$sol_apel,$n_ticket){
		$this->load->modelo('correo');//Cargo el modelo correo
		$titulo = "Solución a la apelación, Técnico ".utf8_decode($_SESSION["nombre"]);//Título del email
		$titulo = utf8_decode($titulo);
		$encabezado = "Se di&oacute; solucion a su apelaci&oacute;n, t&eacute;cnico ".utf8_decode($_SESSION["nombre"])." : ";
		//$correo_sistema = "gsantucci@avilatv.gob.ve";// aqui va el correo del sistema
		$correo_sistema = "soporte@avilatv.gob.ve";// aqui va el correo del sistema
		//--Cargo metodo para consultar: destinatario
		$vector_datos = $this->correo->correo_datos_apelacion_solucion($n_apel);//
		//--
		$mensaje = $this->correo->crear_cuadro_apl($titulo,$encabezado,utf8_decode($sol_apel),$n_apel,$n_ticket);//armo la estructura del cuadro del mensaje
		$resp2 = $this->correo->enviar_correo($correo_sistema,$vector_datos[0][0],$mensaje,$titulo);//envío el correo
		echo $resp;
	}
}	
?>