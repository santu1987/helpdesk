<?php

	class panelController extends controladorBase{

		public function __construct(){
			parent::__construct();
		}

		public function index(){
			//print "MOSTRANDO 'panel'";
			$this->load->vistaLogica('panel');
			$this->load->vistaGrafica('panelUs');
			$this->panel->render_vista(
				"panelUs",
				$this->panelUs,
				array(),
				array()
				);
		}
	}
?>