<?php

	class acercadeController extends controladorBase{

		public function __construct(){
			parent::__construct();
		}

		public function index(){
			print "MOSTRANDO 'ACERDA DE'";
		}

		public function calculo($a){
			$this->load->vistaLogica('index');
			$this->load->vistaGrafica('pagina1');
			//$this->all_in_one->get_vars($_POST);
			$obj = $this->load->modelo('posts');
			//echo "<pre>".print_r($obj)."</pre>";
			$users = $this->posts->get_user();
			$tickets = $this->posts->get_tickets();


			$this->index->render_vista(
				"pagina1",
				$this->pagina1,
				array(
					"titulo"=>$users[0]["sugerencia"],
					"descripcion"=>"LETRAJAX es un Framework con metolodog&iacute;a de desarrollo POO (Programaci&oacute;n Orientada a Objetos) el cual trabaja con una modificaci&oacute;n del popular paradigma MVC (Model View Controller), para convertirse en MVTC (Model View Template Controller). LETRAJAX no mezcla estructuras de c&oacute;digo PHP con el lenguaje de marcado HTML, sino que separa las VISTAS L&Oacute;GICAS del sistema (Parte l&oacute;gica de la vista, donde se trata la obtenci&oacute;n de los datos enviados por alg&uacute;n controlador) de las VISTAS GR&Aacute;FICAS o HTML",
					"creadopor"=>"TSU CARLOS MEDINA",
					"nombres"=>$users[0][1]
				),
				array(
					$this->pagina1,
					"usuarios"	=> $users,
					"tickets"	=> $tickets,
				)
			);
		}

	}

?>