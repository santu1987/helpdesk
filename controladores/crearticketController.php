<?php

	class crearticketController extends controladorBase{

		public function __construct(){
			parent::__construct();
		}
		public function index(){
			$this->load->vistaLogica('creaticket');//Cargo la vista lógica admintickets
			$this->load->vistaGrafica('cargaTicket');//Cargo la vista grafica
			$this->load->modelo('permisologias');//Cargo el modelo
			$obj2 = new allinoneHelper();//Cargo helper
			$ticket_permisos = $this->permisologias->permiso_pantalla(1);//Metodo que consulta si el usuario tiene permisos en pantalla crear ticket
			$vector_permisos = $obj2->array_plpgsql_to_php($ticket_permisos[0][0]);///Metodo que transforma arreglo plpgsql en array php
			if(($vector_permisos[0] == '0')||($vector_permisos[2]=='t'))//Valido solo si es usuario(no técnico) o técnico con permiso de incluir 
			{
				$btn_crearticket = "<div type='button' id='btn_guardar_ticket' name='btn_guardar_ticket' class='btn btn-aceptar'>Crear Ticket</div>";
				$mensaje_permisos = ""; 
			}else
			{
				$btn_crearticket="";
				$mensaje_permisos = "<div class='alert alert-info mensaje_no_permiso'><i class='fa fa-exclamation-circle'></i> No tiene permiso para Crear Tickets</div>";
			}
			//--
			//Consulto tipos de soluciones...
			$opciones_sol = $this->consultar_tipos_soluciones();
			//--
			$this->creaticket->render_vista(
				'cargaTicket',
				$this->cargaTicket,
				array(
						"btn_crearticket" => $btn_crearticket,
					    "mensaje_permisos" =>$mensaje_permisos,
					    "opciones_sol"=>$opciones_sol
					  ),
				array("")
			);
			//print $this->cargaTicket;
		}
		//Metodo para registrar ticket
		public function registrarticket($vector){
			$obj = $this->load->modelo('crearticket');//Cargo el modelo
			$obj2 = new allinoneHelper();//Cargo helper
			$resp = $this->crearticket->registrar_ticket($vector[0],$vector[1]);
			//--Transformación de arreglo postgres a arreglo en php
			$this->load->modelo('fbasic');//Cargo el modelo fbasic
			$record_vector = $obj2->array_plpgsql_to_php($resp[0][0]);//Metodo que transforma arreglo plpgsql en array php
			//--
			if($record_vector[0] == 1){
				//$correo_sistema = "gsantucci@avilatv.gob.ve";//aquí va el correo del sistema
				$correo_sistema = "soporte@avilatv.gob.ve";//aquí va el correo del sistema
				$titulo = "Nueva solicitud de requerimiento ".$_SESSION['nombre'];
				$encabezado = "El usuario ".$_SESSION['nombre']." realizó el siguiente requerimiento:<br>";
				$encabezado = utf8_decode($encabezado);			
				$descripcion_ticket = utf8_decode($vector[0]);
				$result = $this->enviar_correo_ticket($correo_sistema,$correo_sistema,$record_vector[1],$titulo,$encabezado,$descripcion_ticket);
				if($result==-1){
					echo $result;
				}
			}
			echo $record_vector[0];
		}
		//Metodo para anular ticket
		public function eliminarticket($vector){
			$obj = $this->load->modelo('crearticket');//cargo el modelo
			$resp = $this->crearticket->eliminar_ticket($vector[0]);
			echo $resp[0][0];
		}
		//Metodo para realizar envio de email
		public function enviar_correo_ticket($usuario,$destinatario,$n_ticket,$titulo,$encabezado,$descripcion_ticket){
			$this->load->modelo('correo');
			$mensaje = $this->correo->crear_cuadro($titulo,$encabezado,$descripcion_ticket,$n_ticket);
			$resp2 = $this->correo->enviar_correo($usuario,$destinatario,$mensaje,$titulo);
			echo $resp;
		}

		//Metodo que permite consultar los tipos de solicitudes...
		public function consultar_tipos_soluciones(){
			$obj = $this->load->modelo('crearticket');//cargo el modelo de soluciones de tickets
			$soluciones_tickets = $this->crearticket->consultar_soluciones_tickets();//Ejecuto el metodo
			$opcion = "<option value='0'></option>";
			if($soluciones_tickets[0][0]!="NO_DATA"){
				for($i=0;$i<count($soluciones_tickets);$i++){
					if($soluciones_tickets[$i]["descripcion_solicitud"]!="OTRO TIPO DE SOLICITUD"){
						$opcion.="<option id='soli_ticket'".$i." name='soli_ticket'".$i." value='".$soluciones_tickets[$i]["id_tipo_solicitud"]."'>".$soluciones_tickets[$i]["descripcion_solicitud"]."</option>";
					}
				}
				//--
				$opcion.="<option id='soli_ticket12' name='soli_ticket12' value='12'>OTRO TIPO DE SOLICITUD</option>";
			}
			return $opcion;
		}
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	}
?>