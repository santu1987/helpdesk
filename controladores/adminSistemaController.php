<?php

	class adminSistemaController extends controladorBase{

		public function __construct(){
			parent::__construct();
		}

		public function index(){
			
			$this->load->vistaLogica("viewAdminSistema");
			$this->load->vistaGrafica("grafAdminSistema");

			$this->viewAdminSistema->render_vista(
				"grafAdminSistema",
				$this->grafAdminSistema,
				array(
					"a"=>"a"
				),
				array(
					"d"=>"d"
				)
			);
		}
		//Metodo calculo limites
		public function calculo_limites($offset,$cuantos_son,$tipo,$cuantos_x_pag){
			$this->inicio_tabla = $offset+1;
			//Valido que si $offset=0 se inicie la tabla en valor 0
			if($cuantos_son==0){
				$this->inicio_tabla = 0;
			}
			$valor = $offset+$cuantos_x_pag;
			if($valor >= $cuantos_son)
				$this->fin_tabla = $cuantos_son;
			else
				$this->fin_tabla = $valor;		
		}

/*-------------INICIO FUNCIONES DEL MOD TECNICOS------------------*/
		public function technicians($vector){
			$this->load->vistaLogica("viewAdminSistema");
			$this->load->plantilla("tablaAdminSistema_tecnico");
			$this->load->modelo("m_AdminSistema");
			//--Bloque de paginacion
			//Validación de cuantos_son
			if(($vector[1]==0)||($vector[3]==3))
			{
				$cuantos_son = $this->m_AdminSistema->cuantos_tecnicos();//Determino cuantos tecnicos son
				$cuantos_son = $cuantos_son[0][0];
			}				
			else
				$cuantos_son = $vector[1];//Determino cuantos tecnicos son
			//--Calculo de a que página debe ir
			switch ($vector[3]){
				case 0:
						$offset=0;
						///Calculo limites:
						$this->calculo_limites($offset,$cuantos_son,1,$vector[2]);
				break;		
				case 1:
						$offset=$vector[0]+$vector[2];
						///Calculo limites:
						$this->calculo_limites($offset,$cuantos_son,1,$vector[2]);
				break;
				case 2:
						$offset=$vector[0]-$vector[2];
						///Calculo limites:
						$this->calculo_limites($offset,$cuantos_son,2,$vector[2]);
				break;
				case 3:
						$offset=$vector[0];
						//--
						if($vector[0]==$cuantos_son)
							$offset=0;
						//--
						$this->calculo_limites($offset,$cuantos_son,1,$vector[2]);
				break;		
			}
			$clase_sig='';
			$clase_ant='';
			//Para ocultar siguiente 
			$offset_sig = $offset+$vector[2];
			if(($offset_sig == $cuantos_son)||($offset_sig > $cuantos_son ))
				$clase_sig = "disabled";
			//Parea ocultar anterior
			if($offset == 0)
				$clase_ant = "disabled";
			$tecnicos = $this->m_AdminSistema->get_tecnicos($offset, $vector[2]);//--Consulta para obtener a los tecnicos
			//Validar si tiene o no técnicos
			/*if($cuantos_son>0){
					$clase_tabla = 'show';
					$clase_tecnico = 'hide';
			}else{
					$clase_tabla = 'hide';
					$clase_tecnico = 'show';
			}*/
			//--

			$this->viewAdminSistema->render_vista(
				"tablaAdminSistema_tecnico",
				$this->tablaAdminSistema_tecnico,
				array(
					"paginador_siguiente"=>"carga_technicians(".$offset.",".$cuantos_son .",".$vector[2].",1);",
					"paginador_anterior"=>"carga_technicians(".$offset.",".$cuantos_son .",".$vector[2].",2);",
					"clase_paginador_siguiente"=>$clase_sig,
					"clase_paginador_anterior"=>$clase_ant,
					"offset_tabla"=>$offset,
					"cuantos_tabla"=>$cuantos_son,
					"inicio_tabla"=>$this->inicio_tabla,
					"fin_tabla"=>$this->fin_tabla
				),
				array(
					"tecnicos"	=> $tecnicos
				)
			);
		}

		public function forms_tecnico(){
			$this->load->vistaLogica("viewAdminSistema");
			$this->load->plantilla("formsAdminSistema_tecnico");
			$this->load->modelo("m_AdminSistema");

			$pefil_tecnico = $this->m_AdminSistema->get_perfil_tecnico2();
			$areas_solucion = $this->m_AdminSistema->get_areas_solucion2();
			$tec_estatus = $this->m_AdminSistema->get_estatus();

			$this->viewAdminSistema->render_vista(
				"formsAdminSistema_tecnico",
				$this->formsAdminSistema_tecnico,
				array(
					"a"	=> "a"
				),
				array(
					"pefil_tecnico"		=> $pefil_tecnico,
					"areas_solucion"	=> $areas_solucion,
					"tec_estatus"		=> $tec_estatus
				)
			);
		}

		public function ins_tecnicos(){
			/*$this->load->modelo("m_AdminSistema");
			$vars = allinoneHelper::get_vars($_POST);
			$arr_data = array(
				"cedtec"	=> $vars["cedtec"],
				"nomtec" 	=> $vars["nomtec"],
				"apetec" 	=> $vars["apetec"],
				"ematec"	=> $vars["ematec"],
				"perftec"	=> $vars["perftec"],
				"solutec"	=> $vars["solutec"],
				"descrip"	=> $vars["descrip"],
				"tec_est" 	=> $vars["tec_est"]
			);

			$resp = $this->m_AdminSistema->registrar_tecnico($arr_data);
			print $resp;*/
			$this->load->modelo("m_AdminSistema");
			$vars = allinoneHelper::get_vars($_POST);
			$arr_data = array(
				"cedtec"	=> $vars["cedtec"],
				"nomtec" 	=> $vars["nomtec"],
				"apetec" 	=> $vars["apetec"],
				"ematec"	=> $vars["ematec"],
				"perftec"	=> $vars["perftec"],
				"solutec"	=> $vars["solutec"],
				"descrip"	=> $vars["descrip"],
				"tec_est" 	=> $vars["tec_est"]
			);
			$resp = $this->m_AdminSistema->registrar_tecnico($arr_data);
			print $resp;
		}

		public function act_tecnicos(){
			$this->load->modelo("m_AdminSistema");
			$vars = allinoneHelper::get_vars($_POST);

			$arr_data2 = array(
				"cedtec"	=> $vars["cedtec"],
				"nomtec" 	=> $vars["nomtec"],
				"apetec" 	=> $vars["apetec"],
				"ematec"	=> $vars["ematec"],
				"perftec"	=> $vars["perftec"],
				"solutec"	=> $vars["solutec"],
				"descrip"	=> $vars["descrip"],
				"tec_est" 	=> $vars["tec_est"],
				"idtec"		=>$vars["idtec"]
			);

			$resp = $this->m_AdminSistema->actualizar_tecnico($arr_data2);
			print $resp;
//			print $vars["cedtec"];
		}
/*-------------FIN FUNCIONES DEL MOD TECNICOS------------------*/
/*-------------INICIO FUNCIONES DEL MOD AREA_SOLUCION------------------*/

		public function area_solu($vector){
			$this->load->vistaLogica("viewAdminSistema");
			$this->load->plantilla("tablaAdminSistema_area_solucion");
			$this->load->modelo("m_AdminSistema");

			//--Bloque de paginacion
			//Validación de cuantos_son
			if(($vector[1]==0)||($vector[3]==3))
			{
				$cuantos_son = $this->m_AdminSistema->cuantos_areas_solucion();//Determino cuantos areas solución son
				$cuantos_son = $cuantos_son[0][0];
			}				
			else
				$cuantos_son = $vector[1];//Determino cuantos areas solución son
			//--Calculo de a que página debe ir
			switch ($vector[3]){
				case 0:
						$offset=0;
						///Calculo limites:
						$this->calculo_limites($offset,$cuantos_son,1,$vector[2]);
				break;		
				case 1:
						$offset=$vector[0]+$vector[2];
						///Calculo limites:
						$this->calculo_limites($offset,$cuantos_son,1,$vector[2]);
				break;
				case 2:
						$offset=$vector[0]-$vector[2];
						///Calculo limites:
						$this->calculo_limites($offset,$cuantos_son,2,$vector[2]);
				break;
				case 3:
						$offset=$vector[0];
						//--
						if($vector[0]==$cuantos_son)
							$offset=0;
						//--
						$this->calculo_limites($offset,$cuantos_son,1,$vector[2]);
				break;		
			}
			$clase_sig='';
			$clase_ant='';
			//Para ocultar siguiente 
			$offset_sig = $offset+$vector[2];
			if(($offset_sig == $cuantos_son)||($offset_sig > $cuantos_son ))
				$clase_sig = "disabled";
			//Parea ocultar anterior
			if($offset == 0)
				$clase_ant = "disabled";
			$areas_solucion2 = $this->m_AdminSistema->get_areas_solucion($offset, $vector[2]);//--Consulta para obtener a los areas solución

			$this->viewAdminSistema->render_vista(
				"tablaAdminSistema_area_solucion",
				$this->tablaAdminSistema_area_solucion,
				array(
					"paginador_siguiente"		=>"carga_area_solu(".$offset.",".$cuantos_son .",".$vector[2].",1);",
					"paginador_anterior"		=>"carga_area_solu(".$offset.",".$cuantos_son .",".$vector[2].",2);",
					"clase_paginador_siguiente"	=>$clase_sig,
					"clase_paginador_anterior"	=>$clase_ant,
					"offset_tabla"				=>$offset,
					"cuantos_tabla"				=>$cuantos_son,
					"inicio_tabla"				=>$this->inicio_tabla,
					"fin_tabla"					=>$this->fin_tabla
				),
				array(
					"areas_solucion2"	=> $areas_solucion2
				)
			);
		}

		public function forms_area_solu(){
			$this->load->vistaLogica("viewAdminSistema");
			$this->load->plantilla("formsAdminSistema_area_solucion");
			$this->load->modelo("m_AdminSistema");

			$area_solu_estatus = $this->m_AdminSistema->get_estatus();

			$this->viewAdminSistema->render_vista(
				"formsAdminSistema_area_solucion",
				$this->formsAdminSistema_area_solucion,
				array(
					"a"	=> "a"
				),
				array(
					"area_solu_estatus"	=> $area_solu_estatus
				)
			);
		}

		public function ins_area_solu(){
			$this->load->modelo("m_AdminSistema");
			$vars = allinoneHelper::get_vars($_POST);

			$arr_data = array(
				"nom_area_solu"			=> $vars["nom_area_solu"],
				"descrip_area_solu"		=> $vars["descrip_area_solu"],
				"tlf_prin_area_solu" 	=> $vars["tlf_prin_area_solu"],
				"tlf_alter_area_solu"	=> $vars["tlf_alter_area_solu"],
				"area_solu_estatus"		=> $vars["area_solu_estatus"]
			);

			$resp = $this->m_AdminSistema->registrar_areas_solucion($arr_data);
			print $resp;
		}

		public function act_area_solu(){
			$this->load->modelo("m_AdminSistema");
			$vars = allinoneHelper::get_vars($_POST);

			$arr_data = array(
				"nom_area_solu"			=> $vars["nom_area_solu"],
				"descrip_area_solu"		=> $vars["descrip_area_solu"],
				"tlf_prin_area_solu" 	=> $vars["tlf_prin_area_solu"],
				"tlf_alter_area_solu"	=> $vars["tlf_alter_area_solu"],
				"id_area_solu"			=> $vars["id_area_solu"],
				"area_solu_estatus"		=> $vars["area_solu_estatus"]

			);

			$resp = $this->m_AdminSistema->actualizar_areas_solucion($arr_data);
			print $resp;
		}

/*-------------FIN FUNCIONES DEL MOD AREA_SOLUCION------------------*/
/*-------------INICIO FUNCIONES DEL MOD TIPOSOL---------------------*/
	public function tipos_solu($vector){
		$this->load->vistaLogica("viewAdminSistema");
		$this->load->plantilla("tablaAdminSistema_tiposol");
		$this->load->modelo("m_AdminSistema");
		//--Bloque de paginacion
		//Validación de cuantos_son
		if(($vector[1]==0)||($vector[3]==3))
		{
			$cuantos_son = $this->m_AdminSistema->cuantos_tiposol();//Determino cuantos areas solución son
			$cuantos_son = $cuantos_son[0][0];
		}				
		else
			$cuantos_son = $vector[1];//Determino cuantos areas solución son
		//--Calculo de a que página debe ir
		switch ($vector[3]){
			case 0:
					$offset=0;
					///Calculo limites:
					$this->calculo_limites($offset,$cuantos_son,1,$vector[2]);
			break;		
			case 1:
					$offset=$vector[0]+$vector[2];
					///Calculo limites:
					$this->calculo_limites($offset,$cuantos_son,1,$vector[2]);
			break;
			case 2:
					$offset=$vector[0]-$vector[2];
					///Calculo limites:
					$this->calculo_limites($offset,$cuantos_son,2,$vector[2]);
			break;
			case 3:
					$offset=$vector[0];
					//--
					if($vector[0]==$cuantos_son)
						$offset=0;
					//--
					$this->calculo_limites($offset,$cuantos_son,1,$vector[2]);
			break;		
		}
		$clase_sig='';
		$clase_ant='';
		//Para ocultar siguiente 
		$offset_sig = $offset+$vector[2];
		if(($offset_sig == $cuantos_son)||($offset_sig > $cuantos_son ))
			$clase_sig = "disabled";
		//Parea ocultar anterior
		if($offset == 0)
			$clase_ant = "disabled";
		$tiposol = $this->m_AdminSistema->get_tiposol($offset, $vector[2]);//--Consulta para obtener a los areas solución
		$this->viewAdminSistema->render_vista(
			"tablaAdminSistema_tiposol",
			$this->tablaAdminSistema_tiposol,
			array(
					"paginador_siguiente"		=>"carga_tipo_sol(".$offset.",".$cuantos_son .",".$vector[2].",1);",
					"paginador_anterior"		=>"carga_tipo_sol(".$offset.",".$cuantos_son .",".$vector[2].",2);",
					"clase_paginador_siguiente"	=>$clase_sig,
					"clase_paginador_anterior"	=>$clase_ant,
					"offset_tabla"				=>$offset,
					"cuantos_tabla"				=>$cuantos_son,
					"inicio_tabla"				=>$this->inicio_tabla,
					"fin_tabla"					=>$this->fin_tabla
			),
			array(
					"tiposol"	=> $tiposol
			)
		);
	}
	public function forms_tipo_sol(){
		$this->load->vistaLogica("viewAdminSistema");
		$this->load->plantilla("forms_AdminSistema_tiposol");
		$this->load->modelo("m_AdminSistema");
		$tipos_estatus = $this->m_AdminSistema->get_estatus();
		$this->viewAdminSistema->render_vista(
			"forms_AdminSistema_tiposol",
			$this->forms_AdminSistema_tiposol,
			array(""),
			array("tipos_estatus" => $tipos_estatus)
		);
	}

	public function ins_tipo_sol($vector){
		$obj =  $this->load->modelo("m_AdminSistema");
		$resp = $this->m_AdminSistema->registrar_tipo_sol($vector[0],$vector[1],$vector[2]);
		echo $resp;
	}
/*-------------FIN DE FUNCIONES DEL MOD TIPOSOL---------------------*/

/*-------------INICIO FUNCIONES DEL MOD DEPARTAMENTIO ------------------*/
		public function department($vector){
			$this->load->vistaLogica("viewAdminSistema");
			$this->load->plantilla("tablaAdminSistema_departamento");
			$this->load->modelo("m_AdminSistema");

			//--Bloque de paginacion
			//Validación de cuantos_son
			if(($vector[1]==0)||($vector[3]==3))
			{
				$cuantos_son = $this->m_AdminSistema->cuantos_departamentos();//Determino cuantos departamentos son
				$cuantos_son = $cuantos_son[0][0];
			}				
			else
				$cuantos_son = $vector[1];//Determino cuantos areas departamentos son
			//--Calculo de a que página debe ir
			switch ($vector[3]){
				case 0:
						$offset=0;
						///Calculo limites:
						$this->calculo_limites($offset,$cuantos_son,1,$vector[2]);
				break;		
				case 1:
						$offset=$vector[0]+$vector[2];
						///Calculo limites:
						$this->calculo_limites($offset,$cuantos_son,1,$vector[2]);
				break;
				case 2:
						$offset=$vector[0]-$vector[2];
						///Calculo limites:
						$this->calculo_limites($offset,$cuantos_son,2,$vector[2]);
				break;
				case 3:
						$offset=$vector[0];
						//--
						if($vector[0]==$cuantos_son)
							$offset=0;
						//--
						$this->calculo_limites($offset,$cuantos_son,1,$vector[2]);
				break;		
			}
			$clase_sig='';
			$clase_ant='';
			//Para ocultar siguiente 
			$offset_sig = $offset+$vector[2];
			if(($offset_sig == $cuantos_son)||($offset_sig > $cuantos_son ))
				$clase_sig = "disabled";
			//Parea ocultar anterior
			if($offset == 0)
				$clase_ant = "disabled";
			$departamentos = $this->m_AdminSistema->get_departamentos($offset, $vector[2]);//--Consulta para obtener a los departamentos

			$this->viewAdminSistema->render_vista(
				"tablaAdminSistema_departamento",
				$this->tablaAdminSistema_departamento,
				array(
					"paginador_siguiente"		=>"carga_department(".$offset.",".$cuantos_son .",".$vector[2].",1);",
					"paginador_anterior"		=>"carga_department(".$offset.",".$cuantos_son .",".$vector[2].",2);",
					"clase_paginador_siguiente"	=>$clase_sig,
					"clase_paginador_anterior"	=>$clase_ant,
					"offset_tabla"				=>$offset,
					"cuantos_tabla"				=>$cuantos_son,
					"inicio_tabla"				=>$this->inicio_tabla,
					"fin_tabla"					=>$this->fin_tabla
				),
				array(
					"departamentos"	=> $departamentos
				)
			);
		}

		public function forms_depto(){
			$this->load->vistaLogica("viewAdminSistema");
			$this->load->plantilla("formsAdminSistema_departamento");
			$this->load->modelo("m_AdminSistema");

			$this->viewAdminSistema->render_vista(
				"formsAdminSistema_departamento",
				$this->formsAdminSistema_departamento,
				array(
					"a"	=> "a"
				),
				array(
					"b"	=> "b"
				)
			);
		}

		public function ins_depto(){
			$this->load->modelo("m_AdminSistema");
			$vars = allinoneHelper::get_vars($_POST);

			$arr_data = array(
				"depto"				=> $vars["depto"],
				"tlf_prin_depto"	=> $vars["tlf_prin_depto"],
				"tlf_alter_depto"	=> $vars["tlf_alter_depto"]

			);

			$resp = $this->m_AdminSistema->registrar_departamento($arr_data);
			print $resp;
		}

		public function act_depto(){
			$this->load->modelo("m_AdminSistema");
			$vars = allinoneHelper::get_vars($_POST);

			$arr_data = array(
				"depto"				=> $vars["depto"],
				"tlf_prin_depto"	=> $vars["tlf_prin_depto"],
				"tlf_alter_depto"	=> $vars["tlf_alter_depto"],
				"id_depto"			=> $vars["id_depto"]

			);

			$resp = $this->m_AdminSistema->actualizar_departamento($arr_data);
			print $resp;
		}

/*-------------FIN FUNCIONES DEL MOD DEPARTAMENTIO ------------------*/

/*-------------INICIO FUNCIONES DEL MOD PERFIL TECNICO ------------------*/
		public function profile($vector){
			$this->load->vistaLogica("viewAdminSistema");
			$this->load->plantilla("tablaAdminSistema_perfil");
			$this->load->modelo("m_AdminSistema");

			//--Bloque de paginacion
			//Validación de cuantos_son
			if(($vector[1]==0)||($vector[3]==3))
			{
				$cuantos_son = $this->m_AdminSistema->cuantos_perfil_tecnico();//Determino cuantos perfiles son
				$cuantos_son = $cuantos_son[0][0];
			}				
			else
				$cuantos_son = $vector[1];//Determino cuantos areas perfiles son
			//--Calculo de a que página debe ir
			switch ($vector[3]){
				case 0:
						$offset=0;
						///Calculo limites:
						$this->calculo_limites($offset,$cuantos_son,1,$vector[2]);
				break;		
				case 1:
						$offset=$vector[0]+$vector[2];
						///Calculo limites:
						$this->calculo_limites($offset,$cuantos_son,1,$vector[2]);
				break;
				case 2:
						$offset=$vector[0]-$vector[2];
						///Calculo limites:
						$this->calculo_limites($offset,$cuantos_son,2,$vector[2]);
				break;
				case 3:
						$offset=$vector[0];
						//--
						if($vector[0]==$cuantos_son)
							$offset=0;
						//--
						$this->calculo_limites($offset,$cuantos_son,1,$vector[2]);
				break;		
			}
			$clase_sig='';
			$clase_ant='';
			//Para ocultar siguiente 
			$offset_sig = $offset+$vector[2];
			if(($offset_sig == $cuantos_son)||($offset_sig > $cuantos_son ))
				$clase_sig = "disabled";
			//Parea ocultar anterior
			if($offset == 0)
				$clase_ant = "disabled";
			$profile = $this->m_AdminSistema->get_perfil_tecnico($offset, $vector[2]);//--Consulta para obtener a los departamentos

			$this->viewAdminSistema->render_vista(
				"tablaAdminSistema_perfil",
				$this->tablaAdminSistema_perfil,
				array(
					"paginador_siguiente"		=>"carga_profile(".$offset.",".$cuantos_son .",".$vector[2].",1);",
					"paginador_anterior"		=>"carga_profile(".$offset.",".$cuantos_son .",".$vector[2].",2);",
					"clase_paginador_siguiente"	=>$clase_sig,
					"clase_paginador_anterior"	=>$clase_ant,
					"offset_tabla"				=>$offset,
					"cuantos_tabla"				=>$cuantos_son,
					"inicio_tabla"				=>$this->inicio_tabla,
					"fin_tabla"					=>$this->fin_tabla
				),
				array(
					"profile"	=> $profile
				)
			);
		}

		public function forms_profile(){
			$this->load->vistaLogica("viewAdminSistema");
			$this->load->plantilla("formsAdminSistema_perfil");
			$this->load->modelo("m_AdminSistema");

			$profile_estatus = $this->m_AdminSistema->get_estatus();

			$this->viewAdminSistema->render_vista(
				"formsAdminSistema_perfil",
				$this->formsAdminSistema_perfil,
				array(
					"a"	=> "a"
				),
				array(
					"profile_estatus"	=> $profile_estatus
				)
			);
		}

		public function ins_profile(){
			$this->load->modelo("m_AdminSistema");
			$vars = allinoneHelper::get_vars($_POST);

			$arr_data = array(
				"profile"				=> $vars["profile"],
				"profile_estatus"		=> $vars["profile_estatus"]
			);

			$resp = $this->m_AdminSistema->registrar_profile($arr_data);
			print $resp;
		}

		public function act_profile(){
			$this->load->modelo("m_AdminSistema");
			$vars = allinoneHelper::get_vars($_POST);

			$arr_data = array(
				"profile"				=> $vars["profile"],
				"id_profile"			=> $vars["id_profile"],
				"profile_estatus"		=> $vars["profile_estatus"]

			);

			$resp = $this->m_AdminSistema->actualizar_profile($arr_data);
			print $resp;
		}



/*-------------FIN FUNCIONES DEL MOD PERFIL TECNICO ------------------*/

/*-------------INICIO FUNCIONES DEL MOD PERMISOS------------------*/
		public function permissions(){
			$this->load->vistaLogica("viewAdminSistema");
			$this->load->plantilla("permisoAdminSistema_tecnico");
			$this->load->modelo("m_AdminSistema");

			$user_per = $this->m_AdminSistema->get_tecnicos();
			$pantallas = $this->m_AdminSistema->get_pantallas();


			$this->viewAdminSistema->render_vista(
				"permisoAdminSistema_tecnico",
				$this->permisoAdminSistema_tecnico,
				array(
					"a"		=> "a"
				),
				array(
					"user_per"		=> $user_per,
					"pantallas" 	=> $pantallas
				)
			);
		}

		public function save_permisos(){
			$this->load->modelo("m_AdminSistema");
			$vars = allinoneHelper::get_vars($_POST);

			$arr_data = array(
				"usu_permi"		=> $vars["usu_permi"],
				"pan_permi"		=> $vars["pan_permi"],
				"bus_permi"		=> $vars["bus_permi"],
				"inc_permi"		=> $vars["inc_permi"],
				"mod_permi"		=> $vars["mod_permi"],
				"eli_permi"		=> $vars["eli_permi"],
				"acc_permi"		=> $vars["acc_permi"],
				"imp_permi"		=> $vars["imp_permi"],
				"anu_permi"		=> $vars["anu_permi"]
			);

			$resp = $this->m_AdminSistema->asignar_permisos($arr_data);
			print $resp;
		}

		public function selec_panatalla_permisos(){
			$this->load->modelo("m_AdminSistema");
			$vars = allinoneHelper::get_vars($_POST);

			$arr_data = array(
				"usu_permi"		=> $vars["usu_permi"],
				"pan_permi"		=> $vars["pan_permi"]
			);

			$resp = $this->m_AdminSistema->get_pantallas_permisos($arr_data);
			print $resp;
		}

/*-------------FIN FUNCIONES DEL MOD PERMISOS------------------*/

	}

?>
