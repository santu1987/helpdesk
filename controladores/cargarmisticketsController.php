<?php
	class cargarmisticketsController extends controladorBase {
		public $inicio_tabla;
		public $fin_tabla;
		public function __construct(){
			parent::__construct();
		}
		public function index(){

		}
		public function calculo_limites($offset,$cuantos_son,$tipo){
			$this->inicio_tabla = $offset+1;
			//--Valido que si $offset=0 se inicie la tabla en valor 0
			$valor = $offset+50;
			if($valor >= $cuantos_son)
				$this->fin_tabla = $cuantos_son;
			else
				$this->fin_tabla = $valor;		
			if($cuantos_son==0){
				$this->inicio_tabla = 0;
			}
		}
		
		public function cargar_pag($vector){
			$this->load->vistaLogica('cargarmistickets');//cargo vista lógica cargaTicket
			$this->load->vistaGrafica('listamistickets');//cargo vista gráfica listamistickets
			$obj = $this->load->modelo('mistickets');//cargol el modelo cargarmistickets
			//--Validación de cuantos_son
			if(($vector[1]==0)||($vector[3]==3))
			{
				$cuantos_son = $this->mistickets->cuantos_tickets();//Determino cuantos ticket_son
				$cuantos_son = $cuantos_son[0][0];
			}				
			else
				$cuantos_son = $vector[1];//Determino cuantos ticket_son
			//--Calculo de a que página debe ir
			switch ($vector[3]){
				case 0:
						$offset=0;
						///Calculo limites:
						$this->calculo_limites($offset,$cuantos_son,1);
				break;		
				case 1:
						$offset=$vector[0]+$vector[2];
						///Calculo limites:
						$this->calculo_limites($offset,$cuantos_son,1);
				break;
				case 2:
						$offset=$vector[0]-$vector[2];
						///Calculo limites:
						$this->calculo_limites($offset,$cuantos_son,2);
				break;
				case 3:
						$offset=$vector[0];
						//--
						if($vector[0]==$cuantos_son)
							$offset=0;
						//--
						$this->calculo_limites($offset,$cuantos_son,1);
				break;		
			}
			//--Para ocultar siguiente 
			$offset_sig = $offset+$vector[2];
			$clase_sig = "";
			if(($offset_sig == $cuantos_son)||($offset_sig > $cuantos_son ))
				$clase_sig = "disabled";
			//--Para ocultar anterior
			if($offset == 0)
				$clase_ant = "disabled";
			else
				$clase_ant = "";	
			//--Validar si tiene o no tickets
			if($cuantos_son>0){
					$clase_tabla = 'show';
					$clase_tickets = 'hide';
			}else{
					$clase_tabla = 'hide';
					$clase_tickets = 'show';
			}
			//--Bloque de permisologías a acciones
			$this->load->modelo('permisologias');//Cargo el modelo
			$obj2 = new allinoneHelper();//Cargo helper
			$mistickets_permisos = $this->permisologias->permiso_pantalla(2);//Metodo que consulta si el usuario tiene permisos en pantalla Mis tickets
			$vector_permisos = $obj2->array_plpgsql_to_php($mistickets_permisos[0][0]);///Metodo que transforma arreglo plpgsql en array php
			//--	
			$tickets = $this->mistickets->get_tickets_tabla($offset,$vector[2]);//obtener los tickets segun los parametros de paginacion
			$this->cargarmistickets->render_vista(
				"listamistickets",
				$this->listamistickets,
				array(
						"paginador_siguiente"=>"ir_tabla(".$offset.",".$cuantos_son .",50,1);",
						"paginador_anterior"=>"ir_tabla(".$offset.",".$cuantos_son .",50,2);",
						"clase_paginador_siguiente"=>$clase_sig,
						"clase_paginador_anterior"=>$clase_ant,
						"offset_tabla"=>$offset,
						"cuantos_tabla"=>$cuantos_son,
						"inicio_tabla"=>$this->inicio_tabla,
						"fin_tabla"=>$this->fin_tabla,
						"clase_tabla"=>$clase_tabla,
						"clase_tickets"=>$clase_tickets
					),
				array(
						//$this->listamistickets,
						"tickets" =>$tickets,
						"vector_permisos"=>$vector_permisos,
					)
				);

		}
		
	}	
?>