<?php
	
	session_start();

	define('RUTA_SITIO',realpath(dirname(__FILE__))."/");
	define('RUTA_SERVER','http://127.0.0.1/helpdesk/');
	define('RUTA_PC',$_SERVER['SERVER_ADDR']);
	require_once(RUTA_SITIO."aplicacion/helpers/all_in_one.php");

	require_once(RUTA_SITIO."aplicacion/enrutador.php");

	require_once(RUTA_SITIO."aplicacion/controladorBase.php");

	require_once(RUTA_SITIO."aplicacion/config.php");

	require_once(RUTA_SITIO."aplicacion/baseDatos.php");

	require_once(RUTA_SITIO."aplicacion/modeloBase.php");

	require_once(RUTA_SITIO."aplicacion/vistaBase.php");

	require_once(RUTA_SITIO."aplicacion/load.php");

	require_once(RUTA_SITIO."aplicacion/registry.php");

	require_once(RUTA_SITIO."controladores/indexController.php");
	$enrutador = new Enrutador();
	$enrutador->run();
	
?>