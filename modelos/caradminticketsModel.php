<?php
	class caradminticketsModel extends modeloBase{
		public function consultar_admin_tickets($offset,$limit){
			$dat = modeloBase::$bd_hd->execute("SELECT 
														a.cedula_usuario,
														a.id_ticket,
														a.nombres_apellidos,
														(substring (a.descripcion,0,100)||'...') AS descripcion_ticket,
														a.fecha_creacion,
														a.hora_creacion,
														(a.descripcion) AS descripcion_ticket2,
														(SELECT count(b.id_apelacion) FROM apelaciones b WHERE b.id_ticket = a.id_ticket ) AS apelacion_ticket,
														a.id_estatus,
														c.estatus,
														d.descripcion_solicitud

												FROM 
														tickets a
												INNER JOIN 
														estatus c
												ON 
														a.id_estatus=c.id_estatus
												INNER JOIN 
														tipo_solicitud d
												ON 
														a.id_tipo_solicitud=d.id_tipo_solicitud								
												WHERE 
														a.id_estatus!='4'	
												ORDER BY 
														id_ticket DESC 
												limit 
														?
												offset
														?						
												", array($limit,$offset));
			return $dat;
		}
		public function cuantos_tickets_adm(){
			$dat = modeloBase::$bd_hd->execute("SELECT count(*) FROM tickets WHERE id_estatus!='4'");
			return $dat;
		}
		public function cerrar_tickets($vector){
			/*$dat = modeloBase::$bd_hd->execute("SELECT 
														cerrar_tickets(?,?,?)",
												array($vector[0],$_SESSION["usuarios"],$_SESSION['ip']));//Usar en  local*/
			$dat = modeloBase::$bd_hd->execute("SELECT 
														cerrar_tickets('?',?,?)",
												array($vector[0],$_SESSION["usuarios"],$_SESSION['ip']));//USar en servidor
			return $dat[0][0];
		}
		public function consultar_fecha_hora($id_ticket){
			$dat = modeloBase::$bd_hd->execute("SELECT
														fecha_solucion||' '||hora_solucion
												FROM 
														tickets
												WHERE 
														id_ticket=?",
												array($id_ticket));
			return $dat[0][0];
		}
	}
?>