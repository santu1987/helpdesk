<?php

		function validar_usuario($nombre,$clave){
			$ldap['user']              = $nombre;
   		$ldap['pass']              = $clave;
   		$ldap['host']              = '10.0.7.4'; // nombre del host o servidor
   		$ldap['port']              = 389; // puerto del LDAP en el servidor
   		$ldap['dn']                = 'uid='.$ldap['user'].',ou=people,dc=avilatv,dc=gob,dc=ve'; // modificar respecto a los valores del LDAP
   		$ldap['base']              = 'dc=avilatv,dc=gob,dc=ve';
   	   	 		
   		// conexion a ldap
   		$ldap['conn'] = ldap_connect($ldap['host'], $ldap['port']);  
   		ldap_set_option($ldap['conn'], LDAP_OPT_PROTOCOL_VERSION, 3); 
   	
   		// match de usuario y password
   		$ldap['bind'] = ldap_bind($ldap['conn'], $ldap['dn'], $ldap['pass']);            
	   	if ($ldap['bind']){
	   	
	   		$ldap_rootdn = "dc=avilatv,dc=gob,dc=ve";
    			$ldap_searchattr = "userid";
    			$ldap_fname = "givenname";
    			$ldap_lname = "sn";
    			$ldap_uname = "uid";
    			$ldap_email_add = "mail";
    			$ldap_office = "";
    			$ldap_phone = "telephonenumber";
    			$ldap_context = "dn";
    			$ldap_domain = "avilatv.gob.ve";
    			$ldap_passw="userpassword";
    			$pwd=$ldap['pass'];
    			$name=$ldap['user'];
    			$ldap_cedula= "employeenumber";
    			$ldap_rootdn = "dc=avilatv,dc=gob,dc=ve";
    			$ldap_nombre="displayname";
    		
    			$filter = "($ldap_searchattr=$name)"; 
    		
	    		$justthese = array ($ldap_fname, $ldap_lname, $ldap_uname, $ldap_email_add, $ldap_office, $ldap_phone, $ldap_context,$ldap_cedula, $ldap_nombre);
    		    		
   			$sr = ldap_search($ldap['conn'], $ldap_rootdn, $filter, $justthese); 
    		
    			$info = ldap_get_entries($ldap['conn'], $sr); 
    		
    			$nombre = $info[0]["$ldap_nombre"][0];    		    		    		
	         	$email = $info[0]["$ldap_email_add"][0];
        	 	$cedula = $info[0]["$ldap_phone"][0];        	            			           	         	 					 				
 				
		   	$_SESSION['nombre']=$nombre;
			   
	      $_SESSION['cedula']=$cedula;
			   
		   	$_SESSION['usuarios']=$name;
		   	
		   	$_SESSION['activo'] = true;
		   	
				return true;		   		
			} else {
				return false;	
			}
		}
		
		function cerrar_sesion(){
			unset($_SESSION["nombre"]);
			unset($_SESSION["cedula"]);
			unset($_SESSION["usuarios"]);
			unset($_SESSION["activo"]);
			session_destroy();
			
			return trim("cerrado");	
		}

?>
