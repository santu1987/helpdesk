	<?php
		class m_AdminSistemaModel extends modeloBase{


			public function get_tickets(){
				$dat = modeloBase::$bd_hd->execute("SELECT * FROM tickets",array());
				return $dat;
			}
			
			public function get_tecnicos($offset, $limit){
				$dat = modeloBase::$bd_hd->execute("SELECT tec.cedula,tec.nombres,tec.apellidos,tec.correo_electronico_tecnico,pt.id_perfil_tecnico,pt.perfil_tecnico,ars.id_area_solucion,ars.nombre_area_solucion,ars.descripcion_area_solucion,tec.id_tecnico,est.id_estatus,est.estatus FROM tecnicos tec INNER JOIN perfil_tecnico pt ON(tec.id_perfil_tecnico = pt.id_perfil_tecnico) INNER JOIN areas_solucion ars ON(tec.id_area_solucion = ars.id_area_solucion) INNER JOIN estatus est ON(tec.id_estatus = est.id_estatus)  ORDER BY tec.id_tecnico, tec.nombres  offset ? limit ?",array($offset,$limit));
				if($dat!="NO_DATA")
					return $dat;
				else
					return false;
			}

			public function cuantos_tecnicos(){
				$dat = modeloBase::$bd_hd->execute("SELECT count(*) FROM tecnicos ");
				return $dat;
			}
			//--Modificado por gsantucci 15/06/2015: No se habían colocado las variables $offset y $limit
			public function get_perfil_tecnico($offset,$limit){
				$dat = modeloBase::$bd_hd->execute("SELECT * FROM perfil_tecnico pt INNER JOIN estatus est ON (pt.id_estatus = est.id_estatus) ORDER BY perfil_tecnico offset ? limit ?",array($offset,$limit));
				return $dat;
			}

			public function cuantos_perfil_tecnico(){
				$dat = modeloBase::$bd_hd->execute("SELECT count(*) FROM perfil_tecnico ");
				return $dat;
			}

			public function get_perfil_tecnico2(){
				$dat = modeloBase::$bd_hd->execute("SELECT id_perfil_tecnico,perfil_tecnico FROM perfil_tecnico WHERE id_estatus = ? ORDER BY perfil_tecnico",array(9));
				return $dat;
			}
			//--Creado por Gianni Santucci 20/10/2015
			public function get_tiposol($offset,$limit){
				$dat = modeloBase::$bd_hd->execute("SELECT 
															a.id_tipo_solicitud,
															a.descripcion_solicitud,
															b.estatus,
															b.id_estatus
													FROM 
															tipo_solicitud a
													INNER JOIN 
															estatus b
													ON 		
															a.id_estatus = b.id_estatus 		 
													ORDER BY 
															a.id_tipo_solicitud
													OFFSET 
														?
													LIMIT 
														?"
													,array($offset,$limit));
				return $dat;
			}
			public function cuantos_tiposol (){
				$dat = modeloBase::$bd_hd->execute("SELECT count(*) FROM tipo_solicitud");
				return $dat;
			}
			public function registrar_tipo_sol($descripcion_tipo_sol,$estatus_tipo_sol,$id_tipo_sol){
				//--
				if($id_tipo_sol==""){
					$id_tipo_sol2 = 0;
					$id_tipo_sol2 = (int)$id_tipo_sol2;
				}
				else{
					$id_tipo_sol2 = (int)$id_tipo_sol;
				}
				$descripcion_tipo_sol = strtoupper($descripcion_tipo_sol);
				//--
				$ins = modeloBase::$bd_hd->execute("SELECT registrar_tipo_sol (?,?,?,?,?)",array($descripcion_tipo_sol,$estatus_tipo_sol,$id_tipo_sol2, $_SESSION["usuarios"],$_SESSION["ip"]));
				return $ins[0][0];
			}
			//--Modificado por gsantucci 15/06/2015: No se habían colocado las variables $offset y $limit
			public function get_areas_solucion($offset,$limit){
				$dat = modeloBase::$bd_hd->execute("SELECT * FROM areas_solucion are INNER JOIN estatus est ON(are.id_estatus = est.id_estatus) ORDER BY nombre_area_solucion offset ? limit ?",array($offset,$limit));
				return $dat;
			}

			public function cuantos_areas_solucion(){
				$dat = modeloBase::$bd_hd->execute("SELECT count(*) FROM areas_solucion ");
				return $dat;
			}
			public function get_areas_solucion2(){
				$dat = modeloBase::$bd_hd->execute("SELECT id_area_solucion,nombre_area_solucion,descripcion_area_solucion FROM areas_solucion WHERE id_estatus = ? ORDER BY nombre_area_solucion ",array(9));
				return $dat;
			}

			public function get_departamentos($offset,$limit){
				$dat = modeloBase::$bd_hd->execute("SELECT * FROM departamentos ORDER BY codigo_dpto OFFSET ? LIMIT ?",array($offset,$limit));
				return $dat;
			}

			public function cuantos_departamentos(){
				$dat = modeloBase::$bd_hd->execute("SELECT count(*) FROM departamentos");
				return $dat;
			}


			public function get_pantallas(){
				$dat = modeloBase::$bd_hd->execute("SELECT * FROM pantallas",array());
				return $dat;
			}

			public function get_estatus(){
				$dat = modeloBase::$bd_hd->execute("SELECT * FROM estatus WHERE id_estatus = ? OR id_estatus = ?",array(9,10));
				return $dat;
			}
			
			//--Original
			/*public function registrar_tecnico($datos){
				if($datos["cedtec"] != ""){
					$ins =modeloBase::$bd_hd->execute("INSERT INTO tecnicos (cedula,nombres,apellidos,id_perfil_tecnico,id_area_solucion,correo_electronico_tecnico,id_estatus) VALUES (?,?,?,?,?,?,?)",array($datos["cedtec"],$datos["nomtec"],$datos["apetec"],$datos["perftec"],$datos["solutec"],$datos["ematec"],$datos["tec_est"]));
					if($ins[0][0] === true){
						return true;
					}else{
						return false;
					}
				}else{
					return false;
				}
			}*/
			
			//--Modificado por gsantucci 15/05/2015: 
			public function registrar_tecnico($datos){
			    $ins = modeloBase::$bd_hd->execute("SELECT registrar_tecnicos_base(?,?,?,?,?,?,?,?,?)",array($datos["cedtec"],$datos["nomtec"],$datos["apetec"],$datos["perftec"],$datos["solutec"],$datos["ematec"],$datos["tec_est"],$_SESSION["usuarios"],$_SESSION["ip"]));
				//return $ins[0][0];
				//if($ins[0][0] === true){
			    if($ins[0][0] == 1){
					return true;
				}
				else if($ins[0][0] ==0){
					return 0;
				}
				else{
					return false;
				}
			}

			//--Original
			/*
				public function actualizar_tecnico($datos){
				if($datos["cedtec"] != ""){
					$actu =modeloBase::$bd_hd->execute("UPDATE tecnicos SET nombres = ?, apellidos = ?, id_perfil_tecnico = ?, id_area_solucion = ?, correo_electronico_tecnico = ?, id_estatus = ? WHERE cedula = ?",array($datos["nomtec"],$datos["apetec"],$datos["perftec"],$datos["solutec"],$datos["ematec"],$datos["tec_est"],$datos["cedtec"]));
					if($actu){
						return true;
					}else{
						return false;
					}
				}else{
					return false;
				}
			}
			*/
			
			//--Modificado por gsantucci 15/05/2015: 
			public function actualizar_tecnico($datos){
				$actu = modeloBase::$bd_hd->execute("SELECT actualizar_tecnico_base(?,?,?,?,?,?,?,?,?,?)",array($datos["nomtec"],$datos["apetec"],$datos["perftec"],$datos["solutec"],$datos["ematec"],$datos["tec_est"],$datos["cedtec"],$datos["idtec"],$_SESSION["usuarios"],$_SESSION["ip"]));
				if($actu[0][0]==1){
					return true;
				}
				else if($actu[0][0] ==0){
					return 0;
				}
				else{
					return false;
				}
			}

			public function asignar_permisos($datos){
				if($datos["usu_permi"] != ""){
					$sel = modeloBase::$bd_hd->execute("SELECT * FROM permisos WHERE id_pantalla = ? AND id_tecnico = ?",array($datos["pan_permi"],$datos["usu_permi"]));
					if($sel == "NO_DATA"){
						$ins = modeloBase::$bd_hd->execute("INSERT INTO permisos (id_pantalla,acceso,incluir,modificar,buscar,imprimir,eliminar,anular,id_tecnico)values(?,?,?,?,?,?,?,?,?)",array($datos["pan_permi"],$datos["acc_permi"],$datos["inc_permi"],$datos["mod_permi"],$datos["bus_permi"],$datos["imp_permi"],$datos["eli_permi"],$datos["anu_permi"],$datos["usu_permi"]));
						if($ins){
							//--Creado por gsantucci:12/05/2015
							//#Auditoria
							$opcion = $this->ejecutar_auditoria("Registrar permisos","permisos","Ejecutó Inserción","SELECT max(id_permiso) from permisos");						
							if($opcion !="1") {
								return false;
							}
							//--	
							return true;
						}else{
							return false;
						}
					}else{
						$actu =modeloBase::$bd_hd->execute("UPDATE permisos SET acceso = ?, incluir = ?, modificar = ?, buscar = ?, imprimir = ?, eliminar = ?, anular = ? WHERE id_pantalla = ? AND id_tecnico = ? ",array($datos["acc_permi"],$datos["inc_permi"],$datos["mod_permi"],$datos["bus_permi"],$datos["imp_permi"],$datos["eli_permi"],$datos["anu_permi"],$datos["pan_permi"],$datos["usu_permi"]));
						if($actu){
							//--Creado por gsantucci:12/05/2015
							//#Auditoria
							$sql_id = "SELECT id_permiso from permisos WHERE  id_pantalla = ? AND id_tecnico = ? ";
							$parametros = array($datos["pan_permi"],$datos["usu_permi"]);
							$opcion = $this->ejecutar_auditoria_arreglo("Actualizar permisos","permisos","Ejecutó actualización", $sql_id, $parametros);						
							if($opcion !="1") {
								return false;
							}
							//--
							return true;
						}else{
							return false;
						}
					}
				}else{
					return false;
				}
			}
			//--Original
			/*public function registrar_areas_solucion($datos){
				if($datos["nom_area_solu"] != ""){
					$ins = modeloBase::$bd_hd->execute("INSERT INTO areas_solucion (nombre_area_solucion,descripcion_area_solucion,telefono_principal,telefono_alternativo,id_estatus) VALUES (?,?,?,?,?)",array($datos["nom_area_solu"],$datos["descrip_area_solu"],$datos["tlf_prin_area_solu"],$datos["tlf_alter_area_solu"],$datos["area_solu_estatus"]));
					if($ins){
						return true;
					}else{
						return false;
					}

				}else{
					return false;	
				}
			}*/
			
			//--Modificado por gsantucci 18/06/2015: 
			public function registrar_areas_solucion($datos){
				//--Tuve que agregar '?' en servidor tomaba la cadena como integer...
				$ins = modeloBase::$bd_hd->execute("SELECT registrar_area_solucion(?,?,'?','?',?,?,?)",array(strtoupper($datos["nom_area_solu"]),$datos["descrip_area_solu"],$datos["tlf_prin_area_solu"],$datos["tlf_alter_area_solu"],$datos["area_solu_estatus"],$_SESSION["usuarios"],$_SESSION["ip"]));
				//$ins = modeloBase::$bd_hd->execute("SELECT registrar_area_solucion(?,?,?,?,?,?,?)",array(strtoupper($datos["nom_area_solu"]),$datos["descrip_area_solu"],$datos["tlf_prin_area_solu"],$datos["tlf_alter_area_solu"],$datos["area_solu_estatus"],$_SESSION["usuarios"],$_SESSION["ip"]));
				if($ins[0][0] == 1){
					return 1;
				}else if($ins[0][0] == 0){
					return 0;
				}else{
					return 2;
				}
			}
			//--Original
			/*public function actualizar_areas_solucion($datos){
				if($datos["nom_area_solu"] != ""){
					$actu = modeloBase::$bd_hd->execute("UPDATE areas_solucion SET nombre_area_solucion = ?, descripcion_area_solucion = ?, telefono_principal = ?, telefono_alternativo = ?, id_estatus = ? WHERE id_area_solucion = ?",array($datos["nom_area_solu"],$datos["descrip_area_solu"],$datos["tlf_prin_area_solu"],$datos["tlf_alter_area_solu"],$datos["area_solu_estatus"],$datos["id_area_solu"]));
					if($actu){
						return true;
					}else{
						return false;
					}
				}else{
					return false;
				}
			}*/

			//--Modificado por gsantucci 18/06/2015
			public function actualizar_areas_solucion($datos){
				//--Tuve que agregar '?' en servidor tomaba la cadena como integer...
				    $actu = modeloBase::$bd_hd->execute("SELECT actualizar_area_solucion(?,?,'?','?',?,?,?,?)",array(strtoupper($datos["nom_area_solu"]),$datos["descrip_area_solu"],$datos["tlf_prin_area_solu"],$datos["tlf_alter_area_solu"],$datos["area_solu_estatus"],$datos["id_area_solu"],$_SESSION["usuarios"],$_SESSION["ip"]));
					//$actu = modeloBase::$bd_hd->execute("SELECT actualizar_area_solucion(?,?,?,?,?,?,?,?)",array(strtoupper($datos["nom_area_solu"]),$datos["descrip_area_solu"],$datos["tlf_prin_area_solu"],$datos["tlf_alter_area_solu"],$datos["area_solu_estatus"],$datos["id_area_solu"],$_SESSION["usuarios"],$_SESSION["ip"]));
					if($actu[0][0]==1){
						return 1;
					}else if($actu[0][0]==0){
						return 0;
					}
					else{
						return 2;
					}
			}	

			public function registrar_departamento($datos){
				if($datos["depto"] != ""){
					$ins = modeloBase::$bd_hd->execute("INSERT INTO departamentos (departamento,telefono_principal,telefono_alternativo) VALUES (?,?,?)",array($datos["depto"],$datos["tlf_prin_depto"],$datos["tlf_alter_depto"]));
					if($ins){
						return true;
					}else{
						return false;
					}
				}else{
					return false;
				}
			}

			public function actualizar_departamento($datos){
				if($datos["depto"] != ""){
					$actu = modeloBase::$bd_hd->execute("UPDATE departamentos SET departamento = ?, telefono_principal = ?, telefono_alternativo = ? WHERE id_departamento = ?",array($datos["depto"],$datos["tlf_prin_depto"],$datos["tlf_alter_depto"],$datos["id_depto"]));
					if($actu){
						return true;
					}else{
						return false;
					}
				}else{
					return false;
				}
			}
			/*Original
			public function registrar_profile($datos){
				if($datos["profile"] != ""){
					$ins = modeloBase::$bd_hd->execute("INSERT INTO perfil_tecnico (perfil_tecnico,id_estatus) VALUES (?,?)",array($datos["profile"],$datos["profile_estatus"]));
					if($ins){
						return true;
					}else{
						return false;
					}
				}else{
					return false;				
				}
			}
			*/

			//--Modificado por gsantucci:18/06/2015
			public function registrar_profile($datos){
					$ins = modeloBase::$bd_hd->execute("SELECT registrar_perfil_tecnico(?,?,?,?)",array(strtoupper($datos["profile"]),$datos["profile_estatus"],$_SESSION["usuarios"],$_SESSION["ip"]));
					if($ins[0][0]==1){
						return 1;
					}else if($ins[0][0]==0){
						return 0;
					}else{
						return 2;				
					}
			}

			//Original
			/*public function actualizar_profile($datos){
				if($datos["profile"] != ""){
					$actu = modeloBase::$bd_hd->execute("UPDATE perfil_tecnico SET perfil_tecnico = ?, id_estatus = ? WHERE id_perfil_tecnico = ?",array($datos["profile"],$datos["profile_estatus"],$datos["id_profile"]));
					if($actu){
						//--Creado por gsantucci:12/05/2015
						//#Auditoria
						$opcion = $this->ejecutar_auditoria_param("Actualizar perfil","perfil_tecnico","Ejecutó actualizaci&oacute;n",$datos["id_profile"]);						
						if($opcion !="1") {
							return false;
						}
						//--
						return true;
					}else{
						return false;
					}
				}else{
					return false;
				}
			}
			*/

			//--Modificado por gsantucci: 18/06/2015
			public function actualizar_profile($datos){
					$actu = modeloBase::$bd_hd->execute("SELECT actualizar_perfil(?,?,?,?,?)",array(strtoupper($datos["profile"]),$datos["profile_estatus"],$datos["id_profile"],$_SESSION["usuarios"],$_SESSION["ip"]));
					if($actu[0][0]==1){
						return 1;
					}else if($actu[0][0]==0){
						return 0;
					}else{
						return 2;
					}
			}	

			public function get_pantallas_permisos($datos){
				if($datos["usu_permi"] != "" AND $datos["pan_permi"] != ""){
					$dat = modeloBase::$bd_hd->execute("SELECT * FROM permisos WHERE id_pantalla = ? AND id_tecnico = ?",array($datos["pan_permi"],$datos["usu_permi"]));	
					if($dat != "NO_DATA"){
						$resp = $dat[0]["acceso"].",".$dat[0]["incluir"].",".$dat[0]["modificar"].",".$dat[0]["buscar"].",".$dat[0]["imprimir"].",".$dat[0]["eliminar"].",".$dat[0]["anular"];
						return $resp;
					}else{
						return false;
					}
				}else{
					return false;
				}
			}
			////--Creado por gsantucci:12/05/2015
			//Ejecuto auditoria
			public function ejecutar_auditoria($proceso,$tabla,$descripcion,$sql){
			//--
				$id_campo = modeloBase::$bd_hd->execute($sql);
				if($id_campo[0][0] != "NO_DATA"){
					$dat = modeloBase::$bd_hd->execute("INSERT INTO 
															auditoria(
							            								usuario_sistema,
							            								ip,
							            								fecha_hora,
							            								proceso_ejecutado, 
							           									tabla_afectada,
							           									id_campo_tabla,
							           									descripcion_proceso
							           								)
									    				VALUES 
									    					(
									    						?,?,?,?,?,?,?
									    					)",array(
																		$_SESSION["usuarios"],
																		$_SESSION["ip"],
																		date("Y-m-d H:m:s"),
																		$proceso,
																		$tabla,
																		$id_campo[0][0],
																		$descripcion
									    					));
					return "1";
	    		}else
	    		{
	    			return "0";
	    		}			
			//--	
			}
			//--
			////--Creado por gsantucci:12/05/2015
			//Ejecuto auditoria: Con el parametro del ID de la tabla...
			public function ejecutar_auditoria_param($proceso,$tabla,$descripcion,$id_campo){
			//--
				$dat = modeloBase::$bd_hd->execute("INSERT INTO 
														auditoria(
						            								usuario_sistema,
						            								ip,
						            								fecha_hora,
						            								proceso_ejecutado, 
						           									tabla_afectada,
						           									id_campo_tabla,
						           									descripcion_proceso
						           								)
								    				VALUES 
								    					(
								    						?,?,?,?,?,?,?
								    					)",array(
																	$_SESSION["usuarios"],
																	$_SESSION["ip"],
																	date("Y-m-d H:m:s"),
																	$proceso,
																	$tabla,
																	$id_campo,
																	$descripcion
								    					));
					return "1";
			//--	
			}
			//--
			//Ejecuto auditoria: con un arreglo para obtener el id
			public function ejecutar_auditoria_arreglo($proceso,$tabla,$descripcion,$sql,$arreglo){
			$cadena = array();
			//--
				$c=0;
				foreach ($arreglo as $valor ) {
					$cadena[]=$valor;
				}
				$id_campo = modeloBase::$bd_hd->execute($sql,$cadena);
				$dat = modeloBase::$bd_hd->execute("INSERT INTO 
														auditoria(
						            								usuario_sistema,
						            								ip,
						            								fecha_hora,
						            								proceso_ejecutado, 
						           									tabla_afectada,
						           									id_campo_tabla,
						           									descripcion_proceso
						           								)
								    				VALUES 
								    					(
								    						?,?,?,?,?,?,?
								    					)",array(
																	$_SESSION["usuarios"],
																	$_SESSION["ip"],
																	date("Y-m-d H:m:s"),
																	$proceso,
																	$tabla,
																	$id_campo[0][0],
																	$descripcion
								    					));
					return "1";
			//--	
			}

		}
	?>