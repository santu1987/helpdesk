<?php
class reporteticketModel extends modeloBase{
	//--Método que se encarga de consultar los datos de tickets solventados por técnicos
	public function consulta_ticket($arreglo){
		$where = "";
		$valores_opcionales = "";
		if($arreglo["select_tecnico"]!="0"){
			$where = "AND c.id_tecnico=?";
			$valores_opcionales = $arreglo["select_tecnico"];		
		}
		$dat = modeloBase::$bd_hd->execute("SELECT 
												a.id_ticket,
												substr(a.descripcion,0,200) AS desc_ticket100,
												('CI:'||' '||cedula_usuario||' '||nombres_apellidos) AS datos_usuarios,
												(to_char(a.fecha_creacion,'DD-MM-YYYY')||' '||substr(a.hora_creacion::character varying,0,9)) AS tiempo_creacion,
												(c.nombres||' '||c.apellidos) AS tecnico_asignado,
												(to_char(b.fecha_asignacion,'DD-MM-YYYY')||' '||substr(b.hora_asignacion::character varying,0,9))AS tiempo_asignacion,
												(to_char(b.fecha_cierre,'DD-MM-YYYY')||' '||substr(b.hora_cierre::character varying,0,9)) AS tiempo_primera_respuesta,
												((a.fecha_creacion::character varying)||' '||substr(a.hora_creacion::character varying,0,9)) AS tiempo_creacion2,
												((b.fecha_asignacion::character varying)||' '||substr(b.hora_asignacion::character varying,0,9)) AS tiempo_asignacion2,
												((b.fecha_cierre::character varying)||' '||substr(b.hora_cierre::character varying,0,9)) AS tiempo_primera_respuesta2,
												c.id_tecnico,
												d.descripcion_solicitud
											FROM 
												tickets a
											LEFT JOIN 
												tickets_x_tecnicos b
											ON 
												a.id_ticket=b.id_ticket
											LEFT JOIN 
												tecnicos c
											ON 
												b.id_tecnico = c.id_tecnico
											INNER JOIN 
												tipo_solicitud d
											ON 
												d.id_tipo_solicitud = a.id_tipo_solicitud	
											WHERE 
												id_estatus_x_tecnico=3
											AND 
												c.id_estatus='9'	
											AND	
												a.fecha_creacion >= ?
											AND 
												a.fecha_creacion <= ? ".$where."
											order by 
												c.nombres||' '||c.apellidos,
												a.fecha_creacion"													
											,array($arreglo["fecha_desde"],$arreglo["fecha_hasta"],$valores_opcionales));
		return $dat;
		//
											/*c.id_tecnico, a.id_ticket*/
	}
	//--Metodo que se encarga de consultar cantidad de tickets resueltos por tecnicos para gráfico 1
	public function consultar_tickets_tecnicos($vector){
		$where = '';
		$valores_opcionales = "";
		$fecha1 = '';
		$fecha2 = '';
		//--
		//--Valido de que tipo de grafico proviene
		if($vector[3]=="2")
		{
			$where = "";
			$opciones_b = "";
		}elseif ($vector[3]=="1") {
		//-----	
			$where =" AND (SELECT 
							COUNT(*) 
						FROM 
							tickets_x_tecnicos b 
						LEFT JOIN 
							tickets d
						ON 
							d.id_ticket=b.id_ticket					
						WHERE 
							b.id_tecnico=a.id_tecnico 
						AND 
							d.fecha_creacion >= ?
						AND
							d.fecha_creacion <= ?)>0";
			$fecha1 = $vector[0];
			$fecha2 = $vector[1];				
		//------
		}
		else{
				$where = '';
				$fecha1 = '';
				$fecha2 = '';
		}	
		//--
		if($vector[2]!="0"){
			$where = "AND a.id_tecnico=?";
			$valores_opcionales = $vector[2];
		}
		$dat = modeloBase::$bd_hd->execute("SELECT 
													a.nombres||' '||a.apellidos,
													(SELECT 
															COUNT(*) 
														FROM 
															tickets_x_tecnicos b 
														LEFT JOIN 
															tickets d
														ON 
															d.id_ticket=b.id_ticket					
														WHERE 
															b.id_tecnico=a.id_tecnico 
														AND 
															d.fecha_creacion >= ?
														AND
															d.fecha_creacion <= ?
														AND 
															d.id_estatus = 3),
													(SELECT 
															COUNT(*) 
														FROM 
															tickets_x_tecnicos b 
														LEFT JOIN 
															tickets d
														ON 
															d.id_ticket=b.id_ticket					
														WHERE 
															d.fecha_creacion >= ?
														AND
															d.fecha_creacion <= ?
														AND 
															d.id_estatus = 3)
														FROM 
															tecnicos a
														WHERE
															id_estatus='9'	
														 ".$where."	
														order by 
															a.nombres||' '||a.apellidos
														",
														array($vector[0],$vector[1],$vector[0],$vector[1],$fecha1,$fecha2));
		return $dat;
	}
	//--Metodo que se encarga de consultar tecnicos con cantidad de tickets 0.
	public function consulta_notickets($vector){
		$dat = modeloBase::$bd_hd->execute("SELECT 
												a.nombres||' '||a.apellidos
											FROM 
													tecnicos a 
											WHERE 
													a.id_tecnico 
											NOT IN 
													(	SELECT 
															c.id_tecnico
														FROM 	
															tickets_x_tecnicos c
														WHERE 
															id_estatus_x_tecnico!=7
														and 
															id_estatus_x_tecnico!=4		
														AND
															c.fecha_asignacion >= ?
														AND 
															c.fecha_asignacion <= ?			
													)
											AND 
													a.id_estatus!=10",array($vector[0],$vector[1]));
		return $dat;
	}
	//--Metodo que se encarga de consultar departamentos con cantidad de tickets 0.
	public function consulta_notickets_dptos($vector){
		$dat = modeloBase::$bd_hd->execute("SELECT 
												d.departamento
											FROM
												departamentos	d
											WHERE 
												(SELECT
													count(*)
												FROM
													tickets a
												INNER JOIN
													usuarios b
												ON 
													a.cedula_usuario=b.cedula
												INNER JOIN 
													departamentos c
												ON 
													c.codigo_dpto=b.codigo_dpto
												WHERE
													c.codigo_dpto = d.codigo_dpto
												AND 
													a.id_estatus='3'
												AND 
													a.fecha_creacion >= ?
												AND
													a.fecha_creacion <= ?)=0	
											ORDER BY
												d.departamento",array($vector[0],$vector[1]));
		return $dat;											
	}
	//--Metodo consulta cuantos tickets tecnicos
	public function consulta_cuantos_tecnicos($vector){
		$dat = modeloBase::$bd_hd->execute("SELECT 
												COUNT(*) 
											FROM 
												tecnicos a 
											WHERE 
												id_estatus=9");
		return $dat;
	}
	//--Metodo consulta cuantos departamentos global
	public function consulta_cuantos_dptos($vector){
		$dat = modeloBase::$bd_hd->execute("SELECT 
												  COUNT(*)
											FROM 
												   departamentos");
		return $dat;
	}
	//--Metodo que consulta los promedios de tiempo de los tickets x tecnico
	public function consulta_ticket_promedio($arreglo_data){
		$dat = modeloBase::$bd_hd->execute("SELECT * from  promedio_tickets(?,?)",array($arreglo_data["fecha_desde"],$arreglo_data["fecha_hasta"]));
		return $dat;
	}
	//--Metodo que consulta los tickets atendidos por departamentos
	public function consultar_tickets_departamentos($arreglo_data){
		//--Armo el where.....
		$where = "";
		if(($arreglo_data["select_dpto"]!="")&&($arreglo_data["select_dpto"]!="0")){
			$where = "AND 
							b.codigo_dpto='".$arreglo_data["select_dpto"]."'";
		}
		$dat = modeloBase::$bd_hd->execute("SELECT 
												a.id_ticket,
												a.id_tipo_solicitud,
												e.descripcion_solicitud AS descripcion_solicitud,
												a.descripcion AS descripcion_ticket,
												(b.nombres||' '||b.apellidos) AS datos_usuarios,
												UPPER(g.nombres||' '||g.apellidos) AS nombre_tecnico,
												(to_char(a.fecha_creacion,'DD-MM-YYYY')||' '||substr(a.hora_creacion::character varying,0,9)) AS tiempo_creacion,
												(to_char(a.fecha_solucion,'DD-MM-YYYY')||' '||substr(a.hora_solucion::character varying,0,9)) AS tiempo_solucion,
												c.codigo_dpto AS codigo_departamento,
												UPPER(c.departamento) AS nombre_departamento
											FROM 
												tickets a
											INNER JOIN 
												tipo_solicitud e 
											ON 
												e.id_tipo_solicitud =  a.id_tipo_solicitud		
											INNER JOIN 
												usuarios b
											ON 
												a.cedula_usuario = b.cedula
											INNER JOIN 
												departamentos c
											ON 
												c.codigo_dpto = b.codigo_dpto
											INNER JOIN 
												tickets_x_tecnicos f
											ON 
												f.id_ticket = a.id_ticket
											INNER JOIN 
												tecnicos g
											ON 
												g.id_tecnico = f.id_tecnico				
											WHERE 
												a.id_estatus = '3'
											AND
												a.fecha_solucion>=?
											AND
												a.fecha_solucion<=?
											AND 
												g.id_estatus='9'	
											".$where."			
											ORDER BY 
												c.departamento,
												e.descripcion_solicitud,
												a.fecha_creacion,
												a.fecha_solucion",array($arreglo_data["fecha_desde"],$arreglo_data["fecha_hasta"]));
		return $dat;
	} 

	//--Metodo que consulta el resumén general departamentos tipo de solicitud y cantidad...
	public function consultar_general_tickets($arreglo_data){
		$dat = modeloBase::$bd_hd->execute("SELECT * FROM  consultar_cantidad_tickets_general(?,?)", array($arreglo_data["fecha_desde"],$arreglo_data["fecha_hasta"]));
		return $dat;
	}
	//--Metodo que consulta la cantidad de tickets atendidos por departamento segun tipo de solicitud
	public function consultar_cantidad_tickets_dpto($arreglo_data){
		$dat = modeloBase::$bd_hd->execute("SELECT * FROM consultar_cantidad_tickets_dpto(?,?,?)", array($arreglo_data["codigo_departamento"],$arreglo_data["fecha_desde"],$arreglo_data["fecha_hasta"]));
		return $dat;
	}
	//--Metodo que consulta cantidad de tickets atendidos por departamento
	public function consultar_cantidad_tigr($arreglo_data){
		$dat = modeloBase::$bd_hd->execute("SELECT 
												d.departamento,
												(SELECT
													count(*)
												FROM
													tickets a
												INNER JOIN
													usuarios b
												ON 
													a.cedula_usuario=b.cedula
												INNER JOIN 
													departamentos c
												ON 
													c.codigo_dpto=b.codigo_dpto
												WHERE
													c.codigo_dpto = d.codigo_dpto
												AND 
													a.id_estatus='3')
											FROM
												departamentos	d
											WHERE 
												(SELECT
													count(*)
												FROM
													tickets a
												INNER JOIN
													usuarios b
												ON 
													a.cedula_usuario=b.cedula
												INNER JOIN 
													departamentos c
												ON 
													c.codigo_dpto=b.codigo_dpto
												WHERE
													c.codigo_dpto = d.codigo_dpto
												AND 
													a.id_estatus='3')>0	
											ORDER BY
												d.departamento");
		return $dat;
	}
	/*//--Metodo que se encarga de consultar cantidad de tickets resueltos por tecnicos para gráfico 1
	public function consultar_tickets_tecnicos2($vector){
		$where = '';
		$valores_opcionales = "";
		if($vector[2]!="0"){
			$where = "AND a.id_tecnico=?";
			$valores_opcionales = $vector[2];
		}
		$dat = modeloBase::$bd_hd->execute("SELECT 
													a.id_tecnico,
													(a.nombres||' '||a.apellidos) AS tecnico_asignado,
													(SELECT 
															COUNT(*) 
														FROM 
															tickets_x_tecnicos b 
														LEFT JOIN 
															tickets d
														ON 
															d.id_ticket=b.id_ticket					
														WHERE 
															b.id_tecnico=a.id_tecnico 
														AND 
															d.fecha_creacion >= ?
														AND
															d.fecha_creacion <= ?)AS cuantos_tickets,
													(((SELECT 
															COUNT(*) 
														FROM 
															tickets_x_tecnicos b 
														LEFT JOIN 
															tickets d
														ON 
															d.id_ticket=b.id_ticket					
														WHERE 
															b.id_tecnico=a.id_tecnico 
														AND 
															d.fecha_creacion >= ?
														AND
															d.fecha_creacion <= ?)*100)/(SELECT 
																	COUNT(*) 
																		FROM 
																	tickets_x_tecnicos b
																	LEFT JOIN 
																		tickets d
																	ON 
																		d.id_ticket=b.id_ticket					
																	WHERE 
																		d.fecha_creacion >= ?
																	AND
																		d.fecha_creacion <= ?		 
														))AS porcentaje
														FROM 
														tecnicos a
														order by 
														a.nombres",
											array($vector[0],$vector[1],$vector[0],$vector[1],$vector[0],$vector[1]));
		return $dat;
	}*/
}	
?>