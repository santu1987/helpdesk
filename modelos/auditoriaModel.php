<?php
class auditoriaModel extends modeloBase{
	//Metodo que registra la auditoría realizada
	public function registrar_auditoria($vector){
		$dat = modeloBase::$bd_hd->execute("INSERT INTO 
													auditoria(
													            usuario_sistema,
													            ip, 
													            fecha_hora,
													            proceso_ejecutado, 
													            tabla_afectada,
													            id_campo_tabla,
													            descripcion_proceso)
													    VALUES (?,?,?,?,?,?,?)", 
											array(
																$_SESSION['usuarios'],
																$_SERVER['REMOTE_ADDR'],
																date('Y-m-d H:i:s'),
																$vector[0],
																$vector[1],
																$vector[2],
																$vector[3]
												));
		return $dat;
	}
}
?>