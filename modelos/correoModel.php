<?php
require_once(RUTA_SITIO."aplicacion/helpers/PHPMailermaster/class.phpmailer.php");
require_once(RUTA_SITIO."aplicacion/helpers/PHPMailermaster/class.smtp.php");
class correoModel extends modeloBase{
	//Metodo para realizar el envío del correo
	public function enviar_correo($usuario,$destinatario,$mensaje,$titulo)
	{
		//----------------------------------------------------------------
		$exp_dest  = explode("|",$destinatario);

		$mail = new PHPMailer();

		$body = $mensaje;

		$mail->IsSMTP();

		$mail->SMTPSecure = "ssl"; 

		// la dirección del servidor, p. ej.: smtp.servidor.com
		$mail->Host = "correo.avilatv.gob.ve";

		// dirección remitente, p. ej.: no-responder@miempresa.com
		$mail->From = $usuario;

		// nombre remitente, p. ej.: "Servicio de envío automático"
		$mail->FromName = "Sistema de Helpdesk";
		$mail->Port = 465;

		// asunto y cuerpo alternativo del mensaje
		$mail->Subject = $titulo;
		$mail->AltBody = $mensaje; 

		// si el cuerpo del mensaje es HTML
		$mail->MsgHTML($body);

		// podemos hacer varios AddAdress
		for($i=0;$i<count($exp_dest);$i++){
		    $mail->AddAddress($exp_dest[$i]);
		}

		// si el SMTP necesita autenticación
		$mail->SMTPAuth = true;

		// credenciales usuario
		//--Eran las credenciales anteriores
		/*$mail->Username = $usuario;
		$mail->Password = "espada171819"; */
		//--Las credenciales de la cuenta soporte
		$mail->Username = $usuario;
		$mail->Password = "50p0rt3";
		if(!$mail->Send()) {
			//echo "Error enviando: " . $mail->ErrorInfo;
			die ("-1");
		} else {
			//echo "¡¡Enviado!!";
			die("1");
		}
		//-------------------------------------------------------------
	}
	//Metodo para ajustar cuadros de contenido que seran enviados por medio del correo
	public function crear_cuadro($titulo,$encabezado,$descripcion_ticket,$nticket){
		$estructura_mensaje="<div>
									<div style='padding:1%;width:95%;height:auto;background-color:#006063;color:#fff;font-weight:bold;font-size:14px;'>".$encabezado."</div>
									<div style='width: 94.7%;height:auto;background-color:#fff;border-style:solid;border-color:#16a085;padding: 1%;'>	
										<div style='color:#000;font-size:32px;''>Ticket#".$nticket.":</div></br>
										<div style='color:#000;font-size:14px;'>".$descripcion_ticket."</div>
									</div>
									<div style='padding:1%;width:95%;height:auto;background-color:#006063;color:#fff;font-weight:bold;font-size:14px;'>Mensaje enviado a trav&eacute;s de Sistema de Helpdesk, por favor ingrese al sistema para verificar estos datos. </div>
							</div>";
		return $estructura_mensaje;					
	}
	//Metodo para ajustar cuadros de contenido que seran enviados por medio del correo
	public function crear_cuadro_apl($titulo,$encabezado,$descripcion_ticket,$napel,$n_ticket){
		$estructura_mensaje="<div>
									<div style='padding:1%;width:95%;height:auto;background-color:#006063;color:#fff;font-weight:bold;font-size:14px;'>".$encabezado."</div>
									<div style='width: 94.7%;height:auto;background-color:#fff;border-style:solid;border-color:#16a085;padding: 1%;'>	
										<div style='color:#000;font-size:32px;''>Ticket #".$n_ticket." Apelaci&oacute;n #".$napel.":</div></br>
										<div style='color:#000;font-size:14px;'>".$descripcion_ticket."</div>
									</div>
									<div style='padding:1%;width:95%;height:auto;background-color:#006063;color:#fff;font-weight:bold;font-size:14px;'>Mensaje enviado a trav&eacute;s de Sistema de Helpdesk, por favor ingrese al sistema para verificar estos datos. </div>
							</div>";
		return $estructura_mensaje;
	}
	//Metodo para cargar la lista de las apelaciones que seran enviadas en el correo
	public function cargar_apelaciones_listado($apelaciones_asignadas){
		$lista_apl = "<div>Usted tiene las siguientes apelaciones asignadas:</div></br>";
		//Recorro el vector
		foreach ($apelaciones_asignadas as $apl) {
			$lista_apl.="<div style='font-weight:bold;font-size:20px;'>".utf8_decode($apl)."</div>";
		}
		return $lista_apl;			
	}
	//Metodo para obtener datos: descripcion ticket y correo del usuario creador, para correo solución del ticket
	public function correo_datos_correo($n_ticket,$cedula_tecnico){
		$dat = modeloBase::$bd_hd->execute("SELECT
													a.correo_electronico_usuario,
													b.mensaje_solucion_atencion
											FROM 
													tickets a
											INNER JOIN 
													tickets_x_tecnicos 	b
											ON 
													a.id_ticket=b.id_ticket
											INNER JOIN 
													tecnicos c 
											ON 
													c.id_tecnico = b.id_tecnico				
											WHERE 
													b.id_ticket=?
											AND 
													c.cedula=?",
											array(
													$n_ticket,
													$cedula_tecnico
												));
		return $dat;
	}
	//Metodo para obtener los datos: descripcion apelacion de una apelación
	public function correo_datos_apelacion($n_ticket){
		$dat = modeloBase::$bd_hd->execute("SELECT 
													id_apelacion,
													observacion_apelacion
											FROM 
													apelaciones
											WHERE 
													id_ticket=?
											ORDER BY  
													id_apelacion ASC
											LIMIT 
													1",
											array($n_ticket));
		return $dat;
	}
	//Metodo para obtener los datos: descripcion de solución de apelación
	public function correo_datos_apelacion_solucion($n_ticket){
		$dat = modeloBase::$bd_hd->execute("SELECT 
												  	a.correo_electronico_usuario
											FROM 
													tickets a
											INNER JOIN 
													apelaciones b 
											ON 
													a.id_ticket = b.id_ticket				
											WHERE   
													id_apelacion=?",
											array($n_ticket));
		return $dat;
	}

}
?>
