<?php
	class misticketsModel extends modeloBase{

		public function get_tickets(){
			$dat = modeloBase::$bd_hd->execute("SELECT * FROM consultar_resumen_ticket(?) limit 50", array($_SESSION["cedula"]));
			return $dat;
		}
		public function cuantos_tickets(){
			$dat = modeloBase::$bd_hd->execute("SELECT count(*) FROM tickets WHERE id_estatus!='4' AND cedula_usuario=?", array($_SESSION["cedula"]));
			return $dat;
		}
		public function get_tickets_tabla($offset, $limit){
			$dat = modeloBase::$bd_hd->execute("SELECT * FROM consultar_resumen_ticket(?) limit ? offset ? ",array($_SESSION["cedula"],$limit,$offset));
			return $dat;
		}
	}
?>