<?php
class permisologiasModel extends modeloBase{
	//Metodo que devuelve la estructura de las permisologías de acceso según usuario
	public function accesomenu(){
		$dat = modeloBase::$bd_hd->execute("SELECT * FROM consultar_permisos_pantallas(".$_SESSION['cedula'].");");
		return $dat;
	}
	//Metodo que retorna los permisos por pantalla y usuario
	public function permiso_pantalla($id_pantalla){
		$dat = modeloBase::$bd_hd->execute("SELECT consultar_permisos_acciones(?,?);", array($id_pantalla,$_SESSION["cedula"]));
		return $dat;
	}
}
?>