<?php
	class apelacionesModel extends modeloBase{
		public function registrar_apelacion($vector){
			$apelacion2 = str_replace("\n"," ",$vector[0]);
			$apelacion2 = trim($apelacion2);
			$dat = modeloBase::$bd_hd->execute("SELECT 
														registrar_apelacion(?,?,?,?)",
												  array(
												  			$vector[1],
												  			$apelacion2,
												  			$_SESSION["usuarios"],
												  			$_SESSION["ip"]
												  		));
			return $dat;
		}
		public function consultar_mensajes_apelacion($vector){
			$dat = modeloBase::$bd_hd->execute("SELECT 
														a.cedula_usuario,
														b.fecha_apelacion,
														b.hora_apelacion,
														b.observacion_apelacion,
														b.id_apelacion,
														a.nombres_apellidos
												FROM 
														tickets a
												INNER JOIN 
														apelaciones b

												ON 
														b.id_ticket = a.id_ticket
												WHERE  
														a.id_ticket =?
												ORDER BY 
														b.id_apelacion DESC				
												",array($vector[0]));
			return $dat;	
		}

		public function consultar_mensajes_tecnicos_apl($valor){
			$dat = modeloBase::$bd_hd->execute("SELECT
														a.fecha_solucion,
														a.hora_solucion,
														a.mensaje_solucion,
														b.cedula AS cedula_tecnico,
														(b.nombres||' '||b.apellidos) AS nombres_tecnico,
														a.id_apelacion
												FROM  
														apelaciones_x_tecnicos a
												INNER JOIN 
														tecnicos b
												ON   
														a.id_tecnico = b.id_tecnico
												WHERE   
														a.id_apelacion=?
												AND 
														a.mensaje_solucion is not null											
												", array($valor));
			return $dat;
		}

		public function consultar_mensaje_tecnico($vector){
			$dat = modeloBase::$bd_hd->execute("SELECT
													a.id_ticket,
													a.fecha_cierre,
													a.hora_cierre,
													a.mensaje_solucion_atencion,	
													(b.nombres||' '||b.apellidos) AS nombres_tecnico,
													b.cedula AS cedula_tecnico
												FROM
													tickets_x_tecnicos a
												INNER JOIN 
													tecnicos b
												ON 
													b.id_tecnico=a.id_tecnico
												WHERE
													a.id_ticket =?
												AND 
													a.hora_cierre is not null
												AND 
													a.fecha_cierre is not null	
												ORDER BY 
													a.id_ticket_x_tecnico DESC
												",array($vector[0]));
			return $dat;														
		}
		
}
?>
