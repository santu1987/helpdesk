<?php
	class crearticketModel extends modeloBase{
	
		public function registrar_ticket($desc_ticket,$tipo_sol){
			$desc_ticket2 = str_replace("\n"," ",$desc_ticket);
			$desc_ticket2 = trim($desc_ticket2);
			//Armo el correo electronico del usuario
			$correo = $_SESSION['usuarios']."@avilatv.gob.ve";
			$dat = modeloBase::$bd_hd->execute("SELECT registrar_solicitud_ticket(?,?,?,?,?,?,?)",array($desc_ticket2,$tipo_sol,$_SESSION["cedula"],$_SESSION["nombre"],$correo,$_SESSION["usuarios"],$_SESSION["ip"]));
			/*$dat = modeloBase::$bd_hd->execute("INSERT INTO	tickets(descripcion, fecha_creacion, hora_creacion, cedula_usuario, id_estatus, nombres_apellidos, correo_electronico_usuario ) 
												VALUES (?,?,?,?,?,?,?)",array($desc_ticket2, date('Y-m-d'),date("H:i:s"),$_SESSION["cedula"],"1", $_SESSION["nombre"],$correo));*/
			return $dat;
		}
		public function eliminar_ticket($id_ticket){
			$dat = modeloBase::$bd_hd->execute("SELECT eliminar_ticket(?,?,?)",array($id_ticket,$_SESSION["usuarios"],$_SESSION["ip"]));
			return $dat;
		}
		//
		public function consultar_soluciones_tickets(){
			$dat = modeloBase::$bd_hd->execute("SELECT id_tipo_solicitud,descripcion_solicitud FROM tipo_solicitud WHERE id_estatus='9' order by descripcion_solicitud;");
			return $dat;
		}
	}

?>