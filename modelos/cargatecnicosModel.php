<?php
	class cargatecnicosModel extends modeloBase{
		//Metodos para consultar tecnicos asignados
		public function consultar_tecnicos_asignados($numero){
			$dat = modeloBase::$bd_hd->execute("SELECT 
														a.cedula,
														a.nombres,
														a.apellidos,
														b.id_ticket,
														a.id_tecnico as id_tec
												FROM 
														tecnicos a 
												INNER JOIN 
														tickets_x_tecnicos b
												ON 
														a.id_tecnico=b.id_tecnico				
												WHERE  
														b.id_ticket = ?
												AND 
														b.id_estatus_x_tecnico!=8
												AND 
														a.id_estatus=9",
												array($numero));
			return $dat;
		}
		//Metodo que consulta todos los tecnicos del sistema asociados a un ticket
		public function consultar_tecnicos_todos($numero){
			$dat = modeloBase::$bd_hd->execute("SELECT 
													a.cedula,
													a.nombres,
													a.apellidos,
													a.id_tecnico,
													(SELECT count(*) FROM tickets_x_tecnicos b WHERE a.id_tecnico=b.id_tecnico AND b.id_ticket=?)AS asignado_ticket,
													(SELECT count(*) FROM tickets_x_tecnicos b WHERE a.id_tecnico=b.id_tecnico AND b.id_ticket=? AND id_estatus_x_tecnico=8)AS eliminado_ticket,
													(SELECT count(*) FROM apelaciones_x_tecnicos c INNER JOIN apelaciones d ON c.id_apelacion=d.id_apelacion WHERE a.id_tecnico = c.id_tecnico AND d.id_ticket=? AND c.id_estatus_apelacion_x_tecnico!=8)AS asignado_apelaciones,
													(SELECT id_estatus FROM tickets d where d.id_ticket=? )AS estatus,
													a.correo_electronico_tecnico AS correo,
													(SELECT count(*) FROM tickets_x_tecnicos b WHERE a.id_tecnico=b.id_tecnico AND b.id_ticket=? AND id_estatus_x_tecnico=3)AS ticket_con_respuesta
												FROM
											 		tecnicos a
											 	WHERE
											 		a.id_estatus=9		
											 	order by
											 		a.nombres",
											 	array($numero,$numero,$numero,$numero,$numero));
			return $dat;			
		}
		//Metodo que consulta todos los técnicos
		public function consultar_tecnicos_select(){
			$dat = modeloBase::$bd_hd->execute("SELECT 
														a.id_tecnico,
														a.nombres,
														a.apellidos
												FROM 
													 	tecnicos a 
												WHERE 
														a.id_estatus=9
												ORDER BY 
														a.nombres");
			return $dat;
		}
		//Metodo para registrar técnicos
		public function registrar_tecnicos($matriz){
			$id_ticket = $matriz[0];
			$id_tecnico = $matriz[1];
			$opcion = $matriz[2];
			if($opcion == 1){
				$dat = modeloBase::$bd_hd->execute("SELECT registrar_tecnicos(?,?,?,?,?)",
												array(
														$id_tecnico,
														$id_ticket,
														1,
														$_SESSION["usuarios"],
														$_SESSION['ip']
												));	
			}else if($opcion == 2){
				$dat = modeloBase::$bd_hd->execute("SELECT registrar_tecnicos(?,?,?,?,?)",
												array(
														$id_tecnico,
														$id_ticket,
														2,
														$_SESSION["usuarios"],
														$_SESSION['ip']
													));	
			}
			
			return $dat[0][0];
		}
		//Metodo para revocar tickets a tecnicos
		public function eliminar_tecnico($id_ticket,$id_tecnico){
			$dat = modeloBase::$bd_hd->execute("SELECT 
														revocar_tecnico_ticket(?,?)",
												array($id_tecnico,$id_ticket));
			return $dat[0][0];				   	
		}
		//Metodo para revocar tickets a tecnicos
		public function consultar_estatus_tecnico($id_ticket,$id_tecnico){
			$dat = modeloBase::$bd_hd->execute("SELECT consultar_estatus(?,?)",
												array($id_ticket,$id_tecnico));
			return $dat[0][0];				   	
		}
	}

?>