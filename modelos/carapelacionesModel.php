<?php
class carapelacionesModel extends modeloBase{
	//Metodo que se encarga de consultar las apelaciones, y si ese tecnico se le fue asignada la misma
	public function consultar_apl_tecnico($id_ticket,$id_tecnico){
		$dat = modeloBase::$bd_hd->execute("SELECT 
												a.id_ticket,
												a.id_apelacion,
												a.observacion_apelacion,
												(SELECT count(*) FROM apelaciones_x_tecnicos b WHERE a.id_apelacion = b.id_apelacion AND b.id_tecnico = ? AND b.id_estatus_apelacion_x_tecnico!=8) AS asignado,
  											    (SELECT count(*) FROM apelaciones_x_tecnicos c WHERE a.id_apelacion = c.id_apelacion AND c.id_tecnico = ? AND c.id_estatus_apelacion_x_tecnico=8) AS desasignado,
											  	(SELECT count(*) FROM apelaciones_x_tecnicos d WHERE a.id_apelacion = d.id_apelacion AND d.id_tecnico = ? AND d.id_estatus_apelacion_x_tecnico=7) AS solucion
											  FROM 
												apelaciones a
											 WHERE 
												a.id_ticket=?",
											array($id_tecnico,$id_tecnico,$id_tecnico,$id_ticket));
		return $dat;
	}
	//Metodo para registrar las asignaciones a las apelaciones
	public function registrar_asignacion_apel($arreglo_check,$arreglo_marcado,$id_ticket,$id_tecnico){
		//-Recorro el arreglo check
		$i = 0;
		foreach ($arreglo_check as $id_apelacion ) {
					$dat = modeloBase::$bd_hd->execute("SELECT 
																registrar_apelacion_tecnico(?,?,?,?,?,?)",
														array($id_ticket,$id_tecnico,$id_apelacion,$arreglo_marcado[$i],$_SESSION["usuarios"],$_SESSION['ip']));
					if($dat[0][0]==0){
						return "0";
						break;
					}//
			$i++;
			}//
		return "1";		
	}
}	
?>