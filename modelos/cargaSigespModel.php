<?php
class cargaSigespModel extends modelobase{
	//--Metodos para realizar la conexión a la base de datos...
	//--Atributos
	private $conexion;
	protected $query;
	public $arreglo = array();
	public  $sql="";
	//--
	private $servidor="";
	private $clave="";
	private $usuario="";
	private $bd="";
	//-- Metodo constructor*/
	public function __construct($servidor,$clave,$usuario,$bd)
	{
		$this->query="";
		$this->servidor=$servidor;
		$this->clave=$clave;
		$this->usuario=$usuario;
		$this->bd=$bd;
	}
	//-- Metodo que permite conectarse a la bd
	private function conectar()
	{
		//valido la sesion antes de conectar
		$this->conexion=pg_connect('host='.$this->servidor. ' port=5432'. ' dbname='.$this->bd. ' user='.$this->usuario.' password='.$this->clave);
		if($this->conexion)
		{
			return 'SI';
		}
		else
		{
			return 'NO';
		}	
	}
	private function desconectar()
	{
		pg_close($this->conexion);
	}
	//--Metodo que permite ejecutar un query
	protected function enviarQuery($sql)
	{
		$this->conectar();
		$this->query = pg_query($sql);
		return $this->query;
	}
	//--Vectoriza el resultado de una consulta
	protected function vectorizar()
	{
		return pg_fetch_row($this->query);
	}
	//--Para insert, update, delete
	 public function execute($sql)
	{
		$result = $this->enviarQuery($sql);
		if($result){
			$arr = array();
			while($row = $this->vectorizar()){
				$arr[] = $row;
			}
		}else{
			$arr = "error";
		}
		return $arr;
	}
	//--Para ejecutar query
	public function ejecutar_query($query){
		$this->conectar();
		$rs = $this->execute($query);
		$this->desconectar();
		return $rs;
	}
	
}
//------------------------------------------------------------------------------------------------------------------------------------------
//--Consulto a la información de los usuarios
$obj = new cargaSigespModel("10.0.8.17","G4b0m4c0nd0%*","westerlund","db_avilatv_2015");
$recordset = $obj->ejecutar_query("SELECT * FROM  fn_usuarios_carnets('cedula','".$_SESSION["cedula"]."')");
//--Inserto en la tabla helpdesk
//$obj2 = new cargaSigespModel("10.0.2.28","123456","postgres","db_helpdesk");-->Local
$obj2 = new cargaSigespModel("10.0.8.8","Br0wnd4v1nc1%*","aldebaran","db_helpdesk1");
$recordset2 = $obj2->ejecutar_query("INSERT 
											INTO 
										usuarios
									    (
									  			cedula,
											  	nombres,
											  	apellidos,
											  	codigo_dpto
									    )
									    VALUES 
									    (
									   			'".$recordset[0][0]."',
									   			'".$recordset[0][1]."',
									   			'".$recordset[0][2]."',
									   			'".$recordset[0][3]."'
									   	)");
if($recordset2=="error")
	die($recordset[0][0]);
?>