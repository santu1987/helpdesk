<?php
class carauditoriaModel extends modeloBase{
	public function consultar_auditoria($offset,$limit){
		$dat = modeloBase::$bd_hd->execute("SELECT
													a.id_auditoria,
													a.ip,
													a.fecha_hora,
													a.proceso_ejecutado,
													a.tabla_afectada,
													a.id_campo_tabla,
													a.descripcion_proceso,
													a.usuario_sistema
											FROM 
													auditoria a
											ORDER BY 
													id_auditoria DESC
											limit
													?
											offset
													?",array($limit,$offset));
		return $dat;
	}
	public function cuantos_auditoria(){
			$dat = modeloBase::$bd_hd->execute("SELECT count(*) FROM auditoria ");
			return $dat;
	}
}
?>