<?php
class solticketModel extends modeloBase{
	//Metodo para cargalista de solicitudes tickets
	public function cargar_lista_soltickets($offset,$limit){
		$dat = modeloBase::$bd_hd->execute("SELECT 
													a.cedula_usuario,
													a.id_ticket,
													a.nombres_apellidos,
													(substring (a.descripcion,0,100)||'...') AS descripcion_ticket,
													a.fecha_creacion,
													a.hora_creacion,
													(a.descripcion) AS descripcion_ticket2,
													b.id_estatus_x_tecnico,
													d.descripcion_solicitud
											FROM 
													tickets a
											INNER JOIN 
													tickets_x_tecnicos b
											ON 
													b.id_ticket = a.id_ticket		
											INNER JOIN 
													tecnicos c		
											ON 
													b.id_tecnico = c.id_tecnico	
											INNER JOIN 
													tipo_solicitud d
											ON 
													d.id_tipo_solicitud = a.id_tipo_solicitud							
											WHERE
													c.cedula=?
											AND 
													a.id_estatus!=4	
											AND
													b.id_estatus_x_tecnico!=8			
											ORDER BY 
												a.id_ticket DESC
											limit 
													?
											offset
													?				
											",
											array($_SESSION['cedula'],$limit,$offset));
		return $dat;
	}
	//Metodo para calcular cuantos tickets solucion
	public function cuantas_solicitudes(){
		$dat = modeloBase::$bd_hd->execute("SELECT 
													count(*) 
											FROM 
													tickets a
											INNER JOIN 
													tickets_x_tecnicos b
											ON 
													b.id_ticket = a.id_ticket		
											INNER JOIN 
													tecnicos c		
											ON 
													b.id_tecnico = c.id_tecnico				
											WHERE
													c.cedula=?
											AND 
													a.id_estatus!=4	
											AND
													b.id_estatus_x_tecnico!=8",
											array($_SESSION["cedula"]));
		return $dat;
	}
	//Metodo para calcular cuantas solicitudes de apelaciones
	public function cuantas_solicitudes_apel(){
		$dat = modeloBase::$bd_hd->execute("SELECT 
													count(*) 
											FROM 
												apelaciones a
											INNER JOIN 
												tickets b
											ON 
												b.id_ticket = a.id_ticket
											INNER JOIN 
												apelaciones_x_tecnicos c
										    ON 
												a.id_apelacion = c.id_apelacion
											INNER JOIN 
												tecnicos d
											ON 
												d.id_tecnico = c.id_tecnico
											WHERE 
												d.cedula=?
											AND 
												c.id_estatus_apelacion_x_tecnico!=8	
											",
											array($_SESSION['cedula']));
		return $dat;
	}
	//Metodo para registrar solucion del tecnico a un ticket
	public function reg_solucion($vector){
		$solucion = str_replace("\n"," ",$vector[1]);
		$solucion2 = trim($solucion);
		$dat = modeloBase::$bd_hd->execute("SELECT
												  registrar_solucion_ticket	(?,?,?,?,?)",
											array($vector[0],$solucion2,$_SESSION["cedula"],$_SESSION["usuarios"],$_SESSION['ip']));
		return $dat;
	}
	//Metodo para registrar solucion de apelación 
	public function reg_solapel_ticket($vector){
		$solapel2 = str_replace("\n"," ",$vector[1]);
		$solapel = trim($solapel2);
		$dat = modeloBase::$bd_hd->execute("SELECT 
													reg_solucion_apelacion(?,?,?,?,?)",
											array($vector[0],$solapel,$_SESSION["cedula"],$_SESSION["usuarios"],$_SESSION['ip']));
		return $dat;
	}
	//Metodo para cargar listas de apelaciones
	public function cargapelticket($offset,$limit){
		$dat = modeloBase::$bd_hd->execute("SELECT 
												a.id_apelacion, 
												a.id_ticket, 
												a.fecha_apelacion,
												a.hora_apelacion,
												a.observacion_apelacion AS descripcion_apelacion,
												b.descripcion AS descripcion_ticket,
												c.id_tecnico,
												(d.nombres||' '||d.apellidos) AS nombres_apellidos_tecnico,
												c.id_estatus_apelacion_x_tecnico,
												b.nombres_apellidos AS nombres_apellidos_uscreador,
												b.cedula_usuario,
												e.descripcion_solicitud
											  FROM 
												apelaciones a
											  INNER JOIN 
												tickets b
											  ON 
												b.id_ticket = a.id_ticket
											  INNER JOIN 
												apelaciones_x_tecnicos c
											  ON 
												a.id_apelacion = c.id_apelacion
											  INNER JOIN 
												tecnicos d
											  ON 
												d.id_tecnico = c.id_tecnico
											  INNER JOIN 
												tipo_solicitud e
											  ON 
												e.id_tipo_solicitud = b.id_tipo_solicitud	
											WHERE 
												d.cedula=?
											AND 
												c.id_estatus_apelacion_x_tecnico!=8		
											ORDER BY 
												a.id_apelacion DESC 
											limit 
													?
											offset
													?",
											array($_SESSION['cedula'],$limit,$offset));
		return $dat;
	}

}
?>