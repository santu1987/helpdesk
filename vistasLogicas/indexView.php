<?php

	class indexView extends vistaBase{

		public function __construct(){}

		public function render_dinamico($html,$data){
			return $html;
		}
		
		
		public function render_vista($nombre_html,$html,$data_estatica,$data_dinamica){
			$html = $this->render_estatico($nombre_html,$html,$data_estatica);
			$html = $this->render_dinamico($html,$data_dinamica);
			print $html;
		}
	}

?>