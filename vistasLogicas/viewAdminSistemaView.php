<?php

	class viewAdminSistemaView extends vistaBase{

		public function __construct(){}

/*-------------INICIO FUNCIONES DEL MOD TECNICOS------------------*/

		public function render_tecnicos($html,$data){
			if(($html != "")&&(count($data["tecnicos"]) > 0)){
				$render 	= "";

				$match_cal = $this->set_match_identificador_dinamico($html,"<!--ROW_TECNICOS-->");

				if($data["tecnicos"] != "NO_DATA"){
					for($i=0;$i<count($data["tecnicos"]);$i++){
					
						$dicc = array(
							"{num}"		=> $data["tecnicos"][$i]["id_tecnico"],
							"{ced}"		=> $data["tecnicos"][$i]["cedula"],
							"{nom}"		=> $data["tecnicos"][$i]["nombres"],
							"{ape}"		=> $data["tecnicos"][$i]["apellidos"],
							"{ema}"		=> $data["tecnicos"][$i]["correo_electronico_tecnico"],
							"{per}"		=> $data["tecnicos"][$i]["perfil_tecnico"],
							"{idp}"		=> $data["tecnicos"][$i]["id_perfil_tecnico"],
							"{are}"		=> $data["tecnicos"][$i]["nombre_area_solucion"],
							"{ida}"		=> $data["tecnicos"][$i]["id_area_solucion"],
							"{des}"		=> $data["tecnicos"][$i]["descripcion_area_solucion"],
							"{sta}"		=> $data["tecnicos"][$i]["estatus"],
							"{idsta}" 	=> $data["tecnicos"][$i]["id_estatus"],
							"{fun}"		=> "tabla_technicians($i)",
							"{i}"		=> $i
						);
						$render.=str_replace(array_keys($dicc), array_values($dicc), $match_cal);

					}
					$html = str_replace($match_cal, $render, $html);
				}
				
			}
			return $html;
		}

		public function render_forms_tecnico($html,$data){
			if(($html != "")&&(count($data["pefil_tecnico"]) > 0)){
				$render 	= "";

				$match_cal = $this->set_match_identificador_dinamico($html,"<!--PERFIL_TECNICOS-->");

				if($data["pefil_tecnico"] != "NO_DATA"){
					for($i=0;$i<count($data["pefil_tecnico"]);$i++){
					
						$dicc = array(
							"{perfil}"		=> $data["pefil_tecnico"][$i][1],
							"{id_perfil}"	=> $data["pefil_tecnico"][$i][0]
						);
						$render.=str_replace(array_keys($dicc), array_values($dicc), $match_cal);
					}
					$html = str_replace($match_cal, $render, $html);
				}
			}
			return $html;
		}

		public function render_areas_solucion($html,$data){
			if(($html != "")&&(count($data["areas_solucion"]) > 0)){
				$render 	= "";

				$match_cal = $this->set_match_identificador_dinamico($html,"<!--AREA_SOLUCION-->");

				if($data["areas_solucion"] != "NO_DATA"){
					for($i=0;$i<count($data["areas_solucion"]);$i++){
					
						$dicc = array(
							"{area}"		=> $data["areas_solucion"][$i][1],
							"{id_area}"		=> $data["areas_solucion"][$i][0],
							"{descrip}"		=> $data["areas_solucion"][$i][2],
							"{num_i_select}"=> $i
						);
						$render.=str_replace(array_keys($dicc), array_values($dicc), $match_cal);
					}
					$html = str_replace($match_cal, $render, $html);
				}
			}
			return $html;
		}

		public function render_tec_estatus($html,$data){
			if(($html != "")&&(count($data["tec_estatus"]) > 0)){
				$render 	= "";

				$match_cal = $this->set_match_identificador_dinamico($html,"<!--TEC_ESTATUS-->");

				if($data["tec_estatus"] != "NO_DATA"){
					for($i=0;$i<count($data["tec_estatus"]);$i++){
					
						$dicc = array(
							"{estatus}"			=> $data["tec_estatus"][$i]["estatus"],
							"{id_estatus}"		=> $data["tec_estatus"][$i]["id_estatus"]
						);
						$render.=str_replace(array_keys($dicc), array_values($dicc), $match_cal);
					}
					$html = str_replace($match_cal, $render, $html);
				}
			}
			return $html;
		}

/*-------------FIN FUNCIONES DEL MOD TECNICOS------------------*/
/*-------------INICIO FUNCIONES DEL MOD AREAS SOLUCION------------------*/

		public function render_areas_solucion2($html,$data){
			if(($html != "")&&(count($data["areas_solucion2"]) > 0)){
				$render 	= "";

				$match_cal = $this->set_match_identificador_dinamico($html,"<!--ROW_AREAS_SOLUCION2-->");

				if($data["areas_solucion2"] != "NO_DATA"){
					for($i=0;$i<count($data["areas_solucion2"]);$i++){

						$dicc = array(
							"{id}"		=> $data["areas_solucion2"][$i]["id_area_solucion"],
							"{nom}" 	=> $data["areas_solucion2"][$i]["nombre_area_solucion"],
							"{des}"		=> $data["areas_solucion2"][$i]["descripcion_area_solucion"],
							"{tlf1}"	=> $data["areas_solucion2"][$i]["telefono_principal"],
							"{tlf2}"	=> $data["areas_solucion2"][$i]["telefono_alternativo"],
							"{fun}"		=> "tabla_areas_solucion($i)",
							"{sta}"		=> $data["areas_solucion2"][$i]["estatus"],
							"{id_sta}"	=> $data["areas_solucion2"][$i]["id_estatus"],
							"{i}"		=> $i,
							"{num}"		=> $i+1
						);
						$render.=str_replace(array_keys($dicc), array_values($dicc), $match_cal);

					}
					$html = str_replace($match_cal, $render, $html);
				}
				
			}
			return $html;
		}

		public function render_area_solu_estatus($html,$data){
			if(($html != "")&&(count($data["area_solu_estatus"]) > 0)){
				$render 	= "";

				$match_cal = $this->set_match_identificador_dinamico($html,"<!--AREA_SOLUCION_ESTATUS-->");

				if($data["area_solu_estatus"] != "NO_DATA"){
					for($i=0;$i<count($data["area_solu_estatus"]);$i++){
					
						$dicc = array(
							"{estatus}"		=> $data["area_solu_estatus"][$i]["estatus"],
							"{id_estatus}"	=> $data["area_solu_estatus"][$i]["id_estatus"]
						);
						$render.=str_replace(array_keys($dicc), array_values($dicc), $match_cal);

					}
					$html = str_replace($match_cal, $render, $html);
				}
				
			}
			return $html;
		}
/*-------------FIN FUNCIONES DEL MOD AREAS SOLUCION--------------------*/
/*-------------INICIO FUNCIONES DEL MOD TIPOSOL------------------------*/
	public function render_tiposol($html,$data){
		if(($html !="")&&(count($data["tiposol"])>0)){
			$render = "";
			$match_cal = $this->set_match_identificador_dinamico($html,"<!--row_tiposol-->");
			if($data["tiposol"] != "NO_DATA"){
				for($i=0;$i<count($data["tiposol"]);$i++){
					$dicc = array(
						"{id}"  					=> $data["tiposol"][$i]["id_tipo_solicitud"],
						"{descripcion_tipo_sol}" 	=> $data["tiposol"][$i]["descripcion_solicitud"],
						"{estatus}"					=> $data["tiposol"][$i]["estatus"],
						"{id_estatus}"				=> $data["tiposol"][$i]["id_estatus"],
						"{i}"						=>$i,
						"{num}"						=>$i+1,
						"{fun}"						=>"tabla_tiposol($i)"
					);
					$render.= str_replace(array_keys($dicc), array_values($dicc), $match_cal);
				}
				$html = str_replace($match_cal, $render, $html);
			}
		}
		return $html;
	}
	public function render_tipo_estatus($html,$data){
		if(($html != "")&&(count($data["tipos_estatus"]) > 0)){
			$render 	= "";

			$match_cal = $this->set_match_identificador_dinamico($html,"<!--TIPOSOL_ESTATUS-->");

			if($data["tipos_estatus"] != "NO_DATA"){
				for($i=0;$i<count($data["tipos_estatus"]);$i++){
				
					$dicc = array(
						"{id_estatus}"	=> $data["tipos_estatus"][$i]["id_estatus"],
						"{estatus}"		=> $data["tipos_estatus"][$i]["estatus"]

					);
					$render.=str_replace(array_keys($dicc), array_values($dicc), $match_cal);
				}
				$html = str_replace($match_cal, $render, $html);
			}
		}
		return $html;
	}
/*-------------FIN FUNCIONES DEL MOD TIPOSOL---------------------------*/
/*-------------INICIO FUNCIONES DEL MOD DEPARTAMENTOS------------------*/

		public function render_departamentos($html,$data){
			if(($html != "")&&(count($data["departamentos"]) > 0)){
				$render 	= "";

				$match_cal = $this->set_match_identificador_dinamico($html,"<!--ROW_DEPARTAMENTO-->");

				if($data["departamentos"] != "NO_DATA"){
					for($i=0;$i<count($data["departamentos"]);$i++){
					
						$dicc = array(
							"{id}"		=> $data["departamentos"][$i]["id_departamento"],
							"{depto}" 	=> $data["departamentos"][$i]["departamento"],
							"{tlf1}"	=> $data["departamentos"][$i]["telefono_principal"],
							"{tlf2}"	=> $data["departamentos"][$i]["telefono_alternativo"],
							"{fun}"		=> "tabla_departamentos($i)",
							"{i}"		=> $i,
							"{num}"		=> $i+1
						);
						$render.=str_replace(array_keys($dicc), array_values($dicc), $match_cal);

					}
					$html = str_replace($match_cal, $render, $html);
				}
				
			}
			return $html;
		}
/*-------------FIN FUNCIONES DEL MOD DEPARTAMENTOS------------------*/
/*-------------INICIO FUNCIONES DEL MOD PERFILES------------------*/

		public function render_profile($html,$data){
			if(($html != "")&&(count($data["profile"]) > 0)){
				$render 	= "";

				$match_cal = $this->set_match_identificador_dinamico($html,"<!--PERFIL_TECNICO-->");

				if($data["profile"] != "NO_DATA"){
					for($i=0;$i<count($data["profile"]);$i++){
					
						$dicc = array(
							"{id_profile}"	=> $data["profile"][$i]["id_perfil_tecnico"],
							"{profile}"		=> $data["profile"][$i]["perfil_tecnico"],
							"{id_estatus}"	=> $data["profile"][$i]["id_estatus"],
							"{estatus}"		=> $data["profile"][$i]["estatus"],
							"{fun}"			=> "tabla_profile($i)",
							"{num}"			=> $i+1,
							"{i}"			=> $i

						);
						$render.=str_replace(array_keys($dicc), array_values($dicc), $match_cal);
					}
					$html = str_replace($match_cal, $render, $html);
				}
			}
			return $html;
		}

		public function render_profile_estatus($html,$data){
			if(($html != "")&&(count($data["profile_estatus"]) > 0)){
				$render 	= "";

				$match_cal = $this->set_match_identificador_dinamico($html,"<!--PROFILE_ESTATUS-->");

				if($data["profile_estatus"] != "NO_DATA"){
					for($i=0;$i<count($data["profile_estatus"]);$i++){
					
						$dicc = array(
							"{id_estatus}"	=> $data["profile_estatus"][$i]["id_estatus"],
							"{estatus}"		=> $data["profile_estatus"][$i]["estatus"]

						);
						$render.=str_replace(array_keys($dicc), array_values($dicc), $match_cal);
					}
					$html = str_replace($match_cal, $render, $html);
				}
			}
			return $html;
		}

/*-------------FIN FUNCIONES DEL MOD PERFILES------------------*/
/*-------------INICIO FUNCIONES DEL MOD PERMISOS------------------*/
		public function render_user_per($html,$data){
			if(($html != "")&&(count($data["user_per"]) > 0)){
				$render 	= "";

				$match_cal = $this->set_match_identificador_dinamico($html,"<!--USUARIO_PERMISO-->");

				if($data["user_per"] != "NO_DATA"){
					for($i=0;$i<count($data["user_per"]);$i++){
					
						$dicc = array(
							"{nom-ape}"		=> $data["user_per"][$i]["nombres"]." ".$data["user_per"][$i]["apellidos"],
							"{id_tec}"		=> $data["user_per"][$i]["id_tecnico"]
						);
						$render.=str_replace(array_keys($dicc), array_values($dicc), $match_cal);
					}
					$html = str_replace($match_cal, $render, $html);
				}
			}
			return $html;
		}

		public function render_pantallas($html,$data){
			if(($html != "")&&(count($data["pantallas"]) > 0)){
				$render 	= "";

				$match_cal = $this->set_match_identificador_dinamico($html,"<!--PANTALLAS-->");

				if($data["pantallas"] != "NO_DATA"){
					for($i=0;$i<count($data["pantallas"]);$i++){
					
						$dicc = array(
							"{id_pan}"		=> $data["pantallas"][$i]["id_pantalla"],
							"{pan}"			=> $data["pantallas"][$i]["pantalla"]
						);
						$render.=str_replace(array_keys($dicc), array_values($dicc), $match_cal);
					}
					$html = str_replace($match_cal, $render, $html);
				}
			}
			return $html;
		}
/*-------------FIN FUNCIONES DEL MOD PERMISOS------------------*/

		public function render_dinamico($html,$data){
			$html = $this->render_tecnicos($html,$data);
			$html = $this->render_forms_tecnico($html,$data);
			$html = $this->render_areas_solucion($html,$data);
			$html = $this->render_tec_estatus($html,$data);
			$html = $this->render_user_per($html,$data);
			$html = $this->render_pantallas($html,$data);
			$html = $this->render_areas_solucion2($html,$data);
			$html = $this->render_area_solu_estatus($html,$data);
			$html = $this->render_departamentos($html,$data);
			$html = $this->render_profile($html,$data);
			$html = $this->render_profile_estatus($html,$data);
			$html = $this->render_tiposol($html,$data);
			$html = $this->render_tipo_estatus($html,$data);
			return $html;
		}
		public function render_vista($nombre_html,$html,$data_estatica,$data_dinamica){
			$html = $this->render_estatico($nombre_html,$html,$data_estatica);
			$html = $this->render_dinamico($html,$data_dinamica);
			print $html;
		}
	}


?>