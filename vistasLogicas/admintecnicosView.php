<?php
	class admintecnicosView extends vistaBase{
		public function __construct(){
			parent::__construct();
		}
		//--Metodo que renderiza de forma dinámica la consulta de tecnicos asignados a un ticket 
		public function render_dinamico($html,$data){
			$html = $this->render_admintecnicos($html,$data);//renderizo la consulta de administración de tickets
			return $html;
		}
		//--Metodo que renderiza la consulta de tecnicos
		public function render_admintecnicos($html,$data){
			$asignado_ticket = "";
			$asignado_apelacion = "";
			$btn_asig_apel= "";
			$vector = array("cedula"=>"","nombres"=>"","apellidos"=>"");
			$id_ticket = '';
			if(($html !="")&&(count($data) > 0)){
				$render = "";
				//
				$match_cal = $this->set_match_identificador_dinamico($html,"<!--row_tecnico_adm-->");
				//
				if($data["seleccion_tc"]=="NO_DATA")
				{
					$clase_adm_tecnico='hide';
				}else{
					$clase_adm_tecnico='show';
				}	
				for($i=0;$i<count($data["seleccion_tc"]);$i++)
				{
					//Validar colocación de titulo
					if(($i==0)&&($clase_adm_tecnico=='hide'))
					{
						$clase_adm_tecnico2="show";
					}else
					{
						$clase_adm_tecnico2="hide";
					}	
					//--Asigno los valores de la bd a un arreglo clave=>valor
					if(isset($data["seleccion_tc"][$i]["cedula"]))
					{
						$vector["cedula"] = $data["seleccion_tc"][$i]["cedula"];
						$vector["nombres"] = $data["seleccion_tc"][$i]["nombres"];
						$vector["apellidos"] = $data["seleccion_tc"][$i]["apellidos"];
						$id_ticket = $data["ticket"];
						$id_tecnico = $data["seleccion_tc"][$i]["id_tecnico"];
						$asignado_ticket = $data["seleccion_tc"][$i]["asignado_ticket"];
						$asignado_apelaciones = $data["seleccion_tc"][$i]["asignado_apelaciones"];
						$eliminado_ticket = $data["seleccion_tc"][$i]["eliminado_ticket"];
						$ticket_con_respuesta = $data["seleccion_tc"][$i]["ticket_con_respuesta"];
						$apelaciones = $data["apelaciones"];
						$btn = '#btn_asig_tk'.$i;
						$nombres_tecnico =$vector["nombres"]." ".$data["seleccion_tc"][$i]["apellidos"];
						$vector["correo"] = $data["seleccion_tc"][$i]["correo"];
						//--Valido si tiene permiso de revocar asignación de ticket
						if($data["vector_permisos"][7]=="t")
						{
							//--Si el técnico ya dió respuesta al ticket no puede ser revocado-------------------//
							if($ticket_con_respuesta>0)
							{
								$revocar_asignacion = "revocar_negado_ticket_respuesta();";
							}else//Si no tiene respuesta, si puede ser revocado---//
							{
								$revocar_asignacion = "revocar_asig_tk(".$id_ticket.",".$id_tecnico.",\"".$btn."\")";		
							}
							//-----------------------------------------------------------------------------------//
						}else
						{
							$revocar_asignacion = "revocar_negado();";
						}
					}
					//--Valido que si el técnico tiene asignado un ticket con estatus =3 /atendido el botón aparezca bloqueado....
					if($data["seleccion_tc"][$i]["estatus"]==3)
					{
						$clase_btn_bloqueado = "disabled";
					}else
					{
						$clase_btn_bloqueado = "";
					}
					//--Valido si tiene ticket asignado...
					if($eliminado_ticket>0)
					{
						$btn_asig_ticket = "<button type='button' class='btn btn-danger btn_asig' name='btn_asig_tk[]' id='btn_asig_tk".$i."' ".$clase_btn_bloqueado." title='Técnico revocado de este ticket' onclick='asignacion_bloqueada();'>Ticket Revocado <i class='fa fa-times'></i></button>";
					}
					else if($asignado_ticket==0)
					{
						//--Valido si tiene permiso de asignar técnico(incluir)
						if($data["vector_permisos"][2]=="t")
						{
							$btn_asig_ticket = "<button type='button' class='btn btn-cancelar btn_asig' name='btn_asig_tk[]' id='btn_asig_tk".$i."' ".$clase_btn_bloqueado." title='Asignar técnico a ticket' onclick='guardar_asig_tk(".$id_ticket.",".$id_tecnico.",\"".$btn."\",\"".$vector["nombres"]." ".$data["seleccion_tc"][$i]["apellidos"] ."\",\"".$vector["correo"]."\");'>Asignar Ticket</button>";
						}else
						{
							$btn_asig_ticket = "<button type='button' class='btn btn-cancelar btn_asig' title='No tiene permisos para asignar tickets' disabled><i class='fa fa-exclamation-circle'></i> No puede asignar </button>";
						}
					}else if($asignado_ticket>0)
					{
						$btn_asig_ticket = "<button type='button' class='btn btn-aceptar btn_asig' name='btn_asig_tk[]' id='btn_asig_tk".$i."' ".$clase_btn_bloqueado." title='Asignado a ticket' onclick='".$revocar_asignacion."'>Ticket Asignado <i class='fa fa-check'></i></button>";
					}
					if($apelaciones>0)
					{
						//valido si tiene apelación asignada...
						if($asignado_apelaciones==0)
						{
							//--Valido si tiene permiso de asignar técnico(incluir)
							if($data["vector_permisos"][2]=="t")
							{
								$btn_asig_apel = "<button type='button' class='btn btn-amarillo btn_asig' name='btn_asig_apel[]' id='btn_asig_apel".$i."' ".$clase_btn_bloqueado." title='Asignar técnico a apelación' onclick='mostrar_apleaciones_modal(".$id_ticket.",".$id_tecnico.",\"".$vector["nombres"]." ".$data["seleccion_tc"][$i]["apellidos"] ."\",\"".$vector["correo"]."\");'>Asignar Apelación</button>";
							}else
							{
								$btn_asig_apel = "<button type='button' class='btn btn-cancelar btn_asig' disabled title='No tiene permisos para asignar apelaciones'><i class='fa fa-exclamation-circle'></i> No puede asignar</button>";
							}
						}
						else if($asignado_apelaciones>0)
						{
							//--Valido si tiene permiso de asignar apelación(incluir)
							if($data["vector_permisos"][2]=="t")
							{
								$asig_apl = "mostrar_apleaciones_modal(".$id_ticket.",".$id_tecnico.",\"".$vector["nombres"]." ".$data["seleccion_tc"][$i]["apellidos"] ."\",\"".$vector["correo"]."\");";
							}else
							{
								$asig_apl = "apelacion_negado();";
							}	
							$btn_asig_apel = "<button type='button' class='btn btn-amarillo btn_asig' name='btn_asig_apel[]' id='btn_asig_apel".$i."' ".$clase_btn_bloqueado." title='Asignado a apelación' onclick='".$asig_apl."'>Asignado Apelación <i class='fa fa-check'></i></button>";
						}	
					}
					//--
					//Valido si no existe la imagen que coloque una predeterminada...
					//--
					$file = "http://appsigesp.avilatv.gob.ve/sno/fotospersonal/".$vector["cedula"];
					$file_headers = @get_headers($file);
					if($file_headers[0] == 'HTTP/1.1 404 Not Found') {
						//false
						$foto_tecnico = "../media/imagenes/user.png";
					}
					else {
						//true
						$foto_tecnico = "http://appsigesp.avilatv.gob.ve/sno/fotospersonal/".$vector["cedula"];
					}		
					//--
						$btn_eliminar_tec = "<button type='button' class='btn btn-danger btn_el_tec' name='btn_el_tec[]' id='btn_el_tec".$i."' ".$clase_btn_bloqueado." title='Revocar asignación de ticket' onclick='revocar_ticket_tec(".$id_ticket.",".$id_tecnico.")' ><i class='fa fa-times'></i></button>";
						$dicc = array(
							"{imagen_tecnico_adm}"  =>$foto_tecnico,
							"{nombres_tecnico_adm}" =>$nombres_tecnico,
							//"{nombres_tecnico_adm}" =>"http://appsigesp.avilatv.gob.ve/sno/fotospersonal/".$vector["cedula"],
							"{clase_adm_tecnico}"   =>$clase_adm_tecnico, 
							"{clase_adm_tecnico2}"  =>$clase_adm_tecnico2,
							"{btn_asig_ticket}"		=>$btn_asig_ticket,
							"{btn_asig_apel}"		=>$btn_asig_apel
						);
					$render.=str_replace(array_keys($dicc),array_values($dicc), $match_cal);
				}
				$html = str_replace($match_cal, $render, $html);
			}		
			return $html;
		}
		//--Metodo render vista
		public function render_vista($nombre_html,$html,$data_estatica,$data_dinamica){
			$html = $this->render_dinamico($html,$data_dinamica);
			print $html;
		}
	}
?>