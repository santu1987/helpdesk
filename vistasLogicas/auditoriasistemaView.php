<?php
class auditoriasistemaView extends vistaBase{
	public function __construct(){
		parent::__construct();
	}
	//--Metodo que renderiza de forma dinámica la consulta de auditoria del sistema
	public function render_dinamico($html,$data){
		$html = $this->render_audi($html,$data);//renderizo la comnsulta de auditorias
		return $html;
	}
	//--Metodo render_audi-Renderiza la consulta de auditorias
	public function render_audi($html,$data){
	//--cargo plantilla de asignacion tecnicos
		if(($html!="")&&(count($data)>0)){
			$render = "";
			$match_cal = $this->set_match_identificador_dinamico($html,"<!--row_auditoria-->");
				
				if($data["auditoria"]!="NO_DATA")
				{
					for($i=0;$i<count($data["auditoria"]);$i++){
						//--Configurando fecha-hora
						$fecha_hora='';
						if(isset($data["auditoria"][$i]["fecha_hora"]))
						{
							$fecha_hora=substr($data["auditoria"][$i]["fecha_hora"],8,2)."-".substr($data["auditoria"][$i]["fecha_hora"],5,2)."-".substr($data["auditoria"][$i]["fecha_hora"],0,4)." ".substr($data["auditoria"][$i]["fecha_hora"],10,9);
						}	
						//--Verifico que los campos no esten vacíos
						if(isset($data["auditoria"][$i]["id_auditoria"])){$arreglo["id_auditoria"] = $data["auditoria"][$i]["id_auditoria"];}else{$arreglo["id_auditoria"] ="";}
						if(isset($data["auditoria"][$i]["ip"])){$arreglo["ip"] = $data["auditoria"][$i]["ip"];}else{$arreglo["ip"]="";}
						if(isset($data["auditoria"][$i]["proceso_ejecutado"])){$arreglo["proceso_ejecutado"] = $data["auditoria"][$i]["proceso_ejecutado"];}else{$arreglo["proceso_ejecutado"] ="";}
						if(isset($data["auditoria"][$i]["tabla_afectada"])){$arreglo["tabla_afectada"] = $data["auditoria"][$i]["tabla_afectada"];}else{$arreglo["tabla_afectada"] ="";}
						if(isset($data["auditoria"][$i]["id_campo_tabla"])){$arreglo["id_campo_tabla"] = $data["auditoria"][$i]["id_campo_tabla"];}else{$arreglo["id_campo_tabla"] ="";}
						if(isset($data["auditoria"][$i]["descripcion_proceso"])){$arreglo["descripcion_proceso"] = $data["auditoria"][$i]["descripcion_proceso"];}else{$arreglo["descripcion_proceso"] ="";}
						if(isset($data["auditoria"][$i]["usuario_sistema"])){$arreglo["usuario_sistema"] = $data["auditoria"][$i]["usuario_sistema"];}else{ $arreglo["usuario_sistema"] = "";}
						//
						$dicc = array(
										"{numero_audi}" 	   =>$arreglo["id_auditoria"],
										"{ip}" 				   =>$arreglo["ip"],
										"{fecha_hora}"		   =>$fecha_hora,
										"{proceso_ejecutado}"  =>$arreglo["proceso_ejecutado"],
										"{tabla_afectada}"	   =>$arreglo["tabla_afectada"],
										"{id_campo_tabla}"	   =>$arreglo["id_campo_tabla"],
										"{descripcion_proceso}"=>$arreglo["descripcion_proceso"],
										"{usuario_sistema}"	   =>$arreglo["usuario_sistema"]	
						);
						$render.=str_replace(array_keys($dicc), array_values($dicc), $match_cal);
					}
				}
				$html = str_replace($match_cal, $render, $html);
			}	
				return $html;
	}	
	//--Metodo render vista
	public function render_vista($nombre_html,$html,$data_estatica,$data_dinamica ){
		$html = $this->render_dinamico($html,$data_dinamica);
		$html = $this->render_estatico($nombre_html,$html,$data_estatica);
		print $html;
	}
}
?>