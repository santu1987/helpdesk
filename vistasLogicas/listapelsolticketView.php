<?php
class listapelsolticketView extends VistaBase{
	public function __construct(){
			parent::__construct();
	}
	//Metodo para realizar el render dinamico de la vista
	public function render_dinamico($html,$data){
		$html = $this->render_apelaciones_sol($html,$data);
		return $html;
	}
	//Metodo para realizar la renderizacion de la vista
	public function render_vista($nombre_html,$html,$data_estatica,$data_dinamica){
			$html = $this->render_estatico($nombre_html,$html,$data_estatica);
			$html = $this->render_dinamico($html,$data_dinamica);
			print $html;
	}
	//Metodo para renderizar las apelaciones
	public function render_apelaciones_sol($html, $data){
		//cargo plantilla de tickets asignados
		if(($html != "")&&(count($data) > 0)){
			$render = "";
			$match_cal = $this->set_match_identificador_dinamico($html,"<!--row_sol_apelaciones-->");
			if($data["solapel"]!="NO_DATA"){
				for($i=0;$i<count($data["solapel"]);$i++){
				//--Dandole formato a la fecha
					$fecha_hora_calc = $data["solapel"][$i]["fecha_apelacion"]." ".$data["solapel"][$i]["hora_apelacion"];  
					$fecha_hora = '';
					$fecha_hora = $data["solapel"][$i]["fecha_apelacion"];
					$fecha_hora = substr($fecha_hora,8,2)."/".substr($fecha_hora,5,2)."/".substr($fecha_hora,0,4)." ".substr($data["solapel"][$i]["hora_apelacion"],0,8);
					//--Botones
					//Valido que se muestren los botones si se tiene permisos
					if($data["vector_permisos"][2]=="t")
					{
						switch ($data["solapel"][$i]["id_estatus_apelacion_x_tecnico"])
						{
							case 6:
								$btn_operaciones="<button type='button' class='btn btn-amarillo btn_ticket_asig' onclick='cargar_modal_solapel(1,".$data["solapel"][$i]["id_ticket"].",".$data["solapel"][$i]["id_apelacion"].",\"".$data["solapel"][$i]["descripcion_ticket"]."\",\"<b>".$data["solapel"][$i]["descripcion_solicitud"].":</b>"." ".$data["solapel"][$i]["descripcion_apelacion"]."\",\"".$fecha_hora."\",".$_SESSION["cedula"].")'>Responder</button>";
								break;
							case 7:
								$btn_operaciones="<button type='button' id='btn_apel_sol".$i."' class='btn btn-danger btn-dn2' onclick='cargar_modal_solapel(2,".$data["solapel"][$i]["id_ticket"].",".$data["solapel"][$i]["id_apelacion"].",\"".$data["solapel"][$i]["descripcion_ticket"]."\",\"<b>".$data["solapel"][$i]["descripcion_solicitud"].":</b>"." ".$data["solapel"][$i]["descripcion_apelacion"]."\",\"".$fecha_hora."\",".$_SESSION["cedula"].")'>Cerrado</button>";
								break;
						}
					}else
					{
						$btn_operaciones="<div class='alert alert-info mensaje_permiso_solucion'><i class='fa fa-exclamation-circle'></i> No tiene permisos para esta acci&oacute;n</div>";
					}	
					//--Calculo las horas transcurridas hata el momento...
					$horas_transcurridas = $this->calcula_tiempo($fecha_hora_calc,date("Y-m-d H:i:s"));
					//--
					$dicc = array(
									"{nticket}"					=>$data["solapel"][$i]["id_ticket"],
									"{descripcion_ticket}" 		=>"<b>".$data["solapel"][$i]["descripcion_solicitud"].":</b>"." ".$data["solapel"][$i]["descripcion_ticket"],
									"{descripcion_apelacion}"	=>$data["solapel"][$i]["descripcion_apelacion"],
									"{btn_operaciones}"			=>$btn_operaciones,
									"{fecha_hora_apel}"			=>$fecha_hora,
									"{napel}"					=>$data["solapel"][$i]["id_apelacion"]		
					);
					$render.=str_replace(array_keys($dicc),array_values($dicc), $match_cal);
				//---	
				}
				$html = str_replace($match_cal, $render, $html);
			}
		}
		return $html;
	}
	//--Metodo para calcular diferencias entre dos fechas y horas
	public function calcula_tiempo($start_time, $end_time) { 
	    $total_seconds = strtotime($end_time) - strtotime($start_time); 
	    $horas              = floor ( $total_seconds / 3600 );
	    $minutes            = ( ( $total_seconds / 60 ) % 60 );
	    $seconds            = ( $total_seconds % 60 );
	    $time['horas']      = str_pad( $horas, 2, "0", STR_PAD_LEFT );
	    $time['minutes']    = str_pad( $minutes, 2, "0", STR_PAD_LEFT );
	    $time['seconds']    = str_pad( $seconds, 2, "0", STR_PAD_LEFT );
	    $time               = implode( ':', $time );
	    return $time;
	}
	//--
}
?>