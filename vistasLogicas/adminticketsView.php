<?php
	class adminticketsView extends vistaBase{
		public function __construct(){
		parent::__construct();
		}
		//--Metodo que renderiza de forma dinámica la consulta de administración de tickets
		public function render_dinamico($html,$data){
			$html = $this->render_adminticket($html,$data);//renderizo la consulta de administración de tickets
			return $html;
		}
		//--Metodo que renderiza la consulta de administracion de tickets
		public function render_adminticket($html,$data){
			//--cargo plantilla de asignacion tecnicos
			if(($html !="")&&(count($data) > 0)){
				$render = "";
				$match_cal = $this->set_match_identificador_dinamico($html,"<!--row_admintickets-->");
				if($data["admintickets"]!="NO_DATA")
				{
					for($i=0;$i<count($data["admintickets"]);$i++){
						//--Creo un case para determinar el color del span estatus
						switch($data["admintickets"][$i]["id_estatus"])
						{
							case 1:
								$color_label = "label-primary";
								$icono = "<i class='fa fa-clock-o'></i>";
							break;
							case 2:
								$color_label = "label-verde";
								$icono = "<i class='fa fa-check'></i>";
							break;
							case 3:
								$color_label = "label-rojo";
								$icono = "<i class='fa fa-thumbs-o-up'></i>";
							break;
							case 5:
								$color_label = "label-amarillo";
								$icono = "<i class='fa fa-exclamation'></i>";
							break;	
						}
						//--Dandole formato a la fecha
						$fecha_hora_calc = $data["admintickets"][$i]["fecha_creacion"]." ".$data["admintickets"][$i]["hora_creacion"];  
						$fecha_hora = '';
						$fecha_hora = $data["admintickets"][$i]["fecha_creacion"];
						$fecha_hora = substr($fecha_hora,8,2)."/".substr($fecha_hora,5,2)."/".substr($fecha_hora,0,4)." ".substr($data["admintickets"][$i]["hora_creacion"],0,8);
						//--
						//--Botones
						//--Valido permisología: acceso para la asignación de técnicos a ticket
						if(isset($data["permisos_astec"]))//Si existe el vector
						{
							if($data["permisos_astec"][0]=="1")//Si es técnico
							{
								if($data["permisos_astec"][1]=="t")
								{
									//--Valido si el estatus es cerrado que bloquee el boton de acciones
									$btn_acciones="<button type='button' class='btn btn-aceptar' onclick='cargar_modal_astecnico(".$data["admintickets"][$i]["id_ticket"].",".$data["admintickets"][$i]["cedula_usuario"].",\"".$data["admintickets"][$i]["nombres_apellidos"]."\",\"".$fecha_hora."\",\""."<b>".$data["admintickets"][$i]["descripcion_solicitud"].":</b>"." ".$data["admintickets"][$i]["descripcion_ticket2"]."\",".$data["admintickets"][$i]["apelacion_ticket"].",".$data["admintickets"][$i]["id_estatus"].")'>Acciones</button>";
								}else
								{
									$btn_acciones="<div class='alert alert-info'><i class='fa fa-exclamation-circle'></i> No tiene permisos para asignaci&oacute;n de tickets</div>";
								}
							}else
							{
									$btn_acciones="";
							}	
						}	
						if($data["admintickets"][$i]["id_estatus"]==3){
							$this->load->modelo("caradmintickets");//Cargo el modelo para consultar fecha y hora de cierre
							$fecha_restar = $this->caradmintickets->consultar_fecha_hora($data["admintickets"][$i]["id_ticket"]);//Cargo el metodo que consulta la fecha y hora del ticket.
						}else{
							$fecha_restar = date("Y-m-d H:i:s");
						}
						//--Calculo las horas transcurridas hata el momento...
						$obj2 = new allinoneHelper();
						$horas_transcurridas = $obj2->calcula_tiempo($fecha_hora_calc,$fecha_restar);
						//--
						$dicc = array(
										"{imagen_us_tad}"         =>"http://appsigesp.avilatv.gob.ve/sno/fotospersonal/".$data["admintickets"][$i]["cedula_usuario"],
										"{nombres_apellidos_tad}" =>$data["admintickets"][$i]["nombres_apellidos"],
										"{ticket_ad}"			  =>$data["admintickets"][$i]["id_ticket"],
										"{descripcion_tad}"	 	  =>"<b>".$data["admintickets"][$i]["descripcion_solicitud"].":</b>"." ".$data["admintickets"][$i]["descripcion_ticket"],
										"{tiempo_tad}"			  =>$horas_transcurridas." Hrs",
										"{acciones_tad}"		  =>$btn_acciones,
										"{fecha_hora_adm}"		  =>$fecha_hora,
										"{descripcion2}"		  =>$data["admintickets"][$i]["descripcion_ticket2"],
										"{nombre_us_creador}"     =>$data["admintickets"][$i]["nombres_apellidos"],
										"{cedula_us_creador}"     =>$data["admintickets"][$i]["cedula_usuario"],
										"{estatus}"				  =>"<div class='label_estatus'><span class='label ".$color_label."'>".$icono." ".$data["admintickets"][$i]["estatus"]."</span></div>"
						);
						$render.=str_replace(array_keys($dicc),array_values($dicc), $match_cal);
					}
				}	
				$html = str_replace($match_cal, $render, $html);
			}
			return $html;
		}
		//--Metodo render vista
		public function render_vista($nombre_html,$html,$data_estatica,$data_dinamica){
			$html = $this->render_dinamico($html,$data_dinamica);
			$html = $this->render_estatico($nombre_html,$html,$data_estatica);
			print $html;
		}
		//--
	}
?>