<?php
class tablaleyendaView extends vistaBase{
	public function __construct(){
		parent::__construct();
	}
	//--Metodo que renderiza de forma dinámica la consulta de tabla leyenda grafico 3
	public function render_dinamico($html,$data){
		$html = $this->render_tabla_leyenda($html,$data);//renderizo la consulta de administración de tickets
		return $html;
	}
	//--Metodo que renderiza la tabla leyenda del grafico 3
	public function render_tabla_leyenda($html,$data){
		//--
		if((($html)!="")&&(count($data)>0)){
			$render = "";
			$match_cal = $this->set_match_identificador_dinamico($html,"<!--row_tabla_leyenda_g3-->");
			if($data["tec_promedio"]!="NO_DATA")
			{
				for($i=0;$i<count($data["tec_promedio"]);$i++){
				//--
					$dicc = array(
									"{nombre_tecnico}"   => $data["tec_promedio"][$i][1],
									"{tiempo_resp_acum}" => $data["tec_promedio"][$i][2],
									"{tiempo_prom_resp}" => $data["tec_promedio"][$i][3],
									"{n_tickets}"		 => $data["tec_promedio"][$i][6]
					);
					$render.=str_replace(array_keys($dicc),array_values($dicc), $match_cal);
				//--	
				}
			}
			$html = str_replace($match_cal, $render, $html);	
		}
		return $html;
		//--
	}
	//--Metodo render vista
	public function render_vista($nombre_html,$html,$data_estatica,$data_dinamica){
		$html = $this->render_dinamico($html,$data_dinamica);
		$html = $this->render_estatico($nombre_html,$html,$data_estatica);
		print $html;
	}
	//--	
}
?>