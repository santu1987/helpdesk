<?php
	class apelacionestecnicosView extends vistaBase{
		public function __construct(){
			parent::__construct();
		}
		//--Metodo que renderiza de forma dinámica la consulta de tecnicos asignados a un ticket 
		public function render_dinamico($html,$data){
			$html = $this->render_apl_tecnico($html,$data);//renderizo la consulta de administración de tickets
			return $html;
		}
		//--Metodo que renderiza la apelacion tecnicos
		public function render_apl_tecnico($html,$data){
			//--cargo plantilla de asignacion apelaciones tecnicos
			if(($html !="")&&(count($data) > 0)){
				$render = "";
				$match_cal = $this->set_match_identificador_dinamico($html,"<!--row_apl_tec-->");
				if($data["apelaciones"]!="NO_DATA")
				{
					for($i=0;$i<count($data["apelaciones"]);$i++){
					//nombre del check
					$vector["id_check"] = "apelciones_tec".$i;
					$vector["name_check"] = "apelaciones_tec[]";
					//Si esta asignado
					if($data["apelaciones"][$i]["asignado"]!=0){
						$vector["checked"] = "checked";
					}
					else{
						$vector["checked"] = "";
					}
					//Si fue desasignado
					if($data["apelaciones"][$i]["desasignado"]!=0){
						$vector["desasignado"]="cargar_mensaje_desasignado(this);";
					}
					//Si fue solucionado
					if($data["apelaciones"][$i]["solucion"]!=0){
						$vector["desasignado"]="cargar_mensaje_solucionado(this);";
					}
					else
					{
						$vector["desasignado"]="";
					}
					//	
						$dicc = array(
										"{id_check}"        	 =>$vector["id_check"],
										"{name_check}" 			 =>$vector["name_check"],
										"{checked}"			     =>$vector["checked"],
										"{desc_apelacion}"	 	 =>"<span class='titulosd_apl_asg'>Apelación #".$data["apelaciones"][$i]["id_apelacion"]."</span> : ".$data["apelaciones"][$i]["observacion_apelacion"],
										"{id_apelacion}"		 =>$data["apelaciones"][$i]["id_apelacion"],
										"{desasignado}"			 =>$vector["desasignado"],
										"{listado_selec_apelaciones}" =>"listado_selec_apelaciones"												
						);
						$render.=str_replace(array_keys($dicc),array_values($dicc), $match_cal);
					//	
					}
				}	
				$html = str_replace($match_cal, $render, $html);
			}
			return $html;
		}
		//--Metodo render vista
		public function render_vista($nombre_html,$html,$data_estatica,$data_dinamica){
			$html = $this->render_dinamico($html,$data_dinamica);
			$html = $this->render_estatico($nombre_html,$html,$data_estatica);
			print $html;
		}
}		
?>		