<?php
class apelacioneslistadosView extends vistaBase{
	
	public function __construct(){
		parent::__construct();
	}
	//--Metodo que renderiza de forma dinámica mensajes técnicos y apelaciones
	public function render_dinamico($html,$data){
		$html = $this->render_mensajes_tecnicos($html,$data);//renderizo bloque de mensajes tecnicos
		$html = $this->render_apelacion($html,$data);//renderizo bloque de apelacion
		return $html;
	}
	//--Metodo que renderiza los mensajes de respuesta de los tecnicos
	private function render_mensajes_tecnicos($html,$data){
		if(($html != "")&&(count($data) > 0)){
			$render = "";
			$match_cal = $this->set_match_identificador_dinamico($html,"<!--row_mensaje_tecnico-->");
			//-
			if($data["mensajes_tecnicos"]!="NO_DATA")
			{
				for($i=0;$i<count($data["mensajes_tecnicos"]);$i++){
					//Dando el formato de las fechas
					//fecha cierre
					if($data["mensajes_tecnicos"][$i]["fecha_cierre"])
					{
						$fecha_cierre='';
						$fecha_cierre=$data["mensajes_tecnicos"][$i]["fecha_cierre"];
						$fecha_cierre=substr($fecha_cierre,8,2)."/".substr($fecha_cierre,5,2)."/".substr($fecha_cierre,0,4);	
					}
					//En caso de que no existan registros que no muestre el div
					$clase_respuesta_tec = '';
					if($data["mensajes_tecnicos"]=="NO_DATA"){
						$clase_respuesta_tec = 'hide';
					}
					else{
					//
						$dicc = array(
									"{imagen_tecnico}"	=>"http://appsigesp.avilatv.gob.ve/sno/fotospersonal/".$data["mensajes_tecnicos"][$i]["cedula_tecnico"],
									"{nombres_tecnico}"   =>$data["mensajes_tecnicos"][$i]["nombres_tecnico"],
									"{mensaje_solucion_atencion}" =>$data["mensajes_tecnicos"][$i]["mensaje_solucion_atencion"],
									"{datos_sol}"  				  =>$fecha_cierre." ".substr($data["mensajes_tecnicos"][$i]["hora_cierre"],0,8),
									"{show_hide_resp_mtec}"       =>$clase_respuesta_tec,
									"{clase_imagen_resptec}"      =>"border_mensaje_tecnico",
									"{accion_tecnico}"			  =>"&nbspRespondi&oacute; Ticket:"	
								);
						$render.=str_replace(array_keys($dicc), array_values($dicc), $match_cal);
					}
				}
			}				
			//-
			$html = str_replace($match_cal, $render, $html);
		}
		return $html;
	}
	//--Metodo que renderiza las apelaciones
	private function render_apelacion($html,$data){
		if(($html != "")&&(count($data) > 0)){
			$render = "";
			$this->load->plantilla("respuesta_apelaciones");
			$match_cal = $this->set_match_identificador_dinamico($html,"<!--row_mensaje-->");
			$match_cal2 = $this->set_match_identificador_dinamico($this->respuesta_apelaciones,"<!--row_tec_apl-->");
			$this->load->modelo("apelaciones");
			if($data["mensajes"]!="NO_DATA")
			{
				//---
				for($i=0;$i<count($data["mensajes"]);$i++){
					//--Dando el formato de las fechas
					//--fecha apelacion
					if($data["mensajes"][$i]["fecha_apelacion"])
					{
						$fecha_apl='';
						$fecha_apl=$data["mensajes"][$i]["fecha_apelacion"];
						$fecha_apl=substr($fecha_apl,8,2)."/".substr($fecha_apl,5,2)."/".substr($fecha_apl,0,4);	
					}
					//--Validación para ocultar o mostrar cuadro de respuesta tećnicos en apelaciones
					if(isset($data["mensajes"][$i]["nombres_tecnico"]))	
					{
						$clase_respuesta_apl = 'show';
					}else
					{
						$clase_respuesta_apl = 'hide';
					}
					//--Validación para ocultar o mostrar apelaciones
					$clase_apl = 'show';
					if($data["mensajes"]=="NO_DATA")	
					{
						$clase_apl = 'hide';
						$clase_respuesta_apl = 'hide';
					}
					//--Valido si != NO_DATA
					if($data["mensajes"]!="NO_DATA"){
					//no data apl	
						$dicc = array(
									"{imagen_us_apl}"		  =>"http://appsigesp.avilatv.gob.ve/sno/fotospersonal/".$data["mensajes"][$i]["cedula_usuario"],
									"{nombre_us_apl}"   	  =>$data["mensajes"][$i]["nombres_apellidos"],
									"{observacion_apelacion}" =>$data["mensajes"][$i]["observacion_apelacion"],
									"{datos_apl}"  			  =>$fecha_apl." ".substr($data["mensajes"][$i]["hora_apelacion"],0,8),
									"{show_hide_apl}"		  =>$clase_apl,
									"{clase_imagen_respapel}" =>"border_mensaje_apl",
									"{accion_tecnico}"		  =>"&nbspApel&oacute;:"	
								);
						$render.= str_replace(array_keys($dicc), array_values($dicc), $match_cal);
						$resp_apl = $this->apelaciones->consultar_mensajes_tecnicos_apl($data["mensajes"][$i]["id_apelacion"]);
						//--Valido si resp_apl != "NO_DATA"
						if($resp_apl != "NO_DATA"){
							for($j=0;$j<count($resp_apl);$j++){
								//--fecha de solucion
								if($resp_apl[$j][0])
								{
									$fecha_sol="";
									$fecha_sol=$resp_apl[$j][0];
									$fecha_sol=substr($fecha_sol,8,2)."/".substr($fecha_sol,5,2)."/".substr($fecha_sol,0,4);
								}
								$dicc2 = array(
													"{imagen_tec}" 			  =>"http://appsigesp.avilatv.gob.ve/sno/fotospersonal/".$resp_apl[$j][3],
													"{nombre_tec}" 			  =>$resp_apl[$j][4],
													"{descripcion_tec}" 	  =>$resp_apl[$j][2],
													"{datos_tec}" 			  =>$fecha_sol."  ".substr($resp_apl[$j][1],0,8),
													"{clase_respuesta_apl}"   =>"border_respuesta_apl",
													"{accion_tecnico}"		  =>"&nbspRespondi&oacute; Apelaci&oacute;n:"	
											);
								$render.=str_replace(array_keys($dicc2), array_values($dicc2), $match_cal2);
							}//fin de for 
						}//fin if no data apl
					}
				}
				//---
			}	
			$html = str_replace($match_cal, $render, $html);
		}
		return $html;
	}
	//--Metodo render vista
	public function render_vista($nombre_html,$html,$data_estatica,$data_dinamica){
			$html = $this->render_dinamico($html,$data_dinamica);
			print $html;
		}
}
?>