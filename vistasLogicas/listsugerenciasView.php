<?php
	class listsugerenciasView extends vistaBase{
		//--Constructor
		public function __construct(){
			parent::__construct();
		}
		//--Metodo que renderiza la vista de manera dinámica
		public function render_dinamico($html,$data){
			return $html;
		}
		//Metodo que renderiza la vista
		public function render_vista($nombre_html,$html,$data_estatica,$data_dinamica){
			$html = $this->render_estatico($nombre_html,$html,$data_estatica);
			$html = $this->render_dinamico($html,$data_dinamica);
			print $html;
		}
	}
?>