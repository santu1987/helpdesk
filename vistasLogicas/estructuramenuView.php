<?php
	class estructuramenuView extends vistaBase{
		public function __construct(){
		parent::__construct();
		}
		//--Metodo render dinamico
		public function render_dinamico($html,$data){
			$html = $this->render_menu($html,$data);
			return $html;
		}
		//--Metodo rdner_menu -Renderiza la consulta del menú de acceso al sistema
		public function render_menu($html,$data){
		//--cargo plantilla de asignacion tecnicos
			if(($html!="")&&(count($data)>0)){
				$render = "";
				$match_cal = $this->set_match_identificador_dinamico($html,"<!--row_menu-->");
					
					if($data["menu"]!="NO_DATA")
					{
						for($i=0;$i<count($data["menu"]);$i++){
							//--
							if($i==0){
								$clase_tabs = "active";
							}else
							{
								$clase_tabs = "";
							}							
							//--Verifico que los campos no esten vacíos
							if(isset($data["menu"][$i]["pantalla"])){$arreglo["pantalla"] = $data["menu"][$i]["pantalla"];}else{$arreglo["pantalla"] ="";}
							if(isset($data["menu"][$i]["referencia"])){$arreglo["referencia"] = $data["menu"][$i]["referencia"];}else{$arreglo["referencia"]="";}
							//
							$dicc = array(
											"{opmenu}" 	  		   => "opmenu_".$arreglo["referencia"],
											"{href}" 			   => "#".$arreglo["referencia"],
											"{titulo_p}"		   => $arreglo["pantalla"],
											"{clase_tabs}"		   => $clase_tabs	
							);
							$render.=str_replace(array_keys($dicc), array_values($dicc), $match_cal);
						}
					}
					$html = str_replace($match_cal, $render, $html);
				}	
					return $html;
		}	
		//--Metodo render vista
		public function render_vista($nombre_html,$html,$data_estatica,$data_dinamica){
			$html = $this->render_estatico($nombre_html,$html,$data_estatica);
			$html = $this->render_dinamico($html,$data_dinamica);
			print $html;
		}
	}
?>