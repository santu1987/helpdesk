<?php
	class cargarmisticketsView extends vistaBase{
		public function __construct(){
			parent::__construct();
		}
		//para realizar el render dinamico de la vista
		public function render_dinamico($html,$data){
			$html = $this->render_tickets($html, $data);
			return $html;
		}
		private function render_tickets($html, $data){
			if(($html != "")&&(count($data) > 0)){
				$render = "";
				$match_cal = $this->set_match_identificador_dinamico($html,"<!--row_mistickets-->");
				if($data["tickets"]!="NO_DATA")
				{
					$clase_tickets = 'show';
					$clase_tabla = 'hide';
				
					for($i=0;$i<count($data["tickets"]);$i++){
						//--Creo un case para determinar el color del span estatus
						switch($data["tickets"][$i]["id_estatus"])
						{
							case 1:
								$color_label = "label-primary";
								$icono = "<i class='fa fa-clock-o'></i>";
							break;
							case 2:
								$color_label = "label-verde";
								$icono = "<i class='fa fa-check'></i>";
							break;
							case 3:
								$color_label = "label-rojo";
								$icono = "<i class='fa fa-thumbs-o-up'></i>";
							break;
							case 5:
								$color_label = "label-amarillo";
								$icono = "<i class='fa fa-exclamation'></i>";
							break;	
						}
						//--Dandole formato a la fecha
						if(ISSET($data["tickets"][$i]["fecha_hora"]))
						{
							$fecha_hora = '';
							$fecha_hora = $data["tickets"][$i]["fecha_hora"];
							$fecha_hora = substr($fecha_hora,8,2)."/".substr($fecha_hora,5,2)."/".substr($fecha_hora,0,4)." ".substr($fecha_hora,10,9);	
						}	
						else
						{
							$fecha_hora = '';
						}			
						//Valido permisología
						if(($data["vector_permisos"][0]=='0')||($data["vector_permisos"][2]=='t'))//Si es usuario ó es técnico con permiso de incluir
						{
							$btn_apelar="<button name='btn_apelar'".$i." id='btn_apelar'".$i." onclick='apelar_sol(".$_SESSION["cedula"].",".$data["tickets"][$i]["id_ticket"].",\"<label>".$data["tickets"][$i]["descripcion_solicitud_ticket"].":</label>"." ".$data["tickets"][$i]["descripcion_ticket2"]."\");' class='btn btn-aceptar'>Apelar</button>";
						}else
						{
							$btn_apelar = "";
						}
						//Valido permisología
						if(($data["vector_permisos"][0]=='0')||($data["vector_permisos"][7]=='t'))//Si es usuario ó es técnico con permiso de incluir
						{
							$btn_eliminar="<button name='btn_eliminar'".$i." id='btn_eliminar'".$i." onclick='eliminar_sol(".$data["tickets"][$i]["id_ticket"].");' class='btn btn-cancelar'>Anular </button>";
						}else
						{
							$btn_eliminar = "";
						}
						$botones_row=$btn_apelar." ".$btn_eliminar;
						$dicc = array(
							"{numero}"			=>$data["tickets"][$i]["id_ticket"],
							"{descripcion}"		=>"<b>".$data["tickets"][$i]["descripcion_solicitud_ticket"].":</b>"." ".$data["tickets"][$i]["descripcion_ticket"],
							"{fecha_hora}"		=>$fecha_hora,
							"{estatus}"			=>"<div class='label_estatus'><span class='label ".$color_label."'>".$icono." ".$data["tickets"][$i][3]."</span></div>",
							"{estatus2}"		=>$data["tickets"][$i][3],
							"{tecnico}"			=>$data["tickets"][$i][4],
							"{descripcion2}"	=>"<b>".$data["tickets"][$i]["descripcion_solicitud_ticket"].":</b>"." ".$data["tickets"][$i]["descripcion_ticket2"],
							"{nombre_us_creador}"=>$_SESSION["nombre"],
							"{cedula_us_creador}"=>$_SESSION["cedula"],
							"{btn_row}"=>$botones_row,
							"{clase_tickets}"	=>$clase_tickets,
							"{clase_tabla}"		=>$clase_tabla		
						);
						$render.=str_replace(array_keys($dicc), array_values($dicc), $match_cal);
					}
				}else{
					$clase_tickets = 'hide';
					$clase_tabla = 'show';
					$dicc = array(
									"{clase_tickets}"	=>$clase_tickets,
									"{clase_tabla}"		=>$clase_tabla		
						);
						$render.=str_replace(array_keys($dicc), array_values($dicc), $match_cal);
				}	
				$html = str_replace($match_cal, $render, $html);
			}
			return $html;
		}
		public function render_vista($nombre_html,$html,$data_estatica,$data_dinamica){
			$html = $this->render_estatico($nombre_html,$html,$data_estatica);
			$html = $this->render_dinamico($html,$data_dinamica);
			print $html;
		}
	}
?>