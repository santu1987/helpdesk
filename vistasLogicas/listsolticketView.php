<?php
class listsolticketView extends VistaBase{
	public function __construct(){
			parent::__construct();
	}
	//para realizar el render dinamico de la vista
	public function render_dinamico($html,$data){
			$html = $this->render_tickets_sol($html, $data);
			return $html;
	}
	public function render_tickets_sol($html, $data){
		//cargo plantilla de tickets asignados
		if(($html != "")&&(count($data) > 0)){
			$render = "";
			$match_cal = $this->set_match_identificador_dinamico($html,"<!--row_sol_tickets-->");
			if($data["solticket"]!="NO_DATA"){
				for($i=0;$i<count($data["solticket"]);$i++){
				//--Dandole formato a la fecha
					$fecha_hora_calc = $data["solticket"][$i]["fecha_creacion"]." ".$data["solticket"][$i]["hora_creacion"];  
					$fecha_hora = '';
					$fecha_hora = $data["solticket"][$i]["fecha_creacion"];
					$fecha_hora = substr($fecha_hora,8,2)."/".substr($fecha_hora,5,2)."/".substr($fecha_hora,0,4)." ".substr($data["solticket"][$i]["hora_creacion"],0,8);
					$btn = "btn_sol".$i;
					//--Botones
					//Valido que se muestren los botones si se tiene permisos
					if($data["vector_permisos"][2]=="t")
					{
						switch ($data["solticket"][$i]["id_estatus_x_tecnico"])
						{
							case 2:
								$btn_operaciones="<button type='button' id='btn_sol".$i."' class='btn btn-aceptar btn_sol_ticket' onclick='cargar_modal_solticket(1,".$data["solticket"][$i]["id_ticket"].",".$data["solticket"][$i]["cedula_usuario"].",\"".$data["solticket"][$i]["nombres_apellidos"]."\",\"".$fecha_hora."\",\"<b>".$data["solticket"][$i]["descripcion_solicitud"].":</b>"." ".$data["solticket"][$i]["descripcion_ticket2"]."\",".$_SESSION["cedula"].")'>Responder</button>";
								break;
							case 3:
								$btn_operaciones="<button type='button' id='btn_sol".$i."' class='btn btn-danger btn-dn2' onclick='cargar_modal_solticket(2,".$data["solticket"][$i]["id_ticket"].",".$data["solticket"][$i]["cedula_usuario"].",\"".$data["solticket"][$i]["nombres_apellidos"]."\",\"".$fecha_hora."\",\"<b>".$data["solticket"][$i]["descripcion_solicitud"].":</b>"." ".$data["solticket"][$i]["descripcion_ticket2"]."\",".$_SESSION["cedula"].")');'>Cerrado</button>";
								break;
						}
					}else
					{
						$btn_operaciones="<div class='alert alert-info mensaje_permiso_solucion'><i class='fa fa-exclamation-circle'></i> No tiene permisos para esta acci&oacute;n</div>";
					}	
					//--Calculo las horas transcurridas hata el momento...
					$horas_transcurridas = $this->calcula_tiempo($fecha_hora_calc,date("Y-m-d H:i:s"));
					//--
					$dicc = array(
									"{imagen_us_sol}"		=>"http://appsigesp.avilatv.gob.ve/sno/fotospersonal/".$data["solticket"][$i]["cedula_usuario"],
									"{nombres_apellidos}"	=>$data["solticket"][$i]["nombres_apellidos"],
									"{nticket}"				=>$data["solticket"][$i]["id_ticket"],
									"{descripcion_ticket1}" =>$data["solticket"][$i]["descripcion_ticket"],
									"{tiempo_asignacion}"	=>$horas_transcurridas." Hrs",
									"{btn_operaciones}"		=>$btn_operaciones,
									"{fecha_hora_sol}"		=>$fecha_hora,
									"{descripcion2}"		=>"<b>".$data["solticket"][$i]["descripcion_solicitud"].":</b>"." ".$data["solticket"][$i]["descripcion_ticket2"],
									"{nombre_us_creador}"	=>$data["solticket"][$i]["nombres_apellidos"],
									"{cedula_us_creador}"	=>$data["solticket"][$i]["cedula_usuario"]
					);
					$render.=str_replace(array_keys($dicc),array_values($dicc), $match_cal);
				//---	
				}
				$html = str_replace($match_cal, $render, $html);
			}
		}
		return $html;
	}
	//para realizar la renderizacion de la vista
	public function render_vista($nombre_html,$html,$data_estatica,$data_dinamica){
			$html = $this->render_estatico($nombre_html,$html,$data_estatica);
			$html = $this->render_dinamico($html,$data_dinamica);
			print $html;
	}
	//--Metodo para calcular diferencias entre dos fechas y horas
	public function calcula_tiempo($start_time, $end_time) { 
	    $total_seconds = strtotime($end_time) - strtotime($start_time); 
	    $horas              = floor ( $total_seconds / 3600 );
	    $minutes            = ( ( $total_seconds / 60 ) % 60 );
	    $seconds            = ( $total_seconds % 60 );
	    $time['horas']      = str_pad( $horas, 2, "0", STR_PAD_LEFT );
	    $time['minutes']    = str_pad( $minutes, 2, "0", STR_PAD_LEFT );
	    $time['seconds']    = str_pad( $seconds, 2, "0", STR_PAD_LEFT );
	    $time               = implode( ':', $time );
	    return $time;
	}
	//--
}
?>